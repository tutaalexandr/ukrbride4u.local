<?php
return array(
	// set your paypal credential
	'client_id' => 'AfDp-i1axISy8K5a1LE-WbimpG06HDYInuVlQhd7ZdK8JK7tkyNBcd3xkdclt-xLTIn3EbLLi6YCDj-L',
	'secret'    => 'EAQnCR_xI791ldMLR0FeySGinVw8bW2HF2F5D733mE18ysLh1bmopWp6Hs4M4XR6xCn3WCYfGSCa9YRy',
	/**
	 * SDK configuration
	 */
	'settings'  => array(
		/**
		 * Available option 'sandbox' or 'live'
		 */
		'mode'                   => 'live',
		/**
		 * Specify the max request time in seconds
		 */
		'http.ConnectionTimeOut' => 30,
		/**
		 * Whether want to log to a file
		 */
		'log.LogEnabled'         => TRUE,
		/**
		 * Specify the file that want to write on
		 */
		'log.FileName'           => storage_path() . '/logs/paypal.log',
		/**
		 * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
		 *
		 * Logging is most verbose in the 'FINE' level and decreases as you
		 * proceed towards ERROR
		 */
		'log.LogLevel'           => 'ERROR',
	),
);