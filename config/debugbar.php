<?php

return array(

	/*
	 |--------------------------------------------------------------------------
	 | Debugbar Settings
	 |--------------------------------------------------------------------------
	 |
	 | Debugbar is enabled by default, when debug is set to true in app.php.
	 | You can override the value by setting enable to true or false instead of null.
	 |
	 */

	'enabled'         => FALSE,
	/*
	 |--------------------------------------------------------------------------
	 | Storage settings
	 |--------------------------------------------------------------------------
	 |
	 | DebugBar stores data for session/ajax requests.
	 | You can disable this, so the debugbar stores data in headers/session,
	 | but this can cause problems with large data collectors.
	 | By default, file storage (in the storage folder) is used. Redis and PDO
	 | can also be used. For PDO, run the package migrations first.
	 |
	 */
	'storage'         => array(
		'enabled'    => TRUE,
		'driver'     => 'file', // redis, file, pdo
		'path'       => storage_path() . '/debugbar', // For file driver
		'connection' => NULL,   // Leave null for default connection (Redis/PDO)
	),
	/*
	 |--------------------------------------------------------------------------
	 | Vendors
	 |--------------------------------------------------------------------------
	 |
	 | Vendor files are included by default, but can be set to false.
	 | This can also be set to 'js' or 'css', to only include javascript or css vendor files.
	 | Vendor files are for css: font-awesome (including fonts) and highlight.js (css files)
	 | and for js: jquery and and highlight.js
	 | So if you want syntax highlighting, set it to true.
	 | jQuery is set to not conflict with existing jQuery scripts.
	 |
	 */

	'include_vendors' => TRUE,
	/*
	 |--------------------------------------------------------------------------
	 | Capture Ajax Requests
	 |--------------------------------------------------------------------------
	 |
	 | The Debugbar can capture Ajax requests and display them. If you don't want this (ie. because of errors),
	 | you can use this option to disable sending the data through the headers.
	 |
	 */

	'capture_ajax'    => TRUE,
	/*
	 |--------------------------------------------------------------------------
	 | DataCollectors
	 |--------------------------------------------------------------------------
	 |
	 | Enable/disable DataCollectors
	 |
	 */

	'collectors'      => array(
		'phpinfo'         => TRUE,  // Php version
		'messages'        => TRUE,  // Messages
		'time'            => TRUE,  // Time Datalogger
		'memory'          => TRUE,  // Memory usage
		'exceptions'      => TRUE,  // Exception displayer
		'log'             => TRUE,  // Logs from Monolog (merged in messages if enabled)
		'db'              => TRUE,  // Show database (PDO) queries and bindings
		'views'           => TRUE,  // Views with their data
		'route'           => TRUE,  // Current route information
		'laravel'         => TRUE, // Laravel version and environment
		'events'          => TRUE, // All events fired
		'default_request' => TRUE, // Regular or special Symfony request logger
		'symfony_request' => TRUE,  // Only one can be enabled..
		'mail'            => TRUE,  // Catch mail messages
		'logs'            => TRUE, // Add the latest log messages
		'files'           => TRUE, // Show the included files
		'config'          => TRUE, // Display config settings
		'auth'            => TRUE, // Display Laravel authentication status
		'session'         => TRUE, // Display session data in a separate tab
	),
	/*
	 |--------------------------------------------------------------------------
	 | Extra options
	 |--------------------------------------------------------------------------
	 |
	 | Configure some DataCollectors
	 |
	 */

	'options'         => array(
		'auth'  => array(
			'show_name' => FALSE,   // Also show the users name/email in the debugbar
		),
		'db'    => array(
			'with_params' => TRUE,   // Render SQL with the parameters substituted
			'timeline'    => TRUE,  // Add the queries to the timeline
			'backtrace'   => FALSE,  // EXPERIMENTAL: Use a backtrace to find the origin of the query in your files.
			'explain'     => array(            // EXPERIMENTAL: Show EXPLAIN output on queries
											   'enabled' => FALSE,
											   'types'   => array('SELECT'), // array('SELECT', 'INSERT', 'UPDATE', 'DELETE'); for MySQL 5.6.3+
			),
			'hints'       => TRUE,    // Show hints for common mistakes
		),
		'mail'  => array(
			'full_log' => TRUE,
		),
		'views' => array(
			'data' => FALSE,    //Note: Can slow down the application, because the data can be quite large..
		),
		'route' => array(
			'label' => TRUE  // show complete route on bar
		),
		'logs'  => array(
			'file' => NULL,
		),
	),
	/*
	 |--------------------------------------------------------------------------
	 | Inject Debugbar in Response
	 |--------------------------------------------------------------------------
	 |
	 | Usually, the debugbar is added just before <body>, by listening to the
	 | Response after the App is done. If you disable this, you have to add them
	 | in your template yourself. See http://phpdebugbar.com/docs/rendering.html
	 |
	 */

	'inject'          => TRUE,
	/*
	 |--------------------------------------------------------------------------
	 | DebugBar route prefix
	 |--------------------------------------------------------------------------
	 |
	 | Sometimes you want to set route prefix to be used by DebugBar to load
	 | its resources from. Usually the need comes from misconfigured web server or
	 | from trying to overcome bugs like this: http://trac.nginx.org/nginx/ticket/97
	 |
	 */
	'route_prefix'    => '{language_prefix?}/_debugbar',

);
