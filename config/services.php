<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun'  => [
	'domain' => '',
	'secret' => '',
	],
	'mandrill' => [
	'secret' => '',
	],
	'ses'      => [
	'key'    => '',
	'secret' => '',
	'region' => 'us-east-1',
	],
	'stripe'   => [
	'model'  => 'App\User',
	'secret' => '',
	],
	'facebook' => [
	'client_id'     => '794285094009470',
	'client_secret' => '6047cf3aeb7a01001390d8ee9819684d',
	'redirect'      => 'http://ukrbride4u.com/ru/callback',
	],

	];
