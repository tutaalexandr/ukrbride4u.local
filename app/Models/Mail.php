<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'mail';

	//public static $timestamps = TRUE;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'partner_id',
		'msg',
		'viewed',
		'user_delete',
		'partner_delete',
		'user_erase',
		'partner_erase',
		'ArrayIdGifts',
		'Imagedounload',
	];

	public static function getInboxMsgs($user_id, $limit = 50) {
		return DB::table('mail')
				 ->select('mail.created_at AS created_at', 'mail.id AS id', 'mail.msg AS msg', 'mail.viewed AS viewed', 'users.id AS partner_id', 'users.name AS partner_name', 'users.sex AS partner_sex', 'users.main_photo AS partner_photo')
				 ->selectRaw('TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `partner_age`')
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `partner_online`')
				 ->leftJoin('users', 'users.id', '=', 'mail.user_id')
				 ->where('mail.partner_id', '=', $user_id)
				 ->where('mail.partner_delete', '=', 0)
				 ->where('mail.partner_erase', '=', 0)
				 ->orderBy('mail.id', 'desc')
				 ->take($limit)
				 ->get();
	}

	public static function getSentMsgs($user_id, $limit = 50) {
		return DB::table('mail')
				 ->select('mail.created_at AS created_at', 'mail.Imagedounload AS Imagedounload', 'mail.ArrayIdGifts AS ArrayIdGifts', 'mail.id AS id', 'mail.msg AS msg', 'mail.viewed AS viewed', 'users.id AS partner_id', 'users.name AS partner_name', 'users.sex AS partner_sex', 'users.main_photo AS partner_photo')
				 ->selectRaw('TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `partner_age`')
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `partner_online`')
				 ->leftJoin('users', 'users.id', '=', 'mail.partner_id')
				 ->where('mail.user_id', '=', $user_id)
				 ->where('mail.user_delete', '=', 0)
				 ->where('mail.user_erase', '=', 0)
				 ->orderBy('mail.id', 'desc')
				 ->take($limit)
				 ->get();
	}

	public static function getDeleteMsgs($user_id, $limit = 50) {
		return DB::table('mail')
				 ->select('mail.id AS id', 'mail.msg AS msg', 'users.id AS partner_id', 'users.name AS partner_name', 'users.sex AS partner_sex', 'users.main_photo AS partner_photo')
				 ->selectRaw("IF(`mail`.`user_id` = $user_id, 1, 0) AS `is_sent`")
				 ->selectRaw('TIMESTAMPDIFF(YEAR, `users`.`bdate`, CURDATE()) AS `partner_age`')
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `partner_online`')
				 ->leftJoin('users', 'users.id', '=', DB::raw("(CASE WHEN `mail`.`user_id` = $user_id THEN `mail`.`partner_id` ELSE `mail`.`user_id` END)"))
				 ->whereNested(function ($query) use ($user_id) {
					 $query->where('mail.user_id', '=', $user_id)->where('mail.user_delete', '=', 1)->where('mail.user_erase', '=', 0);
				 })
				 ->orWhere(function ($query) use ($user_id) {
					 $query->where('mail.partner_id', '=', $user_id)->where('mail.partner_delete', '=', 1)->where('mail.partner_erase', '=', 0);
				 })
				 ->orderBy('mail.id', 'desc')
				 ->take($limit)
				 ->get();
	}

	public static function getMsg($msg_id, $user_id) {
		if ($mail = Mail::find($msg_id, ['user_id'])) {
			if ($mail->user_id == $user_id) {
				$isDelete = 'mail.user_delete AS delete';
				$join = 'mail.partner_id';
				$whereErase = 'mail.user_erase';
			}
			else {
				$isDelete = 'mail.partner_delete AS delete';
				$join = 'mail.user_id';
				$whereErase = 'mail.partner_erase';
			}
			return DB::table('mail')
					 ->select('mail.Imagedounload AS Imagedounload', 'mail.ArrayIdGifts AS ArrayIdGifts', 'mail.id AS id', 'mail.user_id AS user_id', 'mail.partner_id AS partner_id', 'mail.msg AS msg', 'mail.viewed AS viewed', $isDelete, 'users.id AS user_partner_id', 'users.name AS partner_name', 'users.sex AS partner_sex', 'users.main_photo AS partner_photo', 'users.about_me AS partner_about_me')
					 ->selectRaw('TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `partner_age`')
					 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `partner_online`')
					 ->leftJoin('users', 'users.id', '=', $join)
					 ->where('mail.id', '=', $msg_id)
					 ->where($whereErase, '=', 0)
					 ->first();
		}
		else
			return NULL;
	}

	public static function setViewed($user_id) {
		DB::table('mail')->where('partner_id', $user_id)->update(array('viewed' => 1));
	}

	public static function getNewMsgsCnt($user_id) {
		return DB::table('mail')->where('partner_id', $user_id)->where('viewed', 0)->count();
	}

	public static function clearOldData() {
		foreach (DB::table('mail')->selectRaw("CONCAT_WS(',', `user_id`, `partner_id`) as `in`")->groupBy(['user_id', 'partner_id'])->get() AS $res) {
			$sql = "
				SELECT
					`id`,
					`Imagedounload`
				FROM `mail`
				WHERE `id` NOT IN
					  (
						  SELECT `id`
						  FROM
							  (
								  SELECT `id`
								  FROM `mail`
								  WHERE `user_id` IN ($res->in) AND `partner_id` IN ($res->in)
								  ORDER BY `created_at` DESC, `id` DESC
								  LIMIT 500
							  ) AS `sq`
					  )
					  AND `user_id` IN ($res->in) AND `partner_id` IN ($res->in)
			";
			foreach (DB::select($sql) AS $resToDel) {
				if (!empty($resToDel->Imagedounload))
					if (file_exists(public_path() . "/upload/mailfotos/" . $resToDel->Imagedounload))
						unlink(public_path() . "/upload/mailfotos/" . $resToDel->Imagedounload);
				self::find((int) $resToDel->id)->delete();
			}
		}
	}

	public function setUpdatedAt($value) {
		//Do-nothing
	}

	public function getUpdatedAtColumn() {
		//Do-nothing
	}

	public function getCostGifts($arrayId, $real = 0) {
		return DB::table('gifts')->where('isreal', '=', $real)->whereIn('id', $arrayId)->sum('price');
	}

	public function getImageGifts($arrayId) {
		return DB::table('gifts')->where('isreal', '=', 0)->whereIn('id', $arrayId)->get();
	}
}
