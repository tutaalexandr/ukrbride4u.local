<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class ChatRooms extends Model {

	protected $fillable = [
		'man_id',
		'lady_id',
		'active',
		'man_delete',
		'lady_delete',
		'man_erase',
		'lady_erase',
	];

	public static function needStartCheck($man_id, $lady_id) {
		return !DB::table('chat_rooms')->where('man_id', $man_id)->where('lady_id', $lady_id)->where('active', 1)->exists();
	}

	/**
	 * @param $user User
	 *
	 * @return array|static[]
	 */
	public static function getChatRoomsList($user) {
		$user_join_field = '';
		$user_field = '';
		$del_field = '';
		$erase_field = '';
		if ($user->sex == 'm') {
			$user_join_field = 'chat_rooms.lady_id';
			$user_field = 'chat_rooms.man_id';
			$del_field = 'chat_rooms.man_delete';
			$erase_field = 'chat_rooms.man_erase';
		}
		else {
			$user_join_field = 'chat_rooms.man_id';
			$user_field = 'chat_rooms.lady_id';
			$del_field = 'chat_rooms.lady_delete';
			$erase_field = 'chat_rooms.lady_erase';
		}

		return DB::table('chat_rooms')
				 ->select('chat_rooms.id AS id', 'chat_rooms.active AS active', 'users.id AS partner_id', 'users.name AS partner_name', 'users.sex AS partner_sex', 'users.main_photo AS partner_main_photo')
				 ->selectRaw("(SELECT COUNT(`id`) FROM `chat` WHERE `user_id` = users.id AND `partner_id` = $user->id AND `viewed` = 0) AS `new_msgs_cnt`")
				 ->leftJoin('users', 'users.id', '=', $user_join_field)
				 ->where($user_field, '=', $user->id)
				 ->where($del_field, '=', 0)
				 ->where($erase_field, '=', 0)
				 ->orderBy('chat_rooms.active', 'desc')
				 ->orderBy('chat_rooms.updated_at', 'desc')
				 ->get();
	}
	public static function getChatRoomsListAdmin($id) {
		$user_join_field = '';
		$user_field = '';
		$del_field = '';
		$erase_field = '';

			$user_join_field = 'chat_rooms.man_id';
			$user_field = 'chat_rooms.lady_id';
			$del_field = 'chat_rooms.lady_delete';
			$erase_field = 'chat_rooms.lady_erase';


		return DB::table('chat_rooms')
			->select('chat_rooms.id AS id', 'chat_rooms.active AS active', 'users.id AS partner_id', 'users.name AS partner_name', 'users.sex AS sex', 'users.main_photo AS partner_main_photo')
			->selectRaw("(SELECT COUNT(`id`) FROM `chat` WHERE `user_id` = users.id AND `partner_id` = $id AND `viewed` = 0) AS `new_msgs_cnt`")
			->leftJoin('users', 'users.id', '=', $user_join_field)
			->where($user_field, '=', $id)
			->where($del_field, '=', 0)
			->where($erase_field, '=', 0)
			->orderBy('chat_rooms.active', 'desc')
			->orderBy('chat_rooms.updated_at', 'desc')
			->get();
	}
	/**
	 * @param $user User
	 *
	 * @return string
	 */
	public static function getFirstActiveChatRoomTime($user) {
		$user_field = '';
		$del_field = '';
		$erase_field = '';
		if ($user->sex == 'm') {
			$user_field = 'chat_rooms.man_id';
			$del_field = 'chat_rooms.man_delete';
			$erase_field = 'chat_rooms.man_erase';
		}
		else {
			$user_field = 'chat_rooms.lady_id';
			$del_field = 'chat_rooms.lady_delete';
			$erase_field = 'chat_rooms.lady_erase';
		}

		$res = DB::table('chat_rooms')
				 ->select('chat_rooms.updated_at')
				 ->where($user_field, '=', $user->id)
				 ->where($del_field, '=', 0)
				 ->where($erase_field, '=', 0)
				 ->where('chat_rooms.active', '=', 1)
				 ->orderBy('chat_rooms.updated_at', 'asc')
				 ->first();
		return empty($res) ? NULL : $res->updated_at;
	}

	public static function stopOldChatRooms() {
		DB::table('chat_rooms')
		  ->leftJoin('users', 'users.id', '=', 'chat_rooms.man_id')
		  ->where('chat_rooms.active', '=', 1)
		  ->whereRaw('TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) > ' . \Config::get('auth.user_online_time'))
		  ->update([
			  'chat_rooms.active'     => 0,
			  'chat_rooms.updated_at' => DB::raw('UTC_TIMESTAMP()'),
		  ]);
		self::setEndTimeForStoppedRooms();
	}

	private static function setEndTimeForStoppedRooms() {
		DB::table('chat_rooms_log')
		  ->leftJoin('chat_rooms', 'chat_rooms_log.chat_room_id', '=', 'chat_rooms.id')
		  ->where('chat_rooms.active', '=', 0)
		  ->whereNull('chat_rooms_log.end_time')
		  ->update([
			  'chat_rooms_log.end_time' => DB::raw('UTC_TIMESTAMP()'),
		  ]);
	}

	public static function stopChatRoomsWhereUserNoMoney() {
		$activeChatRooms = DB::table('chat_rooms')
							 ->select(DB::raw('COUNT(chat_rooms.id) AS chat_rooms_count'), 'chat_rooms.man_id', 'billing.balance')
							 ->leftJoin('billing', 'billing.user_id', '=', 'chat_rooms.man_id')
							 ->where('chat_rooms.active', '=', 1)
							 ->groupBy('billing.user_id')
							 ->get();
		foreach ($activeChatRooms AS $room) {
			$userBalance = (float) $room->balance;
			$chatRoomsCnt = (int) $room->chat_rooms_count;
			$textChatPrice = (float) \Config::get('prices.text_chat');
			if ($userBalance < ($chatRoomsCnt * $textChatPrice))
				DB::table('chat_rooms')->where('chat_rooms.active', '=', 1)->where('chat_rooms.man_id', '=', $room->man_id)->update([
					'chat_rooms.active'     => 0,
					'chat_rooms.updated_at' => DB::raw('UTC_TIMESTAMP()'),
				]);
		}
		self::setEndTimeForStoppedRooms();
	}

	/**
	 * @param $user User
	 *
	 * @return array|static[]
	 */
	public static function getChatRoomsKeyList($user) {
		$user_join_field = '';
		$user_field = '';
		$del_field = '';
		$erase_field = '';
		if ($user->sex == 'm') {
			$user_join_field = 'chat_rooms.lady_id';
			$user_field = 'chat_rooms.man_id';
			$del_field = 'chat_rooms.man_delete';
			$erase_field = 'chat_rooms.man_erase';
		}
		else {
			$user_join_field = 'chat_rooms.man_id';
			$user_field = 'chat_rooms.lady_id';
			$del_field = 'chat_rooms.lady_delete';
			$erase_field = 'chat_rooms.lady_erase';
		}

		return DB::table('chat_rooms')
				 ->select(DB::raw("GROUP_CONCAT(`chat_rooms`.`id`, `chat_rooms`.`active`, `users`.`id`, (SELECT COUNT(`id`) FROM `chat` WHERE `user_id` = users.id AND `partner_id` = $user->id AND `viewed` = 0) ORDER BY `chat_rooms`.`active` DESC, `chat_rooms`.`updated_at` DESC SEPARATOR '') AS `res`"))
				 ->leftJoin('users', 'users.id', '=', $user_join_field)
				 ->where($user_field, '=', $user->id)
				 ->where($del_field, '=', 0)
				 ->where($erase_field, '=', 0)
				 ->first()->res;
	}

	/**
	 * @param $user User
	 *
	 * @return array|static[]
	 */
	public static function getChatHistoryRoomsList($user) {
		$user_join_field = '';
		$user_field = '';
		$del_field = '';
		$erase_field = '';
		if ($user->sex == 'm') {
			$user_join_field = 'chat_rooms.lady_id';
			$user_field = 'chat_rooms.man_id';
			$del_field = 'chat_rooms.man_delete';
			$erase_field = 'chat_rooms.man_erase';
		}
		else {
			$user_join_field = 'chat_rooms.man_id';
			$user_field = 'chat_rooms.lady_id';
			$del_field = 'chat_rooms.lady_delete';
			$erase_field = 'chat_rooms.lady_erase';
		}

		return DB::table('chat_rooms')
				 ->select('chat_rooms.id AS id', 'chat_rooms.active AS active', 'users.id AS partner_id', 'users.name AS partner_name', 'users.sex AS partner_sex', 'users.main_photo AS partner_main_photo')
				 ->selectRaw("(SELECT COUNT(`id`) FROM `chat` WHERE `user_id` = users.id AND `partner_id` = $user->id AND `viewed` = 0) AS `new_msgs_cnt`")
				 ->leftJoin('users', 'users.id', '=', $user_join_field)
				 ->where($user_field, '=', $user->id)
				 ->where($del_field, '=', 1)
				 ->where($erase_field, '=', 0)
				 ->orderBy('chat_rooms.active', 'desc')
				 ->orderBy('chat_rooms.updated_at', 'desc')
				 ->get();
	}

	/**
	 * @param $user User
	 *
	 * @return array|static[]
	 */
	public static function getChatHistoryRoomsKeyList($user) {
		$user_join_field = '';
		$user_field = '';
		$del_field = '';
		$erase_field = '';
		if ($user->sex == 'm') {
			$user_join_field = 'chat_rooms.lady_id';
			$user_field = 'chat_rooms.man_id';
			$del_field = 'chat_rooms.man_delete';
			$erase_field = 'chat_rooms.man_erase';
		}
		else {
			$user_join_field = 'chat_rooms.man_id';
			$user_field = 'chat_rooms.lady_id';
			$del_field = 'chat_rooms.lady_delete';
			$erase_field = 'chat_rooms.lady_erase';
		}

		return DB::table('chat_rooms')
				 ->select(DB::raw("GROUP_CONCAT(`chat_rooms`.`id`, `chat_rooms`.`active`, `users`.`id`, (SELECT COUNT(`id`) FROM `chat` WHERE `user_id` = users.id AND `partner_id` = $user->id AND `viewed` = 0) ORDER BY `chat_rooms`.`active` DESC, `chat_rooms`.`updated_at` DESC SEPARATOR '') AS `res`"))
				 ->leftJoin('users', 'users.id', '=', $user_join_field)
				 ->where($user_field, '=', $user->id)
				 ->where($del_field, '=', 1)
				 ->where($erase_field, '=', 0)
				 ->first()->res;
	}

	public static function restoreRoom($man_id, $lady_id) {
		DB::table('chat_rooms')->where('man_id', $man_id)->where('lady_id', $lady_id)->update([
			'man_delete'  => 0,
			'lady_delete' => 0,
			'man_erase'   => 0,
			'lady_erase'  => 0,
		]);
	}

	public static function stopAllChatRooms($user) {
		$data = [];
		if ($user->sex == 'm') {
			$whereField = 'man_id';
			$data['active'] = 0;
			$data['man_delete'] = 1;
			$data['man_erase'] = 0;
		}
		else {
			$whereField = 'lady_id';
			$data['active'] = 0;
			$data['lady_delete'] = 1;
			$data['lady_erase'] = 0;
		}

		DB::table('chat_rooms')->where($whereField, $user->id)->update($data);
	}

	/**
	 * @param $user User
	 *
	 * @return bool User can or can't start a new chat
	 */
	public static function checkBillingForStartNewChat($user) {
		return self::checkEnoughMoney($user, 2);
	}

	/**
	 * @param $user User
	 * @param $mins int
	 *
	 * @return bool
	 */
	public static function checkEnoughMoney($user, $mins) {
		$userBalance = (float) $user->billing['balance'];
		$chatRoomsCnt = (int) DB::table('chat_rooms')->where('chat_rooms.active', '=', 1)->count('id');
		$textChatPrice = (float) \Config::get('prices.text_chat');
		return ($textChatPrice * $mins) < ($userBalance - ($chatRoomsCnt * $textChatPrice));
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function chatRoomLog() {
		return $this->hasMany('App\Models\ChatRoomsLog', 'chat_room_id');
	}

}
