<?php namespace App\Models;

use Cache;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;

class ChoiseAdminUsers extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'choise_admin_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id',
		'sex',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	public function setUpdatedAt($value) {
		//Do-nothing
	}

	public function getUpdatedAtColumn() {
		//Do-nothing
	}

	public function setCreatedAt($value) {
		//Do-nothing
	}

	public function getCreatedAtColumn() {
		//Do-nothing
	}
}
