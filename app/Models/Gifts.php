<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gifts extends Model {

	protected $table = 'gifts';

	protected $fillable = [
		'id',
		'title',
		'price',
		'oldprice',
		'isreal',
		'image',

	];

	public function setUpdatedAt($value) {
		//Do-nothing
	}

	public function getUpdatedAtColumn() {
		//Do-nothing
	}

	public function setCreatedAt($value) {
		//Do-nothing
	}

	public function getCreatedAtColumn() {
		//Do-nothing
	}
}