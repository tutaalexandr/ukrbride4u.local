<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $table = 'news';

    protected $fillable = [
        'news_date',
        'image',
        'url_name',
        'text',
        'preview',
        'title',
        'newsOrStories',

    ];

    public function GetPablichedNews()
    {
        $News = News::latest('news_date')
                    ->where('newsOrStories', '=', 0)
                    ->where('news_active', '=', 0)
                    ->orderBy('created_at', 'desc')
                    ->limit(8)
                    ->get();

        return $News;
    }

    public function GetPablichedOneStory()
    {
        $Story = News::latest('news_date')->where('newsOrStories', '=', 1)->where('news_active', '=', 0)->orderBy('created_at', 'desc')->first();

        return $Story;
    }

    public function GetStories()
    {
        $ggetstory = News::latest('id')->where('newsOrStories', '=', 1)->where('news_active', '=', 0)->get();

        return $ggetstory;
    }

    public function GetPablichedOneNews($id)
    {
        $news = News::where('url_name', '=', $id)->get();

        return $news;
    }

    public function AdminPablichedNews()
    {
        $News = News::latest('news_date')
                    ->where('newsOrStories', '=', 0)
                    ->where('news_active', '=', 0)
                    ->orderBy('created_at', 'desc')
                    ->limit(100)
                    ->get();

        return $News;
    }

    public function AdminPablichedStory()
    {
        $Story = News::latest('news_date')
                     ->where('newsOrStories', '=', 1)
                     ->where('news_active', '=', 0)
                     ->orderBy('created_at', 'desc')
                     ->limit(100)
                     ->get();

        return $Story;
    }
}
