<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPhotos extends Model {

//	protected $table = 'user_photos';

	public $timestamps = FALSE;

	protected $fillable = [
		'img',
	];

	public function user() {
		return $this->belongsTo('App\Models\User');
	}

}
