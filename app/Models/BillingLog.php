<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class BillingLog extends Model {

	protected $table = 'billing_log';

//	public $timestamps = true;

	protected $fillable = [
		'billing_id',
		'partner_id',
		'event',
		'gifts',
		'before',
		'difference',
		'after',
	];

	public static function getHistoryData($billing_id, $limit = 200) {
		return DB::table('billing_log')
				 ->select(DB::raw('DATE_FORMAT(billing_log.created_at, \'%Y-%m-%d\') AS date'), 'billing_log.event AS event', 'billing_log.partner_id AS partner_id', 'users.name AS partner_name', DB::raw('ROUND(billing_log.difference, 2) AS paid'))
				 ->leftJoin('users', 'users.id', '=', 'billing_log.partner_id')
				 ->where('billing_log.billing_id', '=', $billing_id)
				 ->orderBy('billing_log.created_at', 'ASC')
				 ->take($limit)
				 ->get();
	}

	public static function getHistoryDataDesc($billing_id, $limit = 200) {
		return DB::table('billing_log')
			->select(DB::raw('DATE_FORMAT(billing_log.created_at, \'%Y-%m-%d\') AS date'), 'billing_log.gifts AS gifts', 'billing_log.event AS event', 'billing_log.partner_id AS partner_id', 'users.name AS partner_name', DB::raw('ROUND(billing_log.difference, 2) AS paid'))
			->leftJoin('users', 'users.id', '=', 'billing_log.partner_id')
			->where('billing_log.billing_id', '=', $billing_id)
			->orderBy('billing_log.created_at', 'Desc')
			->take($limit)
			->get();
	}
	public static function getAdminStatistics($fromDate = NULL, $toDate = NULL) {
		$userOnlineTime = \Config::get('auth.user_online_time');
		$startDate = "date_format(" . (empty($fromDate) ? 'UTC_TIMESTAMP()' : "'$fromDate'") . ", '%Y-%m-%d 00:00:00')";
		$endDate = "date_format(" . (empty($toDate) ? 'UTC_TIMESTAMP()' : "'$toDate'") . ", '%Y-%m-%d 23:59:59')";
		$sql = "
			SELECT
				`u`.`id` AS `user_id`,
				`u`.`name` AS `user_name`,
				`b`.`balance` AS `user_balance`,
				(TIMESTAMPDIFF(MINUTE, `u`.`last_visit_at`, UTC_TIMESTAMP()) < $userOnlineTime) AS `user_online`,
				`b`.`id` AS `billing_id`,
				(SELECT MAX(`before`) FROM `billing_log`  WHERE `created_at` = MIN(`bl`.`created_at`) AND `billing_id` = `bl`.`billing_id`) as `beginning_of_period`,
				(SELECT MIN(`after`) FROM `billing_log` WHERE `created_at` = MAX(`bl`.`created_at`) AND `billing_id` = `bl`.`billing_id`) as `end_of_period`,
				SUM(CASE WHEN `event` = 'recharge' THEN `difference` ELSE 0 END) as `total_refill`,
				SUM(CASE WHEN `event` != 'recharge' THEN `difference` ELSE 0 END) as `total_paid`,
				(SELECT
					SUM(TIMESTAMPDIFF(SECOND, `crl_in`.`start_time`, `crl_in`.`end_time`)) / 60
				FROM
					`chat_rooms_log` `crl_in`
				LEFT JOIN `chat_rooms` `cr_in` ON `cr_in`.`id` = `crl_in`.`chat_room_id`
				WHERE
					`crl_in`.`start_time` >= $startDate AND
					`crl_in`.`start_time` <= $endDate AND
					`crl_in`.`end_time` >= $startDate AND
					`crl_in`.`end_time` <= $endDate AND
					`cr_in`.`man_id` = `u`.`id`
				GROUP BY `cr_in`.`man_id`) AS `chat_mins`,
				(SELECT
					SUM(`crl_in`.`paid`)
				FROM
					`chat_rooms_log` `crl_in`
				LEFT JOIN `chat_rooms` `cr_in` ON `cr_in`.`id` = `crl_in`.`chat_room_id`
				WHERE
					`crl_in`.`start_time` >= $startDate AND
					`crl_in`.`start_time` <= $endDate AND
					`crl_in`.`end_time` >= $startDate AND
					`crl_in`.`end_time` <= $endDate AND
					`cr_in`.`man_id` = `u`.`id`
				GROUP BY `cr_in`.`man_id`) AS `chat_paid`,
				SUM(CASE WHEN `event` = 'real_gift' THEN `difference` ELSE 0 END) as `real_gifts_paid`,
				SUM(CASE WHEN `event` = 'virt_gift' THEN `difference` ELSE 0 END) as `virt_gifts_paid`,
				SUM(CASE WHEN `event` = 'mail' THEN `difference` ELSE 0 END) as `mails_paid`
			FROM
				`billing_log` `bl`
			LEFT JOIN `billing` `b` ON `b`.`id` = `bl`.`billing_id`
			LEFT JOIN `users` `u` ON `u`.`id` = `b`.`user_id`
			WHERE
				`bl`.`created_at` >= $startDate AND
				`bl`.`created_at` <= $endDate # AND
				#`u`.`rights` != 'superadmin'
			GROUP BY `billing_id`;
		";

		return DB::select($sql);
	}

	public static function clearOldData() {
		DB::table('billing_log')->whereRaw('`created_at` < DATE_SUB(UTC_TIMESTAMP, INTERVAL 3 MONTH)')->delete();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function partner() {
		return $this->belongsTo('App\Models\User');
	}

	/**
	 * @return User
	 */
	public function user() {
		return $this->billing()->user();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function billing() {
		return $this->belongsTo('App\Models\Billing');
	}

	public function setUpdatedAt($value) {
		//Do-nothing
	}

	public function getUpdatedAtColumn() {
		//Do-nothing
	}
}
