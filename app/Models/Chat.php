<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'chat';

	//public static $timestamps = TRUE;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id',
		'partner_id',
		'msg',
	];

	public static function getMsgs($user_id, $partner_id, $last_id = 0, $limit = 100) {
		return DB::table('chat')
				 ->select('chat.id AS id', 'chat.user_id AS user_id', 'users.name AS name', 'chat.msg AS msg', 'chat.created_at AS created_at')
				 ->leftJoin('users', 'users.id', '=', 'chat.user_id')
				 ->where('chat.id', '>', $last_id)
				 ->whereIn('chat.user_id', [$user_id, $partner_id,])
				 ->whereIn('chat.partner_id', [$user_id, $partner_id,])
				 ->orderBy('chat.id', 'desc')
				 ->take($limit)
				 ->get();
	}

	public static function setViewed($user_id, $partner_id) {
		DB::table('chat')->where('user_id', $partner_id)->where('partner_id', $user_id)->update(array('viewed' => 1));
	}

	public static function setViewedAll($user_id) {
		DB::table('chat')->where('partner_id', $user_id)->update(array('viewed' => 1));
	}

	public static function getNewMsgsCnt($user_id) {
		return DB::table('chat')->where('partner_id', $user_id)->where('viewed', 0)->count();
	}
	public static function getNewNews($user_id) {
		return DB::table('view_news')->where('id_user', '=', $user_id)->count();
	}
	public static function getLastMsgs($user_id) {
		return DB::table('chat')
			->where('partner_id', $user_id)
			->where('viewed', 0)
			->orderBy('chat.id', 'desc')
			->take(1)
			->get();
	}
	public static function clear($user_id, $partner_id) {
		DB::table('chat')->whereIn('user_id', [$user_id, $partner_id])->whereIn('partner_id', [$user_id, $partner_id])->delete();
	}

	public static function clearOldData() {
		foreach (DB::table('chat')->selectRaw("CONCAT_WS(',', `user_id`, `partner_id`) as `in`")->groupBy(['user_id', 'partner_id'])->get() AS $res) {
			DB::delete("
				DELETE FROM `chat`
					WHERE `id` NOT IN
						  (
							  SELECT `id`
							  FROM
								  (
									  SELECT `id`
									  FROM `chat`
									  WHERE `user_id` IN ($res->in) AND `partner_id` IN ($res->in)
									  ORDER BY `created_at` DESC, `id` DESC
									  LIMIT 1000
								  ) AS `sq`
						  )
						  AND `user_id` IN ($res->in) AND `partner_id` IN ($res->in)
			");
		}
	}

	public function setUpdatedAt($value) {
		//Do-nothing
	}

	public function getUpdatedAtColumn() {
		//Do-nothing
	}
}
