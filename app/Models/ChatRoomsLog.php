<?php namespace App\Models;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class ChatRoomsLog extends Model {

	protected $table = 'chat_rooms_log';

	protected $fillable = [
		'start_time',
		'payment_time',
		'end_time',
		'paid_complete',
		'paid',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function chatRoom() {
		return $this->belongsTo('App\Models\ChatRooms');
	}

	public static function boot() {
		parent::boot();

		ChatRoomsLog::saving(function ($chatRoomLog) {
			if (empty($chatRoomLog->start_time))
				$chatRoomLog->start_time = \DB::raw('UTC_TIMESTAMP()');
		});
	}

	public static function calcPaidForChatRoomsLog() {
		$res = DB::table('chat_rooms_log')->where('paid_complete', '=', 0)->get();
		$carbonNow = Carbon::createFromTimestampUTC(time());
		$price = 0;
		foreach ($res AS $log) {
			$price = 0;
			$updateData = [];
			if (empty($log->end_time) && empty($log->payment_time)) { // диалог продолжается, еще не снимали средства
				$updateData['payment_time'] = $carbonNow->format('Y-m-d H:i:s');
				$price = ((float) Carbon::createFromFormat('Y-m-d H:i:s', $log->start_time)->diffInSeconds($carbonNow) * \Config::get('prices.text_chat')) / 60;
				$updateData['paid'] = (float) $log->paid + $price;
				$updateData['paid_complete'] = 0;
			}
			elseif (empty($log->end_time) && !empty($log->payment_time)) { // диалог продолжается, средства снимали
				$updateData['payment_time'] = $carbonNow->format('Y-m-d H:i:s');
				$price = ((float) Carbon::createFromFormat('Y-m-d H:i:s', $log->payment_time)->diffInSeconds($carbonNow) * \Config::get('prices.text_chat')) / 60;
				$updateData['paid'] = (float) $log->paid + $price;
				$updateData['paid_complete'] = 0;
			}
			elseif (!empty($log->end_time) && empty($log->payment_time)) { // диалог окончен, средства еще не снимали
				$updateData['payment_time'] = $log->end_time;
				$price = ((float) Carbon::createFromFormat('Y-m-d H:i:s', $log->start_time)
										->diffInSeconds(Carbon::createFromFormat('Y-m-d H:i:s', $log->end_time)) * \Config::get('prices.text_chat')) / 60;
				$updateData['paid'] = (float) $log->paid + $price;
				$updateData['paid_complete'] = 1;
			}
			elseif (!empty($log->end_time) && !empty($log->payment_time)) { // диалог окончен, средства снимали
				$updateData['payment_time'] = $log->end_time;
				$price = ((float) Carbon::createFromFormat('Y-m-d H:i:s', $log->payment_time)
										->diffInSeconds(Carbon::createFromFormat('Y-m-d H:i:s', $log->end_time)) * \Config::get('prices.text_chat')) / 60;
				$updateData['paid'] = (float) $log->paid + $price;
				$updateData['paid_complete'] = 1;
			}

			/**
			 * @var $user        User
			 * @var $chatRoomLog ChatRoomsLog
			 */

			// Set data in chat_room_log
			$chatRoomLog = self::find($log->id);
			$chatRoomLog->update($updateData);

			// Get money from current man
			$user = User::find($chatRoomLog->chatRoom['man_id']);
			$user->billing()->update(['balance' => $user->billing['balance'] - $price]);
			$user->save();
			$user = User::find($chatRoomLog->chatRoom['man_id']); // TODO: can optimize

			// Save action in history actions
			if ($updateData['paid_complete'] == 1) {
				BillingLog::create([
					'billing_id' => $user->billing['id'],
					'partner_id' => $chatRoomLog->chatRoom['lady_id'],
					'event'      => 'chat',
					'before'     => $user->billing['balance'] + $updateData['paid'],
					'difference' => $updateData['paid'],
					'after'      => $user->billing['balance'],
				]);
			}
		}
	}

	public static function clearOldData() {
		DB::table('chat_rooms_log')->whereRaw('`start_time` < DATE_SUB(UTC_TIMESTAMP, INTERVAL 3 MONTH)')->delete();
	}

	public function setUpdatedAt($value) {
		//Do-nothing
	}

	public function setCreatedAt($value) {
		//Do-nothing
	}

	public function getUpdatedAtColumn() {
		//Do-nothing
	}

	public function getCreatedAtColumn() {
		//Do-nothing
	}
}
