<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFavorites extends Model {

//	protected $table = 'user_favorites';

	public $timestamps = FALSE;

	protected $fillable = [
		'fav_id',
	];

	public function user() {
		return $this->belongsTo('App\Models\User');
	}

	/**
	 * @return User
	 */
	public function getFavoriteUser() {
		return User::find($this->fav_id);
	}
}
