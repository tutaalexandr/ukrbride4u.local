<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model {

	protected $table = 'billing';

//	public $timestamps = true;

	protected $fillable = [
		'balance',
	];

	public static function calcPaidForChatRoomsLog() {
		ChatRoomsLog::calcPaidForChatRoomsLog();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user() {
		return $this->belongsTo('App\Models\User');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function billingLog() {
		return $this->hasMany('App\Models\BillingLog');
	}
}
