<?php namespace App\Models;

use Cache;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'about_me',
		'address',
		'main_photo',
		'bdate',
		'email',
		'en_level',
		'eyes',
		'hair',
		'height',
		'locale',
		'name',
		'password',
		'phone',
		'rights',
		'sex',
		'viewer',
		'weight',
		'youtube',
		'zodiac',
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	public function favorites() {
		return $this->hasMany('App\Models\UserFavorites');
	}

	public function photos() {
		return $this->hasMany('App\Models\UserPhotos');
	}

	public function billing() {
		return $this->hasOne('App\Models\Billing');
	}

	public function billingLogPartner() {
		return $this->hasMany('App\Models\Billing', 'partner_id');
	}

	public function getPhotos() {
		$res = [];
		foreach ($this->photos()->getResults() as $o) {
			/**
			 * @var $o UserPhotos
			 */
			if (file_exists(public_path() . $o->getAttributes()['img']))
				$res[] = $o->getAttributes();
			else
				$o->delete();
		}
		return $res;
	}

	public function getZodiacList() {
		return Cache::rememberForever("users_zodiac_list", function () {
			return getDBEnumListForColumn($this->table, 'zodiac');
		});
	}

	public function getEyesList() {
		return Cache::rememberForever("users_eyes_list", function () {
			return getDBEnumListForColumn($this->table, 'eyes');
		});
	}

	public function getHairList() {
		return Cache::rememberForever("users_hair_list", function () {
			return getDBEnumListForColumn($this->table, 'hair');
		});
	}

	public function getEnLevelList() {
		return Cache::rememberForever("users_en_level_list", function () {
			return getDBEnumListForColumn($this->table, 'en_level');
		});
	}

	public function getRightsList() {
		return Cache::rememberForever("users_rights_list", function () {
			return getDBEnumListForColumn($this->table, 'rights');
		});
	}

	public function updateLastVisit() {
		$this->last_visit_at = $this->freshTimestamp();
		$this->timestamps = FALSE;
		$this->save();
		$this->timestamps = TRUE;
	}

	public function getFavoritesUsers($limit = 50) {
		return DB::table('user_favorites')
				 ->select('user_favorites.id AS favorite_id', 'users.id AS id', 'users.main_photo AS main_photo', 'users.name AS name', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `online`')
				 ->leftJoin('users', 'users.id', '=', 'user_favorites.fav_id')
				 ->where('user_favorites.user_id', '=', $this->id)
				 ->orderBy('users.name')
				 ->take($limit)
				 ->get();
	}

	public function checkFavoriteExist($fav_id) {
		return DB::table('user_favorites')->where('user_favorites.user_id', '=', $this->id)->where('user_favorites.fav_id', '=', (int) $fav_id)->exists();
	}

	public function getOnlineUsersList($exclude_id = 0, $limit = 2) {
		return DB::table('users')
				 ->select('users.id AS id', 'users.main_photo AS main_photo', 'users.name AS name', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `online`')
				 ->where('users.id', '!=', $exclude_id)
				 ->where('users.sex', '!=', $this->sex)
				 ->where('users.rights', '!=', 'superadmin')
				 ->where('users.rights', '!=', 'admin')
				 ->whereRaw('TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time'))
				 ->orderBy(DB::raw('RAND()'))
				 ->take($limit)
				 ->get();
	}
	public function getAllLadyList($exclude_id = 0) {
		return DB::table('users')
			->select('users.id AS id', 'users.main_photo AS main_photo', 'users.name AS name', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
			->where('users.id', '!=', $exclude_id)
			->where('users.sex', '!=', 'm')
			->where('users.rights', '!=', 'superadmin')
			->where('users.rights', '!=', 'admin')
			->get();
	}
	public function getChoiseAdminUsersList($exclude_id = array()) {
		return DB::table('users')
			->select('users.id AS id', 'users.main_photo AS main_photo', 'users.name AS name', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
			->whereIn('id', $exclude_id )
			->where('users.sex', '=', 'w')
			->where('users.rights', '!=', 'superadmin')
			->where('users.rights', '!=', 'admin')
			->get();
	}
	public function setAddChoiseAdminUsersList($id)
	{
		ChoiseAdminUsers::create([
			'id'         => $id,
			'sex'         => 'w',
		]);
	}
	public function deletAddChoiseAdminUsersList($id)
	{
		DB::table('choise_admin_users')->where('id', '=', $id )->delete();
	}
	public function setAdminChatUsersOnline() {
		$users_cheise = $this::getAddChoiseAdminUsersList();
		$arr_id = array();
		foreach ($users_cheise as $item => $value) {
			$arr_id[] = $value->id;
		}
		DB::table('users')
			->whereIn('id', $arr_id )
			->update(['last_visit_at' => $this->freshTimestamp()]);
	}
	public function getAddChoiseAdminUsersList()
	{
		$choise_admin_users = DB::table('choise_admin_users')->get();
		return $choise_admin_users;
	}
	public function getChoiseAdminUsersListAll($exclude_id = 0 ) {
		return DB::table('users')
			->select('users.id AS id', 'users.main_photo AS main_photo', 'users.name AS name', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
			->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `online`')
			->where('users.id', '!=', $exclude_id)
			->where('users.sex', '=', 'w')
			->where('users.rights', '!=', 'superadmin')
			->where('users.rights', '!=', 'admin')
			->orderBy('users.name', 'ASC')
			//->take($limit)
			->get();
	}
	public function getUserInfoById($id) {
		return DB::table('users')
				 ->select('users.id AS id', 'users.email AS email', 'users.main_photo AS main_photo', 'users.name AS name', 'users.address AS address', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `online`')
				 ->where('users.id', '=', (int) $id)
				 ->first();
	}

	public function getUserStatusById($id) {
		$res = DB::table('users')
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `online`')
				 ->where('users.id', '=', (int) $id)
				 ->first();
		return (bool) $res->online;
	}

	public function getUsersListBySex($sex) {
		return DB::table('users')
				 ->select('users.id AS id', 'users.name AS name', 'users.email AS email')
				 ->where('users.sex', '=', $sex)
				 ->where('users.rights', '!=', 'superadmin')
				 ->where('users.rights', '!=', 'admin')
				 ->orderBy('users.name', 'ASC')
				 ->get();
	}

	public function setAllUsersOnline() {
		DB::table('users')->update(['last_visit_at' => $this->freshTimestamp()]);
	}

	public function getSearchUsersList($data, $limit = 50) {
		$whereSql = "";

		if ($data['fromAge'] > 0)
			$whereSql .= "AND TIMESTAMPDIFF(YEAR, bdate, CURDATE()) >= {$data['fromAge']}\r\n";
		if ($data['toAge'] > 0 && $data['toAge'] > $data['fromAge'])
			$whereSql .= "AND TIMESTAMPDIFF(YEAR, bdate, CURDATE()) <= {$data['toAge']}\r\n";

		if ($data['fromHeight'] > 0)
			$whereSql .= "AND height >= {$data['fromHeight']}\r\n";
		if ($data['toHeight'] > 0 && $data['toHeight'] > $data['fromHeight'])
			$whereSql .= "AND height <= {$data['toHeight']}\r\n";

		if ($data['fromWeight'] > 0)
			$whereSql .= "AND weight >= {$data['fromWeight']}\r\n";
		if ($data['toWeight'] > 0 && $data['toWeight'] > $data['fromWeight'])
			$whereSql .= "AND weight <= {$data['toWeight']}\r\n";

		if (!empty($data['address']))
			$whereSql .= "AND address LIKE '%{$data['address']}%'\r\n";

		if (!empty($data['en_level']))
			$whereSql .= "AND en_level = '{$data['en_level']}'\r\n";

		if (!empty($data['hair']))
			$whereSql .= "AND hair = '{$data['hair']}'\r\n";

		if (!empty($data['eyes']))
			$whereSql .= "AND eyes = '{$data['eyes']}'\r\n";

		if ($data['withVideo'])
			$whereSql .= "AND youtube != ''\r\n";

		if ($data['isOnline'])
			$whereSql .= "AND (TIMESTAMPDIFF(MINUTE, last_visit_at, UTC_TIMESTAMP()) < " . \Config::get('auth.user_online_time') . ") = 1\r\n";


		$sql = "
			SELECT
				id,
				main_photo,
				name,
				sex,
				about_me,
				TIMESTAMPDIFF(YEAR, bdate, CURDATE()) AS age,
				(TIMESTAMPDIFF(MINUTE, last_visit_at, UTC_TIMESTAMP()) < " . \Config::get('auth.user_online_time') . ") AS online
			FROM users
			WHERE
				sex != '$this->sex' AND
				rights != 'superadmin' AND
				rights != 'admin'
				$whereSql
			ORDER BY name
			LIMIT 0, $limit
		;";

		return DB::select($sql);
	}

	public function getACAddressList($ss) {
		$res = [];
		foreach (DB::select("SELECT address FROM {$this->table} WHERE address LIKE '%$ss%' LIMIT 0, 20;") AS $obj)
			$res[] = $obj->address;
		return $res;
	}

	public function getLadiesList($limit = 3) {
		return DB::table('users')
				 ->select('users.id AS id', 'users.main_photo AS main_photo', 'users.name AS name', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
				 ->where('users.sex', '!=', 'm')
				 ->where('users.rights', '!=', 'superadmin')
				 ->where('users.rights', '!=', 'admin')
				 ->orderBy(DB::raw('RAND()'))
				 ->take($limit)
				 ->get();
	}

	public function searchUser($mail) {
		$user = DB::table('users')->where('email', $mail)->first();
		return $user;
	}

	public function getGiftInfoById($isreal) {
		return DB::table('gifts')->where('isreal', '=', $isreal)->get();
	}
	public function getGiftInfoByIdArray($isreal , $id) {
		return DB::table('gifts')->where('isreal', '=', $isreal)->where('id', '=', $id )->get();
	}
	public function getUserInfoByIdForGift($id) {
		return DB::table('users')
				 ->select('users.hair AS hair', 'users.eyes AS eyes', 'users.id AS id', 'users.main_photo AS main_photo', 'users.name AS name', 'users.address AS address', 'users.sex AS sex', 'users.about_me AS about_me', DB::raw("TIMESTAMPDIFF(YEAR, users.bdate, CURDATE()) AS `age`"))
				 ->selectRaw('(TIMESTAMPDIFF(MINUTE, `users`.`last_visit_at`, UTC_TIMESTAMP()) < ' . \Config::get('auth.user_online_time') . ') AS `online`')
				 ->where('users.id', '=', (int) $id)
				 ->first();
	}

	public function getUserInfoByIdForProfile($id) {
		return DB::table('users')->where('users.id', '=', (int) $id)->first();
	}

	public function getFotoByIdForProfile($id) {
		return DB::table('user_photos')->where('user_id', '=', (int) $id)->get();
	}
}
