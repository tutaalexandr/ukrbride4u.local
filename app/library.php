<?php

/**
 * Get enum list from DB
 *
 * @param $table  string Table name
 * @param $column string Column name
 *
 * @return array
 */
function getDBEnumListForColumn($table, $column) {
	preg_match_all('/\'([^\']+?)\'/', DB::selectOne("SHOW COLUMNS FROM `$table` LIKE '{$column}'")->Type, $res);
	return $res[1];
}

/**
 * @param $valuesList     array Data for preparation
 * @param $nullFieldsList array Null fields list (keys)
 */
function parseNullableValues(&$valuesList, $nullFieldsList) {
	foreach ($valuesList AS $key => $value)
		if (in_array($key, $nullFieldsList))
			$valuesList[$key] = empty($value) ? NULL : $value;
}

/**
 * Recursivelly rmdir
 *
 * @param $path string Path to remove dir
 */
function rrmdir($path) {
	if ($objs = glob($path . "/*"))
		foreach ($objs as $obj)
			is_dir($obj) ? rrmdir($obj) : unlink($obj);
	rmdir($path);
}

/**
 * @param string $lang Needed language
 *
 * @return string URL with needed lang
 */
function getCurrentURLWithLang($lang = NULL) {
	$url = Request::segments();
	if (isset($url[0]))
		$url[0] = empty($lang) ? Lang::getLocale() : $lang;
	return asset(implode('/', $url));
}

/**
 * Get current controller name
 * @return string
 */
function getCurrentControllerName() {
	return preg_replace('/.*\\\/', '', explode('@', \Route::currentRouteAction())[0]);
}

/**
 * Get current controller method
 * @return string
 */
function getCurrentMethodName() {
	return explode('@', \Route::currentRouteAction())[1];
}

function execTime($sec = NULL) {
	if (isset($sec))
		set_time_limit($sec);
	return (int) ini_get("max_execution_time");
}
