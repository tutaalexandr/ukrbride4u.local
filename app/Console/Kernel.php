<?php namespace App\Console;
use App\Models\User;
use App\Models\Billing;
use App\Models\BillingLog;
use App\Models\Chat;
use App\Models\ChatRooms;
use App\Models\ChatRoomsLog;
use App\Models\Mail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
	];
	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule  php artisan migrate --force
	 *
	 * @return void https://api.telegram.org/bot229793781:AAEvfpsOca0BGJB3acmGPjn8LqPPxxQ7sls/getMe
	 */
	protected function schedule(Schedule $schedule) {
		$schedule->call(function () {
			ChatRooms::stopOldChatRooms();
			Billing::calcPaidForChatRoomsLog();
			ChatRooms::stopChatRoomsWhereUserNoMoney();
			$a = new User();
			$a->setAdminChatUsersOnline();
		})->cron('* * * * *');
		$schedule->call(function () {
			ChatRoomsLog::clearOldData();
			BillingLog::clearOldData();
			Chat::clearOldData();
			Mail::clearOldData();
		})->cron('0 0 * * *');
	}


}
