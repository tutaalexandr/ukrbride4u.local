<?php namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\BillingLog;
use App\Models\Mail;
use App\Models\User;
use App\Services\ChatService;
use Illuminate\Http\Request;
use Image;
use Input;
use Validator;

class MailController extends Controller {


	/**
	 * Create a new controller instance.
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	private function saveUserImage($file, $imgName) {
		$imageDirUrl = "/upload/mailfotos/";
		$imageDirPath = public_path() . $imageDirUrl;
		$imagePath = $imageDirPath . '/' . $imgName;

		if (empty($file))
			if (file_exists($imagePath))
				return $imageDirUrl . $imgName;
			else
				return NULL;

		@mkdir($imageDirPath, 0755, TRUE);

		if (file_exists($imagePath))
			unlink($imagePath);
		Image::make($file)->resize(650, 650, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		})->save($imagePath);

		return $imgName;
	}

	private static function generateMicroTimeString() {
		$mtime = explode(" ", microtime());
		$msec = explode(".", $mtime[0]);
		return $mtime[1] . '.' . $msec[1];
	}

	/**
	 * Send a letter.
	 *
	 * @return \Response
	 */
	public function postSend(Request $request) {
		/**
		 * @var $user User
		 */
		$user = $request->user();

		$user->updateLastVisit();

		$validator = Validator::make($request->only('user_id', 'partner_id', 'msg', 'image'), [

			'partner_id' => 'integer|required',
			'msg'        => 'required|min:1|max:2500',
			'image'      => 'image',

		]);
		if ($validator->fails())
			return redirect()->back()->withInput($request->input())->withErrors($validator, 'letter');

		$imgName = $this->saveUserImage(Input::file('image'), self::generateMicroTimeString() . '.png');
		if (!isset($imgName)) {
			$imgName = "";
		}
		$data = [
			'user_id'       => $user->id,
			'partner_id'    => (int) $request->get('partner_id'),
			'msg'           => ChatService::prepareInputMsg($request->get('msg'), FALSE, FALSE),
			'Imagedounload' => $imgName,
		];

		Mail::create($data);

		if ($user->sex == 'm') {
			$amount = \Config::get('prices.mail');
			$user_balance = $user->billing['balance'];

			$user->billing()->update(['balance' => $user_balance - $amount]);
			BillingLog::create([
				'billing_id' => $user->billing['id'],
				'partner_id' => (int) $request->get('partner_id'),
				'event'      => 'mail',
				'before'     => $user_balance,
				'difference' => $amount,
				'after'      => $user_balance - $amount,
			]);
		}
		//print_r($user->sex); exit();
		//if($user->sex == 'w') {
			$men_info = $user->getUserInfoById((int)$request->get('partner_id'));
			$mesege = "You have new mail from " . $user->name ;
			mail($men_info->email, "Ukrbride4u you have new mail", $mesege);
		//}

		return redirect(\Lang::getLocale() . '/cabinet/sent-list');
	}

	/**
	 * Delete letter from sent list, move letter to deleted list
	 *
	 * @return \Response
	 */
	public function postLetterDelete(Request $request) {
		$request->user()->updateLastVisit();

		$data = [
			'user_id' => $request->user()->id,
			'msg_id'  => (int) $request->get('msg_id'),
		];

		/**
		 * @var $mail Mail
		 */
		if (!$mail = Mail::find($data['msg_id']))
			return redirect(\Lang::getLocale() . '/cabinet/deleted-list')->with([
				'flashMsg' => trans('cabinet.letter_not_found'),
			]);

		if ($mail->user_id != $data['user_id'] && $mail->partner_id != $data['user_id'])
			return redirect(\Lang::getLocale() . '/cabinet/deleted-list')->with([
				'flashMsg' => trans('cabinet.letter_access_violiation'),
			]);

		// move letter to deleted list
		if ($mail->user_id == $data['user_id'])
			$mail->user_delete = 1;
		else
			$mail->partner_delete = 1;
		$mail->save();
		return redirect(\Lang::getLocale() . '/cabinet/deleted-list')->with([
			'flashMsg' => trans('cabinet.letter_moved_to_delete_list'),
		]);
	}

	/**
	 * Erase letter from deleted list
	 *
	 * @return \Response
	 */
	public function postLetterErase(Request $request) {
		$request->user()->updateLastVisit();

		$data = [
			'user_id' => $request->user()->id,
			'msg_id'  => (int) $request->get('msg_id'),
		];

		/**
		 * @var $mail Mail
		 */
		if (!$mail = Mail::find($data['msg_id']))
			return redirect(\Lang::getLocale() . '/cabinet/deleted-list')->with([
				'flashMsg' => trans('cabinet.letter_not_found'),
			]);

		if ($mail->user_id != $data['user_id'] && $mail->partner_id != $data['user_id'])
			return redirect(\Lang::getLocale() . '/cabinet/deleted-list')->with([
				'flashMsg' => trans('cabinet.letter_access_violiation'),
			]);

		// erase letter
		if ($mail->user_id == $data['user_id'])
			$mail->user_erase = 1;
		else
			$mail->partner_erase = 1;

		if ($mail->user_erase == 1 && $mail->partner_erase == 1) {
			$mail->delete();
			if (file_exists(public_path() . "/upload/mailfotos/" . $mail->Imagedounload)) {
				unlink(public_path() . "/upload/mailfotos/" . $mail->Imagedounload);
			}
		}
		else {
			$mail->save();
		}

		return redirect(\Lang::getLocale() . '/cabinet/deleted-list')->with([
			'flashMsg' => trans('cabinet.letter_erased'),
		]);
	}

	public function postSendgifts(Request $request, Mail $MailModel) {
		$request->user()->updateLastVisit();

		if ($request->input('gifts') == 0)
			return redirect()->back()->withInput($request->input());

		if (count($request->input('gifts')) > 0) {
			$gifts = implode(",", $request->input('gifts'));
		}
		else {
			$gifts = $request->input('gifts');
		}

		$costBilling = $request->user()->billing['balance'];

		$costGifg = $MailModel->getCostGifts($request->input('gifts'));

		if ($costBilling < $costGifg)
			return redirect()->back()->with(['flashMsg' => 'You not have Money',]);

		$costResult = $costBilling - $costGifg;

		$request->user()->billing()->update(['balance' => $costResult]);
		/*		$request->user()->billing()->update(['balance'=> $costResult]);	*/
		$data = [
			'user_id'      => $request->user()->id,
			'partner_id'   => (int) $request->get('partner_id'),
			'msg'          => ChatService::prepareInputMsg($request->get('msg'), FALSE, FALSE),
			'ArrayIdGifts' => $gifts,
		];
		$validator = Validator::make($data, [
			'user_id'    => 'integer|required',
			'partner_id' => 'integer|required',
			/*'msg'        => 'required',*/
		]);
		$gifts_text = '';
		foreach ($request->input('gifts') as $item=>$id){

			$gifts_text .= $request->user()->getGiftInfoByIdArray(0 , $id)[0]->title.", ";
		}
		if ($validator->fails())
			return redirect()->back()->withInput($request->input())->withErrors($validator, 'letter');

		BillingLog::create([
			'billing_id' => $request->user()->billing['id'],
			'partner_id' => (int) $request->get('partner_id'),
			'event'      => 'virt_gift',
			'gifts'      => $gifts_text,
			'before'     => $costBilling,
			'difference' => $costGifg,
			'after'      => $costResult,
		]);

		Mail::create($data);

		return redirect(\Lang::getLocale() . '/cabinet/sent-list');
	}


	public function postSendgiftsreal(Request $request, Mail $MailModel) {
		$request->user()->updateLastVisit();

		if ($request->input('gifts') == 0)
			return redirect()->back()->withInput($request->input());
		if (count($request->input('gifts')) > 0) {
			$gifts = implode(",", $request->input('gifts'));
		}
		else {
			$gifts = $request->input('gifts');
		}

		$costBilling = $request->user()->billing['balance'];

		$costGifg = $MailModel->getCostGifts($request->input('gifts'), 1);
		$costResult = $costBilling - $costGifg;

		if ($costBilling < $costGifg)
			return redirect()->back()->with(['flashMsg' => 'You not have Money',]);
		$request->user()->billing()->update(['balance' => $costResult]);
		$gifts_text = '';
		foreach ($request->input('gifts') as $item=>$id){

			$gifts_text .= $request->user()->getGiftInfoByIdArray(1 , $id)[0]->title.", ";
		}/*
		print_r($gifts_text);
		exit();*/
		$text = 'You have new mail from '.$request->user()->name.'.'.$request->user()->name.'
wants to buy a real gift ( '.$gifts_text.' ) for '.$request->user()->getUserInfoByIdForProfile((int) $request->get('partner_id'))->name.'.';
		mail('varchuk90@mail.ru', "Buy a real gift" , $text);
		//mail('tuta.alexandr@mail.ru', "Buy a real gift" , $text);
		BillingLog::create([
			'billing_id' => $request->user()->billing['id'],
			'partner_id' => (int) $request->get('partner_id'),
			'event'      => 'real_gift',
			'gifts'      => $gifts_text,
			'before'     => $costBilling,
			'difference' => $costGifg,
			'after'      => $costResult,
		]);
		return redirect()->back()->with(['flashMsg' => 'Thank you for your gift',]);
	}

}
