<?php namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\ChatRooms;
use App\Models\User;
use App\Services\ChatService;
use Illuminate\Http\Request;
use Validator;

class ChatController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return \Response
	 */
	public function getIndex() {
		return view('chat');
	}

	public function postGetAll(Request $request) {
		ChatService::printAllMsgs((int) $request->get('partner_id'));
	}

	private function isCommand(&$data) {
		if (!\Config::get('app.debug'))
			return FALSE;
		if (iconv_strlen($data['msg']) > 50)
			return FALSE;
		switch ($data['msg']) {
			case '/clear':
				Chat::clear($data['user_id'], $data['partner_id']);
				return TRUE;
			default:
				return FALSE;
		}
	}

	public function postAdd(Request $request) {
		$request->user()->updateLastVisit();

		/**
		 * @var $user User
		 */
		$user = $request->user();
		$partner_id = (int) $request->get('partner_id');

		$data = [
			'user_id'    => (int) $user->id,
			'partner_id' => $partner_id,
			'msg'        => ChatService::prepareInputMsg($request->get('chatMyMsg')),
		];
		$validator = Validator::make($data, [
			'user_id'    => 'integer|required',
			'partner_id' => 'integer|required',
			'msg'        => 'required|min:1|max:2500',
		]);
		if ($validator->fails())
			return;

		if ($this->isCommand($data))
			return;

		if ($user->sex == 'm')
			if (ChatRooms::needStartCheck($user->id, $partner_id))
				return;


		if ($user->sex == 'm')
			ChatRooms::restoreRoom($user->id, $partner_id);
		else
			ChatRooms::restoreRoom($partner_id, $user->id);

		Chat::create($data);
	}

	public function postAddAdmin(Request $request) {
		//$request->user()->updateLastVisit();

		/**
		 * @var $user User
		 */
		//$user = $request->user();
		$idledis = (int) $request->get('idledis');
		$partner_id = (int) $request->get('partner_id');

		$data = [
			'user_id'    => $idledis ,
			'partner_id' => $partner_id,
			'msg'        => ChatService::prepareInputMsg($request->get('chatMyMsg')),
		];


			ChatRooms::restoreRoom($partner_id, $idledis );

		Chat::create($data);
	}
}
