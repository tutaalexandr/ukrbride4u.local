<?php namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\BillingLog;
use App\Models\Chat;
use App\Models\ChatRooms;
use App\Models\Mail;
use App\Models\User;
use App\Models\ChoiseAdminUsers;
use App\Models\UserFavorites;
use App\Services\ChatService;
use App\Services\TwoCheckoutService;
use App\Services\PayPalService;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\News;

//use Illuminate\Http\Request;

class CabinetController extends Controller
{

    const TIME_LIMIT = 180;
    const TIME_TO_STOP_SCRIPT = 5;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Response
     */
    public function getIndex()
    {
        return \Redirect::to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'));
    }

    /**
     * @return \Response
     */
    public function postChecker(Request $request)
    {
        execTime(self::TIME_LIMIT);
        $endTime = time() + execTime() - self::TIME_TO_STOP_SCRIPT;

        $resData = [];
        $requestData = $request->all();
        $tmp = NULL;

        /**
         * @var $user User
         */
        $user = $request->user();

      //  while (TRUE) {
            // timer start time
            if (isset($requestData['to']) && isset($requestData['tst']) && $user->sex == 'm') {
                $facrt = ChatRooms::getFirstActiveChatRoomTime($request->user());
                $ffacrt = empty($facrt) ? '' : Carbon::createFromFormat('Y-m-d H:i:s', $facrt)->subMinute($requestData['to'])->format('Y-m-d H:i:s');
                if ($ffacrt != $requestData['tst'])
                    $resData['facrt'] = $ffacrt;
            }

            // get partner chat messages
            if (isset($requestData['gpcm'])) {
                $data = Chat::getMsgs($user->id, (int)$requestData['pid'], isset($requestData['lmid']) ? (int)$requestData['lmid'] : 0);
                if (count($data) > 0) {
                    Chat::setViewed($user->id, (int)$requestData['pid']);
                    $resData['pcms'] = ChatService::prepareMessages($data); // partner chat messages
                }
            }

            // mails count
            if (isset($requestData['mc'])) {
                $tmp = Mail::getNewMsgsCnt($user->id);
                if ($tmp != (int)$requestData['mc'])
                    $resData['nmc'] = $tmp; // new mails count
            }

            // chat msgs count
            if (isset($requestData['cmc'])) {
                $tmp = Chat::getNewMsgsCnt($user->id);
                if ($tmp != (int)$requestData['cmc'])
                    $resData['ncmc'] = $tmp; // new chat messages count
            }
            if (isset($requestData['cmcnews'])) {
               // $tmp = Chat::getNewMsgsCnt($user->id);
                $tmp = Chat::getNewNews($user->id);
               /* $tmp = \DB::table('view_news')
                    ->where('id_user', '=', $user->id)
                    ->get();*/
              // $tmp = $user->id;
                //$tmp = 0 ;
                if (($tmp !=  $requestData['cmcnews'] )){
                   // $tmp = 1 ;
                    $resData['ncmcnews'] = $tmp; // new chat messages count
                }
            }

            // chat partner status
            if (isset($requestData['cps'])) {
                $tmp = (int)$user->getUserStatusById((int)$requestData['pid']);
                if ($tmp != (int)$requestData['cps'])
                    $resData['ncps'] = $tmp; // new chat partner status
            }

            // chat rooms key list
            if (isset($requestData['crkl'])) {
                $tmp = (string)ChatRooms::getChatRoomsKeyList($user);
                if ($tmp != (string)$requestData['crkl'])
                    $resData['nrkl'] = $tmp; // new room key list
            }

            // chat history rooms key list
            if (isset($requestData['chrkl'])) {
                $tmp = (string)ChatRooms::getChatHistoryRoomsKeyList($user);
                if ($tmp != (string)$requestData['chrkl'])
                    $resData['nhrkl'] = $tmp; // new room key list
            }

            // chat rooms is active
            if (isset($requestData['cria']) && $user->sex != 'w') {
                $tmp = ChatRooms::needStartCheck($user->id, (int)$requestData['pid']) ? 0 : 1;
                if ($tmp != (int)$requestData['cria'])
                    $resData['nscbs'] = $tmp; // new start chat button status
            }


           // if (time() >= $endTime || !empty($resData))
             //   break;
           // else
            //    sleep(1);
        //}

        return json_encode($resData, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Profile page.
     *
     * @return \Response
     */
    public function getProfile(Request $request, User $UserModel)
    {
        $profile_id = (int)$request->segment(4);

        \Session::put('chat_previous_url', '/cabinet/profile/' . ($profile_id ? $profile_id : ''));

        if ($profile_id == 0)
            return view('cabinet.profile-edit', [
                'user' => array_only(Auth::getUser()->getAttributes(), [
                    'name',
                    'phone',
                    'bdate',
                    'address',
                    'main_photo',
                    'weight',
                    'height',
                    'zodiac',
                    'eyes',
                    'hair',
                    'en_level',
                    'about_me',
                    'youtube',
                ]),
                'photos' => Auth::getUser()->getPhotos(),
                'zodiacList' => Auth::getUser()->getZodiacList(),
                'eyesList' => Auth::getUser()->getEyesList(),
                'hairList' => Auth::getUser()->getHairList(),
                'enLevelList' => Auth::getUser()->getEnLevelList(),
            ]);
        else
            $userFoto = $UserModel->getFotoByIdForProfile($profile_id);
        $userProfile = $UserModel->getUserInfoByIdForProfile($profile_id);
        return view('cabinet.profile', ['userProfile' => $userProfile, 'userFoto' => $userFoto]);
    }

    /**
     * Edit account page.
     *
     * @return \Response
     */
    public function getAccount()
    {
        \Session::put('chat_previous_url', '/cabinet/account/');

        return view('cabinet.account', [
            'user' => array_only(Auth::getUser()->getAttributes(), ['email',]),
        ]);
    }

    /**
     * Search page.
     *
     * @return \Response
     */
    public function getSearch(Request $request)
    {
        \Session::put('chat_previous_url', '/cabinet/search/');

        return view('cabinet.search', [
            'eyesList' => Auth::getUser()->getEyesList(),
            'hairList' => Auth::getUser()->getHairList(),
            'enLevelList' => Auth::getUser()->getEnLevelList(),
            'usersList' => \Session::has('searchUsersList') ? \Session::get('searchUsersList') : [],
        ]);
    }

    /**
     * Search page.
     *
     * @return \Response
     */
    public function postSearch(Request $request)
    {
        $data = $request->only([
            'fromAge',
            'toAge',
            'fromHeight',
            'toHeight',
            'fromWeight',
            'toWeight',
            'address',
            'en_level',
            'hair',
            'eyes',
            'withVideo',
            'isOnline',
        ]);

        /**
         * @var $user User
         */
        $user = $request->user();

        $validator = Validator::make($data, [
            'fromAge' => 'integer',
            'toAge' => 'integer',
            'fromHeight' => 'integer',
            'toHeight' => 'integer',
            'fromWeight' => 'integer',
            'toWeight' => 'integer',
            'address' => 'min:3|max:250',
            'en_level' => 'min:2|max:50|in:' . implode(",", $user->getEnLevelList()),
            'hair' => 'min:2|max:50|in:' . implode(",", $user->getHairList()),
            'eyes' => 'min:2|max:50|in:' . implode(",", $user->getEyesList()),
        ]);

        if ($validator->fails())
            return redirect()->to(\Lang::getLocale() . '/cabinet/search')->withInput($request->input())->withErrors($validator);

        $data['fromAge'] = (int)$data['fromAge'];
        $data['toAge'] = (int)$data['toAge'];
        $data['fromHeight'] = (int)$data['fromHeight'];
        $data['toHeight'] = (int)$data['toHeight'];
        $data['fromWeight'] = (int)$data['fromWeight'];
        $data['toWeight'] = (int)$data['toWeight'];
        $data['address'] = (string)$data['address'];
        $data['en_level'] = (string)$data['en_level'];
        $data['hair'] = (string)$data['hair'];
        $data['eyes'] = (string)$data['eyes'];
        $data['withVideo'] = isset($data['withVideo']);
        $data['isOnline'] = isset($data['isOnline']);

        return redirect()->to(\Lang::getLocale() . '/cabinet/search')->withInput($request->input())->with([
            'searchUsersList' => $user->getSearchUsersList($data, 200),
        ]);
    }

    /**
     * Write letter page
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getWriteLetter(Request $request)
    {
        if (($request->segment(4) == NULL) || !ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => trans('cabinet.letter_incorrect_partner'),
            ]);

        \Session::put('chat_previous_url', '/cabinet/write-letter/' . $request->segment(4));

        $data = [
            'partner_id' => (int)$request->segment(4),
            'partner_info' => $request->user()->getUserInfoById($request->segment(4)),
        ];

        return view('cabinet.write-letter')->with($data);
    }

    /**
     * @param      $user       User
     * @param      $partner_id int
     * @param null $active int 0 or 1
     */
    private function updateChatRoom($user, $partner_id, $active = NULL)
    {
        if ($partner_id == 0)
            return;

        $data = [];
        if ($user->sex == 'm') {
            $data['man_id'] = $user->id;
            $data['lady_id'] = $partner_id;
            if ($active !== NULL && is_int($active)) {
                $data['active'] = $active;
                $data['man_delete'] = 0;
                $data['man_erase'] = 0;
            }
        } else {
            $data['man_id'] = $partner_id;
            $data['lady_id'] = $user->id;
            if ($active !== NULL && is_int($active)) {
//				$data['active'] = $active;
                $data['lady_delete'] = 0;
                $data['lady_erase'] = 0;
            }
        }

        $chatRoom = ChatRooms::updateOrCreate(array_only($data, ['man_id', 'lady_id']), $data);
        if ($active === 0 || $active === 1) {
            $chatRoomLog = $chatRoom->chatRoomLog()->firstOrNew([
                'end_time' => NULL,
            ]);
            if ($active == 1)
                $chatRoomLog->start_time = \DB::raw('UTC_TIMESTAMP()');
            if ($active == 0)
                $chatRoomLog->end_time = \DB::raw('UTC_TIMESTAMP()');
            $chatRoomLog->save();
        }
    }

    /**
     * Chat page
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getChat(Request $request)
    {
        if (!ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . '/cabinet/chat')->with([
                'flashMsg' => trans('cabinet.chat_incorrect_partner'),
            ]);

        $partner_id = (int)$request->segment(4);

        $this->updateChatRoom($request->user(), $partner_id);

        $partnerInfo = $request->user()->getUserInfoById($partner_id);

        $data = [
            'chat_previous_url' => \Session::get('chat_previous_url'),
            'chatRoomsList' => ChatRooms::getChatRoomsList($request->user()),
            'chatRoomsKeyList' => ChatRooms::getChatRoomsKeyList($request->user()),
            'onlineUsersList' => $request->user()->getOnlineUsersList($partner_id),
            'partner_id' => $partner_id,
            'partner_info' => $partnerInfo,
            'smiles_list' => ChatService::getSmiles(),
            'needStart' => ChatService::needStartCheck($partner_id),
            'needStop' => ChatService::needStopCheck($partner_id),
            'canStartChat' => (empty($partnerInfo->online) ? FALSE : $partnerInfo->online == 1) && ChatRooms::checkBillingForStartNewChat($request->user()),
        ];

        return view('cabinet.chat')->with($data);
    }

    public function getChatAdmin(Request $request)
    {
        if (!ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . '/cabinet/chat-admin')->with([
                'flashMsg' => trans('cabinet.chat_incorrect_partner'),
            ]);

        $partner_id = (int)$request->segment(4);



        $partnerInfo = $request->user()->getUserInfoById($partner_id);
        $arr_choise_admin_users = $request->user()->getAddChoiseAdminUsersList();
        $arr_id = array();
        foreach ($arr_choise_admin_users as $item => $value) {
            $arr_id[] = $value->id;
        }

        $mass_arr_mass = [];
        foreach ($arr_id as $item_arr_id => $value_arr_id) {
            $ledi = ChatRooms::getChatRoomsListAdmin($value_arr_id);
            $count = 0;
            foreach ($ledi as $item => $value) {
                $count += $value->new_msgs_cnt;
            }
            $mass_arr_mass[$value_arr_id] = $count;

        }
        /*     echo "<pre>";
                     print_r( $arr_id[0]  );
                     echo "</pre>";
                     exit();*/
        /*		echo "<pre>";
                //$d = $request->user()->getChoiseAdminUsersList();
                //print_r($d);
              //  print_r($request->user());
              //  print_r(ChatRooms::getChatRoomsListAdmin(37));
               // $d  = ChatRooms::getChatRoomsListAdmin();

                foreach ($request->user()->getChoiseAdminUsersList(37) as $item=>$value){
                    echo $value->id.'<br>';
                    echo $value->sex.'<br>';
                    $d1 = (object)array('id' =>$value->id , 'sex' =>$value->sex) ;
                    print_r(ChatRooms::getChatRoomsList($d1));
                        }
                echo "</pre>";
                exit();*/

        if ($request->get('idledis')) {

            $value->id = $request->get('idledis');
            $datauser = (object)array('id' => $value->id, 'sex' => 'w');
            $this->updateChatRoom($datauser, $partner_id);
            $data = [
                'chat_previous_url' => \Session::get('chat_previous_url'),
                'chatRoomsList' => ChatRooms::getChatRoomsList($datauser),
                'chatRoomsKeyList' => ChatRooms::getChatRoomsKeyList($datauser),
                'onlineUsersList' => $request->user()->getOnlineUsersList($partner_id , 100),
                'partner_id' => $partner_id,
                'idledis' => $value->id,
                'partner_info' => $partnerInfo,
                'smiles_list' => ChatService::getSmiles(),
                'needStart' => ChatService::needStartCheck($partner_id),
                'needStop' => ChatService::needStopCheck($partner_id),
                'canStartChat' => (empty($partnerInfo->online) ? FALSE : $partnerInfo->online == 1) && ChatRooms::checkBillingForStartNewChat($request->user()),

            ];
        } else {
            if (isset($arr_id[0]) && !empty($arr_id[0])) {

                $datauser = (object)array('id' => $arr_id[0], 'sex' => 'w');
            } else {
                $datauser = $request->user();
            }

            // $datauser = (object)array('id' => $value->id, 'sex' => 'w');
            $data = [
                'chat_previous_url' => \Session::get('chat_previous_url'),
                'chatRoomsList' => ChatRooms::getChatRoomsList($datauser),
                'chatRoomsKeyList' => ChatRooms::getChatRoomsKeyList($datauser),
                'onlineUsersList' => $request->user()->getOnlineUsersList($partner_id),
                'partner_id' => $partner_id,
                'idledis' => $datauser->id,
                'partner_info' => $partnerInfo,
                'smiles_list' => ChatService::getSmiles(),
                'needStart' => ChatService::needStartCheck($partner_id),
                'needStop' => ChatService::needStopCheck($partner_id),
                'canStartChat' => (empty($partnerInfo->online) ? FALSE : $partnerInfo->online == 1) && ChatRooms::checkBillingForStartNewChat($request->user()),

            ];
            $addtourlid = '?idledis=' . $datauser->id;
            return redirect()->to(\Lang::getLocale() . '/cabinet/chat-admin' . $addtourlid);

        }
        $arr_id_2 = $request->user()->getChoiseAdminUsersList($arr_id);
        function sort_nested_arrays( $array, $args = array('votes' => 'desc') ){
            usort( $array, function( $a, $b ) use ( $args ){
                $res = 0;

                $a = (object) $a;
                $b = (object) $b;

                foreach( $args as $k => $v ){
                    if( $a->$k == $b->$k ) continue;

                    $res = ( $a->$k < $b->$k ) ? -1 : 1;
                    if( $v=='desc' ) $res= -$res;
                    break;
                }

                return $res;
            } );

            return $array;
        }
        foreach ($arr_id_2 as $key => $value){
            $chat_data =  Chat::getLastMsgs($value->id);
            foreach ($chat_data as $key_d=>$value_d){
                $value->updated_at = $value_d->created_at;
            }
            if(!isset($value->updated_at)){
                $value->updated_at = 0;
            }
        }
        /*echo '<pre>';
        print_r($arr_id_2);
        // print_r($arr_id_2);
        echo '</pre>';exit();*/
        $arr_id_2 = sort_nested_arrays( $arr_id_2, array('updated_at' => 'desc') );
        $data['girl_wrap_choise'] = $arr_id_2;
        $data['mass_arr_mass'] = $mass_arr_mass;
        $data['girl_wrap'] = $request->user()->getChoiseAdminUsersListAll();
        return view('cabinet.chat-admin')->with($data);
    }

    public function getWriteAdd(Request $request)
    {
        $id = (int)$request->segment(4);
        $request->user()->setAddChoiseAdminUsersList($id);
        return redirect()->to(\Lang::getLocale() . '/cabinet/chat-admin');
    }

    public function getWriteDelete(Request $request)
    {
        $id = (int)$request->segment(4);
        $request->user()->deletAddChoiseAdminUsersList($id);
        return redirect()->to(\Lang::getLocale() . '/cabinet/chat-admin');
    }

    public function postCheckerAdmin(Request $request)
    {
        execTime(self::TIME_LIMIT);
        $endTime = time() + execTime() - self::TIME_TO_STOP_SCRIPT;

        $resData = [];
        $requestData = $request->all();
        $tmp = NULL;

        /**
         * @var $user User
         */
        // $user = $request->user();
        $user = $request->user()->getUserInfoById($requestData['idledis']);
        $idledis = $requestData['idledis'];

        /*while (TRUE) {*/
            // get partner chat messages
            if (isset($requestData['gpcm'])) {
                $data = Chat::getMsgs($user->id, (int)$requestData['pid'], isset($requestData['lmid']) ? (int)$requestData['lmid'] : 0);
                if (count($data) > 0) {
                    Chat::setViewed($idledis, (int)$requestData['pid']);
                    $resData['pcms'] = ChatService::prepareMessages($data , $user->id); // partner chat messages
                }
            }
            // mails count
            if (isset($requestData['mc'])) {
                $tmp = Mail::getNewMsgsCnt($idledis);
                if ($tmp != (int)$requestData['mc'])
                    $resData['nmc'] = $tmp; // new mails count
            }
            // chat msgs count
            if (isset($requestData['cmc'])) {
                $tmp = Chat::getNewMsgsCnt($idledis);
                if ($tmp != (int)$requestData['cmc'])
                    $resData['ncmc'] = $tmp; // new chat messages count
            }
            // no job need check
            // chat partner status
            /*   if (isset($requestData['cps'])) {
                   $tmp = (int)$user->getUserStatusById((int)$requestData['pid']);
                   if ($tmp != (int)$requestData['cps'])
                       $resData['ncps'] = $tmp; // new chat partner status
               }*/
            // chat rooms key list
            if (isset($requestData['crkl'])) {
                $tmp = (string)ChatRooms::getChatRoomsKeyList($user);
                if ($tmp != (string)$requestData['crkl'])
                    $resData['nrkl'] = $tmp; // new room key list
            }
            // chat history rooms key list
            if (isset($requestData['chrkl'])) {
                $tmp = (string)ChatRooms::getChatHistoryRoomsKeyList($user);
                if ($tmp != (string)$requestData['chrkl'])
                    $resData['nhrkl'] = $tmp; // new room key list
            }
       /*     if (time() >= $endTime || !empty($resData))
                break;
            else
                sleep(1);
        }*/

        return json_encode($resData, JSON_UNESCAPED_UNICODE);
    }

    public function postRoomsListAdmin(Request $request)
    {
        $user = (object)array('id' => (int)$request->get('idledis'), 'sex' => 'w');
        return view('cabinet.inc.chat-rooms-admin')->with([
            'chatRoomsList' => ChatRooms::getChatRoomsList($user),
            'idledis' => $user->id,
        ]);
    }

    public function getGreen(Request $request){
        $request->user()->setAdminChatUsersOnline();
    }
    public function postMassArrMass(Request $request)
    {

       // $request->user()->setAdminChatUsersOnline();
        $arr_choise_admin_users = $request->user()->getAddChoiseAdminUsersList();
        $arr_id = array();
        foreach ($arr_choise_admin_users as $item => $value) {
            $arr_id[] = $value->id;
        }
        $mass_arr_mass = [];
        foreach ($arr_id as $item_arr_id => $value_arr_id) {
            $ledi = ChatRooms::getChatRoomsListAdmin($value_arr_id);
            $count = 0;
            foreach ($ledi as $item => $value) {
                $count += $value->new_msgs_cnt;
            }
            $mass_arr_mass[$value_arr_id] = $count;
        }
        $array_sum = array_sum($mass_arr_mass);
/*        \Session::put('chat_previous_url', '/cabinet/payments/');
        $payment_id = \Session::get('paypal_payment_id');
        \Session::forget('paypal_payment_id');*/
        if( ((\Session::get('count_msg')) < $array_sum )){
            \Session::put('count_msg', $array_sum );
            $count_msg = 1 ;
        }elseif(((\Session::get('count_msg')) >= $array_sum )){
            \Session::put('count_msg', $array_sum );
            $count_msg = 0 ;
        }else{
            \Session::put('count_msg', 0 );
            $count_msg = 0 ;
        }
      //  \Session::put('count_msg', $array_sum  );
        $arr_id_2 = $request->user()->getChoiseAdminUsersList($arr_id);
        function sort_nested_arrays( $array, $args = array('votes' => 'desc') ){
            usort( $array, function( $a, $b ) use ( $args ){
                $res = 0;

                $a = (object) $a;
                $b = (object) $b;

                foreach( $args as $k => $v ){
                    if( $a->$k == $b->$k ) continue;

                    $res = ( $a->$k < $b->$k ) ? -1 : 1;
                    if( $v=='desc' ) $res= -$res;
                    break;
                }

                return $res;
            } );

            return $array;
        }
        foreach ($arr_id_2 as $key => $value){
         $chat_data =  Chat::getLastMsgs($value->id);
            foreach ($chat_data as $key_d=>$value_d){
                $value->updated_at = $value_d->created_at;
            }
            if(!isset($value->updated_at)){
                $value->updated_at = 0;
            }
        }
        /*echo '<pre>';
        print_r($arr_id_2);
        // print_r($arr_id_2);
        echo '</pre>';exit();*/
        $arr_id_2 = sort_nested_arrays( $arr_id_2, array('updated_at' => 'desc') );

        return view('cabinet.girls-panel')->with([
            'girl_wrap_choise' => $arr_id_2,
            'mass_arr_mass'    => $mass_arr_mass,
            'idledis'          =>(int)$request->get('idledis'),
            'count_msg'        => $count_msg,
        ]);
        // print_r( $request->user()->getChoiseAdminUsersList($arr_id));
    }

    public function getDeleteChatRoomAdmin(Request $request)
    {
        /**
         * @var $user User
         * @var $room ChatRooms
         */
        $room_id = (int)$request->segment(4);
        if ($room_id == 0)
            return redirect()->back();

        if ($room = ChatRooms::find($room_id)) {
            $user = (object)array('id' => (int)$request->get('idledis'), 'sex' => 'w');
           // $user = $request->user();

            $data = [
                'active' => 0,
            ];
            if ($user->sex == 'm')
                $data['man_delete'] = 1;
            else
                $data['lady_delete'] = 1;

            $room->fill($data);
            $room->save();

            Chat::setViewed($user->id, $room->man_id == $user->id ? $room->lady_id : $room->man_id);
        }
        return redirect()->to(\Lang::getLocale() . '/cabinet/chat-admin');
    }

    public function postChatOnlineFromAdmin(Request $request)
    {
        $partner_id = (int)$request->get('partner_id');
        $user = (object)array('id' => (int)$request->get('idledis'), 'sex' => 'w');
        return view('cabinet.inc.chat-online-from-admin')->with([
            'partner_id' => $partner_id,
            'onlineUsersList' => $request->user()->getOnlineUsersList($partner_id , 1000),
            'idledis' => $user->id,
        ]);
    }
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Start chat page
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getNews(Request $request , News $newsModel)
    {
        $user = $request->user();
        $tmp = \DB::table('view_news')
            ->where('id_user', '=', $user->id)
            ->get();
        if(count($tmp) == 0){
            \DB::table('view_news')->insert(
                ['id_user' => $user->id]
            );
            //\DB::table('view_news')->where('id_user', '=', $user->id)->delete();
        }
        /*print_r($tmp);
        print_r(count($tmp));
        exit();*/
        $storyid = $request->segment(4);
        if(isset($storyid) && !empty($storyid)){
            $news = $news = News::where('url_name', '=', $storyid)->get();;
            return view('cabinet.news', [
                'News' => $news,
                'actions' => true,
            ]);
        }else{
            // print_r($news_id);
            /// exit();
            $news = $newsModel->GetPablichedNews();
            return view('cabinet.news', [
                'News' => $news,
            ]);
        }
    }

    public function getStories(Request $request , News $newsModel)
    {
        $storyid = $request->segment(4);
        if(isset($storyid) && !empty($storyid)){
            $news = $newsModel->GetPablichedOneNews($storyid);
            return view('cabinet.news', [
                'News' => $news,
                'actions' => true,
                'stories' => true,
            ]);
        }
        // print_r($news_id);
        /// exit();
        $news = $newsModel->GetStories();
        return view('cabinet.news', [
            'News' => $news,
            'stories' => true,
        ]);
    }



    public function getStartChat(Request $request)
    {
        if (!ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . '/cabinet/chat')->with([
                'flashMsg' => trans('cabinet.chat_incorrect_partner'),
            ]);

        if (!ChatRooms::checkBillingForStartNewChat($request->user()))
            return redirect()->to(\Lang::getLocale() . '/cabinet/chat')->with([
                'flashMsg' => 'Not enough money in the account to start a new chat', // TODO: make translate
            ]);

        $partner_id = (int)$request->segment(4);

        $this->updateChatRoom($request->user(), $partner_id, 1);

        return redirect()->to(\Lang::getLocale() . '/cabinet/chat/' . $partner_id);
    }

    /**
     * Stop chat page
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getStopChat(Request $request)
    {
        if (!ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . '/cabinet/chat')->with([
                'flashMsg' => trans('cabinet.chat_incorrect_partner'),
            ]);

        $partner_id = (int)$request->segment(4);

        $this->updateChatRoom($request->user(), $partner_id, 0);

        return redirect()->to(\Lang::getLocale() . '/cabinet/chat/');
    }

    /**
     * Stop all chats
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getCloseChat(Request $request)
    {
        /**
         * @var $user User
         */
        $user = $request->user();

        ChatRooms::stopAllChatRooms($user);
        Chat::setViewedAll($user->id);

        return redirect()->to(\Lang::getLocale() . \Session::get('chat_previous_url', \Config::get('app.default_cabinet_page')));
    }

    /**
     * Delete chat rooms
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getDeleteChatRoom(Request $request)
    {
        /**
         * @var $user User
         * @var $room ChatRooms
         */
        $room_id = (int)$request->segment(4);
        if ($room_id == 0)
            return redirect()->back();

        if ($room = ChatRooms::find($room_id)) {
            $user = $request->user();

            $data = [
                'active' => 0,
            ];
            if ($user->sex == 'm')
                $data['man_delete'] = 1;
            else
                $data['lady_delete'] = 1;

            $room->fill($data);
            $room->save();

            Chat::setViewed($user->id, $room->man_id == $user->id ? $room->lady_id : $room->man_id);
        }
        return redirect()->to(\Lang::getLocale() . '/cabinet/chat');
    }

    /**
     * Delete history chat rooms
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getDeleteHistoryChatRoom(Request $request)
    {
        /**
         * @var $user User
         * @var $room ChatRooms
         */
        $room_id = (int)$request->segment(4);
        if ($room_id == 0)
            return redirect()->back();

        if ($room = ChatRooms::find($room_id)) {
            $user = $request->user();

            if ($room->man_id != $user->id && $room->lady_id != $user->id)
                return redirect()->back()->with([
                    'flashMsg' => trans('cabinet.letter_access_violiation'),
                ]);

            // erase letter
            if ($room->man_id == $user->id)
                $room->man_erase = 1;
            else
                $room->lady_erase = 1;

            if ($room->man_erase == 1 && $room->lady_erase == 1)
                Chat::clear($room->man_id, $room->lady_id);
            $room->save();
        }

        return redirect()->to(\Lang::getLocale() . '/cabinet/chat-history');
    }

    /**
     * Restore chat from history
     *
     * @param Request $request
     *
     * @return $this
     */
    public function getRestoreChat(Request $request)
    {
        $partner_id = (int)$request->segment(4);

        $this->updateChatRoom($request->user(), $partner_id, 0);

        return redirect()->to(\Lang::getLocale() . '/cabinet/chat/' . $partner_id);
    }

    /**
     * Recommended right block (for chat)
     *
     * @param Request $request
     *
     * @return $this
     */
    public function postRoomsList(Request $request)
    {
        return view('cabinet.inc.chat-rooms')->with([
            'chatRoomsList' => ChatRooms::getChatRoomsList($request->user()),
        ]);
    }

    /**
     * Recommended right block (for chat)
     *
     * @param Request $request
     *
     * @return $this
     */
    public function postHistoryRoomsList(Request $request)
    {
        return view('cabinet.inc.chat-rooms')->with([
            'chatHistoryRoomsList' => ChatRooms::getChatHistoryRoomsList($request->user()),
        ]);
    }

    /**
     * Recommended right block (for chat)
     *
     * @param Request $request
     *
     * @return $this
     */
    public function postChatRecommended(Request $request)
    {
        $partner_id = (int)$request->get('partner_id');
        return view('cabinet.inc.chat-recommended')->with([
            'partner_id' => $partner_id,
            'onlineUsersList' => $request->user()->getOnlineUsersList($partner_id),
        ]);
    }

    /**
     * Chat history page.
     *
     * @return \Response
     */
    public function getChatHistory(Request $request)
    {
        if (!ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . '/cabinet/chat-history')->with([
                'flashMsg' => trans('cabinet.chat_incorrect_partner'),
            ]);

        $partner_id = (int)$request->segment(4);

        $this->updateChatRoom($request->user(), $partner_id);

        $data = [
            'chat_previous_url' => \Session::get('chat_previous_url'),
            'chatHistoryRoomsList' => ChatRooms::getChatHistoryRoomsList($request->user()),
            'chatHistoryRoomsKeyList' => ChatRooms::getChatHistoryRoomsKeyList($request->user()),
            'onlineUsersList' => $request->user()->getOnlineUsersList($partner_id),
            'partner_id' => $partner_id,
            'partner_info' => $request->user()->getUserInfoById($partner_id),
        ];

        return view('cabinet.chat-history')->with($data);
    }

    /**
     * Favorite list page.
     *
     * @return \Response
     */
    public function getFavoriteList(Request $request)
    {
        \Session::put('chat_previous_url', '/cabinet/favorite-list/');

        $data = [
            'userSex' => $request->user()->sex,
            'favoritesList' => $request->user()->getFavoritesUsers(),
        ];
        return view('cabinet.favorite-list', $data);
    }

    /**
     * Favorite list page.
     *
     * @return \Response
     */
    public function getUsersOnlineList(Request $request)
    {
        \Session::put('chat_previous_url', '/cabinet/users-online-list/');

        $data = [
            'userSex' => $request->user()->sex,
            'onlineUsersList' => $request->user()->getOnlineUsersList(0, 50),
        ];
        return view('cabinet.users-online-list', $data);
    }
    /**
     * Favorite list page.
     *
     * @return \Response
     */
    public function getUsersAllList(Request $request)
    {
        \Session::put('chat_previous_url', '/cabinet/users-all-list/');

        $data = [
            'userSex' => $request->user()->sex,
            'onlineUsersList' => $request->user()->getAllLadyList($request->user()->id),
        ];
        return view('cabinet.users-all-list', $data);
    }
    /**
     * Read letters page.
     *
     * @return \Response
     */
    public function getReadLetter(Request $request, Mail $MailModel)
    {
        if ($request->segment(4) == NULL)
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => trans('cabinet.letter_not_selected'),
            ]);

        \Session::put('chat_previous_url', '/cabinet/read-letter/' . $request->segment(4));

        $msg = Mail::getMsg((int)$request->segment(4), $request->user()->id);

        if (empty($msg))
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => trans('cabinet.letter_not_found'),
            ]);

        if ($msg->user_id != $request->user()->id && $msg->partner_id != $request->user()->id)
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => trans('cabinet.letter_access_violiation'),
            ]);
        if ($msg->ArrayIdGifts) {
            $Gifts = explode(",", $msg->ArrayIdGifts);
            $Gifts = $MailModel->getImageGifts($Gifts);
        } else {
            $Gifts = FALSE;
        }


        if ($msg->Imagedounload) {
            $image = $msg->Imagedounload;
        } else {
            $image = FALSE;
        }

        $data = [
            'user_id' => $request->user()->id,
            'msg' => $msg,
            'Gifts' => $Gifts,
            'image' => $image,
        ];

        return view('cabinet.read-letter', $data);
    }

    /**
     * Inbox list page.
     *
     * @return \Response
     */
    public function getInboxList(Request $request)
    {
        \Session::put('chat_previous_url', '/cabinet/inbox-list/');

        Mail::setViewed($request->user()->id);
        return view('cabinet.inbox-list', [
            'inboxList' => Mail::getInboxMsgs($request->user()->id),
        ]);
    }

    /**
     * Sent mails list page.
     *
     * @return \Response
     */
    public function getSentList(Request $request)
    {
        \Session::put('chat_previous_url', '/cabinet/sent-list/');

        return view('cabinet.sent-list', [
            'sentList' => Mail::getSentMsgs($request->user()->id),
        ]);
    }

    /**
     * Inbox list page.
     *
     * @return \Response
     */
    public function getDeletedList(Request $request)
    {
        \Session::put('chat_previous_url', '/cabinet/deleted-list/');

        return view('cabinet.deleted-list', [
            'deleteList' => Mail::getDeleteMsgs($request->user()->id),
        ]);
    }

    /**
     * Payments history page.
     *
     * @return \Response
     */
    public function getPayments(Request $request)
    {
          $st = $request->get('st');
          $cm = $request->get('cm');
        if (($st == 'Completed') && (isset($cm))) {
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => "please",
            ]);
        }

        return view('cabinet.payments', [
            'balance' => round($request->user()->billing->balance, 2),
            'historyData' => BillingLog::getHistoryDataDesc($request->user()->billing->id),
            'id_user' => $request->user()->id,
        ]);
    }
    public function getPaymentsSession(Request $request)
    {
        $cache = md5((int)$request->get('custom').$request->user()->id."sole_ukrbride4u");
        \DB::insert('insert into billing_paypal_cache (id_users, cache) values (?, ?)', [$request->user()->id, $cache]);
        return json_encode(true);
    }
    /**
     * Set all users online.
     *
     * @return \Response
     */
    public function getAllUsersOnline(Request $request)
    {
        if (!in_array($request->user()->rights, ['superadmin',]))
            abort(404);

        $request->user()->setAllUsersOnline();

        return redirect()->back()->with([
            'flashMsg' => 'All users is online!',
        ]);
    }

    /**
     * Add to favorites.
     *
     * @return \Response
     */
    public function getAddFav(Request $request)
    {

        if (($request->segment(4) == NULL) || !ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => trans('cabinet.letter_incorrect_partner'),
            ]);

        /**
         * @var $user User
         */
        $user = $request->user();

        if ($user->checkFavoriteExist((int)$request->segment(4)))
            return redirect()->back()->with([
                'flashMsg' => 'This user is already in favorites list!', // TODO: make translate
            ]);

        $user->favorites()->save(new UserFavorites(['fav_id' => (int)$request->segment(4)]));

        return redirect()->back()->with([
            'flashMsg' => 'User added to favorite list!', // TODO: make translate
        ]);
    }


    /**
     * Buy contact.
     *
     * @return \Response
     */
    public function getAddCon(Request $request)
    {
        //	$user->id

    }

    public function getBayFav(Request $request)
    {

        $validator = Validator::make($request->only('userid', 'ladiesid'), [
            'ladiesid' => 'required|min:1|max:300',
        ]);

        if (($request->segment(4) == NULL)) {
            return redirect()->back()->with([
                'flashMsg' => 'mail not send', // TODO: make translate
            ]);
        }
        $user = $request->user();

        $mesege = "Пользователь " . $user->email . "   хочет запросить контакты " . $request->segment(4);
        mail('varchuk90@mail.ru', "Запрос пользовательских данных", $mesege);
        // mail('tuta.alexandr@mail.ru', "Запрос пользовательских данных" , $mesege );
        return redirect()->back()->with([
            'flashMsg' => 'mail is send', // TODO: make translate
        ]);
    }

    /**
     * Delete user from favorite.
     *
     * @return \Response
     */
    public function postDelFav(Request $request)
    {
        $data = [
            'favorite_id' => (int)$request->get('favorite_id'),
            'user_id' => $request->user()->id,
            'fav_id' => (int)$request->get('fav_id'),
        ];

        /**
         * @var $favoriteUser UserFavorites
         */
        if (!$favoriteUser = UserFavorites::find($data['favorite_id']))
            return redirect()->back()->with([
                'flashMsg' => 'error', // TODO: make translate
            ]);

        if ($favoriteUser->user_id != $data['user_id'] || $favoriteUser->fav_id != $data['fav_id'])
            return redirect()->back()->with([
                'flashMsg' => trans('cabinet.letter_access_violiation'), // TODO: make translate
            ]);

        $favoriteUser->delete();

        return redirect()->back();
    }

    public function getAutocompleteAddress(Request $request)
    {
        return \Response::json($request->user()->getACAddressList($request->get('term')));
    }

    public function getSendGift(Request $request, User $UserModel)
    {
        if ($request->user()->sex == 'w')
            abort(404);

        if (($request->segment(4) == NULL) || !ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => trans('cabinet.letter_incorrect_partner'),
            ]);

        \Session::put('chat_previous_url', '/cabinet/send-gift/' . $request->segment(4));

        $data = [
            'partner_id' => (int)$request->segment(4),
            'partner_info' => $request->user()->getUserInfoByIdForGift($request->segment(4)),
            'gifts' => $UserModel->getGiftInfoById(0),
        ];

        return view('cabinet.send-gift')->with($data);
    }

    public function getSendGiftReal(Request $request, User $UserModel)
    {
        if ($request->user()->sex == 'w')
            abort(404);

        if (($request->segment(4) == NULL) || !ChatService::checkChatPageRequestData($request))
            return redirect()->to(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
                'flashMsg' => trans('cabinet.letter_incorrect_partner'),
            ]);

        \Session::put('chat_previous_url', '/cabinet/send-gift/' . $request->segment(4));

        $data = [
            'partner_id' => (int)$request->segment(4),
            'partner_info' => $request->user()->getUserInfoByIdForGift($request->segment(4)),
            'gifts' => $UserModel->getGiftInfoById(1),
        ];

        return view('cabinet.send-gift-real')->with($data);
    }
}
