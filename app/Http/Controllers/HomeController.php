<?php namespace App\Http\Controllers;

use App\Models\Mail;
use App\Models\News;
use App\Models\User;
use App\Services\TwoCheckoutService;
use Illuminate\Http\Request;
use Validator;
use Artisan;
use App\Models\Billing;
use App\Models\BillingLog;
use App\Models\Chat;
use App\Models\ChatRooms;
use App\Models\ChatRoomsLog;

class HomeController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */

    public function getIndex(News $NewsModel, User $UserModel)
    {
        return view('home.index', [
            'News' => $NewsModel->GetPablichedNews(),
            'Story' => $NewsModel->GetPablichedOneStory(),
            'onlineUsersList' => $UserModel->getLadiesList(6),
        ]);
    }

    public function getNews($ruin, $newsid, User $user)
    {
        $news = News::where('url_name', '=', $newsid)->get();

        return view('home.about-us', [
            'News' => $news,
            'ladiesList' => $user->getLadiesList(4),
        ]);
    }

    /**
     * Show women registration page.
     *
     * @return Response
     */
    public function getWReg(Request $request)
    {
        return view('home.w-reg');
    }

    public function postSendMailWReg(Request $request)
    {
        $validator = Validator::make($request->only('name', 'phone'), [
            'name' => 'required|min:2|max:50',
            'phone' => 'required|min:2|max:15',
        ]);

        if ($validator->fails()) {
            return json_encode(false);
        }
        $mesege = "";
        if ($request->get('mesege')) {
            $mesege = $request->get('mesege');
        } else {
            $mesege = 'Девушка  ' . $request->get('name') . '  . Номер  ' . $request->get('phone') . '.';
        }
        mail('dl@upperlinestudio.com', 'dl@upperlinestudio.com', $mesege);


        /*        Mail::raw('Девушка  ' . $request->get('name') . '. Номер  ' . $request->get('phone') . '.', function ($message) {
                    $message->from('tuta.alexandr@mail.ru', 'Laravel');

                    $message->to('tuta.alexandr@mail.ru')->cc('tuta.alexandr@mail.ru');
                });*/

        return json_encode(true);

    }

    public function getSupport(Request $request)
    {
        return view('home.support');
    }

    public function getSupport2()
    {
        
        ChatRooms::stopOldChatRooms();
        Billing::calcPaidForChatRoomsLog();
        ChatRooms::stopChatRoomsWhereUserNoMoney();
        $a = new User();
        $a->setAdminChatUsersOnline();
       // User->setAdminChatUsersOnline();
    }
    public function postPaypalvalid(Request $request)
    {
       // return json_encode($request->all());
// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
        define("DEBUG", 1);

// Set to 0 once you're ready to go live
        define("USE_SANDBOX", 0);


        define("LOG_FILE", public_path()."/upload/ipn.log");


// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
       /* $raw_post_data = $request->all();
        $raw_post_array = explode('&', $raw_post_data);*/
        $raw_post_array = $request->all();
        $raw_post_data = $request->all() ;
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
                $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
// read the post from PayPal system and add 'cmd'
        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data

        if(USE_SANDBOX == true) {
            $paypal_url = "https://ipnpb.sandbox.paypal.com/cgi-bin/webscr";
        } else {
            $paypal_url = "https://ipnpb.paypal.com/cgi-bin/webscr";
        }

        $ch = curl_init($paypal_url);
        if ($ch == FALSE) {
            return FALSE;
        }

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

        if(DEBUG == true) {
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
        }

// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);

// Set TCP timeout to 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.

//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);

        $res = curl_exec($ch);
        if (curl_errno($ch) != 0) // cURL error
        {
            if(DEBUG == true) {
                error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
            exit;

        } else {
            // Log the entire HTTP response if debug is switched on.
            if(DEBUG == true) {
                error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
                error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
            }
            curl_close($ch);
        }

// Inspect IPN validation result and act accordingly

// Split response headers and payload, a better way for strcmp
        $tokens = explode("\r\n\r\n", trim($res));
        $res = trim(end($tokens));

        if (strcmp ($res, "VERIFIED") == 0) {
            // check whether the payment_status is Completed
            // check that txn_id has not been previously processed
            // check that receiver_email is your PayPal email
            // check that payment_amount/payment_currency are correct
            // process payment and mark item as paid.

            // assign posted variables to local variables
            //$item_name = $_POST['item_name'];
            //$item_number = $_POST['item_number'];
            //$payment_status = $_POST['payment_status'];
            //$payment_amount = $_POST['mc_gross'];
            //$payment_currency = $_POST['mc_currency'];
            //$txn_id = $_POST['txn_id'];
            //$receiver_email = $_POST['receiver_email'];
            //$payer_email = $_POST['payer_email'];

            if(DEBUG == true) {
                error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
            }
        } else if (strcmp ($res, "INVALID") == 0) {
            // log for manual investigation
            // Add business logic here which deals with invalid IPN messages
            if(DEBUG == true) {
                error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
            }
        }


    }
    public function postSendMailSupport(Request $request)
    {
        $validator = Validator::make($request->only('name', 'mesege'), [
            'name' => 'required|min:2|max:50',
            'mesege' => 'required|min:2|max:300',
        ]);

        if ($validator->fails()) {
            return json_encode(false);
        }
        $mesege = "";
        if ($request->get('mesege')) {
            $mesege = $request->get('mesege');
        }
        mail('dl@upperlinestudio.com', $request->get('name'), $mesege);

        return json_encode(true);

    }

    /**
     * Show women registration page.
     *
     * @return Response
     */

    public function getLoveStorys(News $newsModel, User $user)
    {
        return view('home.love-story', [
            'ggetstory' => $newsModel->GetStories(),
            'ladiesList' => $user->getLadiesList(4),
        ]);
    }

    public function getstory(News $NewsModel, $ruin, $storyid, User $user)
    {
        $news = $NewsModel->GetPablichedOneNews($storyid);

        return view('home.about-us', [
            'News' => $news,
            'ladiesList' => $user->getLadiesList(4),
        ]);
    }

    public function getAboutUs(User $user)
    {
        return view('home.about-us', [
            'ladiesList' => $user->getLadiesList(4),
        ]);
    }

    public function getPrivacyPolicy(User $user)
    {
        return view('home.privacypolicy', [
            'ladiesList' => $user->getLadiesList(4),
        ]);
    }

    public function getTermsAndConditions(User $user)
    {
        return view('home.terms-and-conditions', [
            'ladiesList' => $user->getLadiesList(4),
        ]);
    }

    public function twoCOApproved(Request $request, TwoCheckoutService $twoCheckoutService)
    {
        return $twoCheckoutService->parseDirectReturnCallback($request);
    }

}
