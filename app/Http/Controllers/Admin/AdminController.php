<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BillingLog;
use App\Models\News;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Image;
use Input;
use Validator;

class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getIndex(Request $request)
    {
        return \Redirect::to(\Lang::getLocale() . \Config::get('app.default_admin_page'));
    }

    public function getArticles(Request $request, News $NewsModel)
    {
        $News = $NewsModel->AdminPablichedNews();
        $Story = $NewsModel->AdminPablichedStory();

        return view('admin.articles', ['News' => $News, 'Story' => $Story]);
    }

    public function postStatistics(Request $request)
    {
        return redirect()->back()->withInput($request->input());
    }

    public function getStatistics(Request $request)
    {
        return view('admin.statistics', [
            'out'        => $request->old('from') . ' - ' . $request->old('to'),
            'statistics' => BillingLog::getAdminStatistics($request->old('from'), $request->old('to')),
        ]);
    }
    public function getShowDetailed(Request $request , $en , $id , $name)
    {
       // print_r(BillingLog::getHistoryData($id) );
         return view('admin.statistics-detailed', [
            'name' => $name,
            'historyData' => BillingLog::getHistoryDataDesc($id),
            'id_user' => $id,
        ]);
    }
    public function getUsers(Request $request)
    {
        return view('admin.users', [
            'm_list' => $request->user()->getUsersListBySex('m'),
            'w_list' => $request->user()->getUsersListBySex('w'),
        ]);
    }

    public function postRegisterUser(Request $request)
    {
        $validator = Validator::make($request->only('sex', 'name', 'email', 'password', 'password_confirmation'), [
            'sex'      => 'required|min:1|max:1',
            'name'     => 'required|min:2|max:50',
            'email'    => 'required|email|min:6|max:50|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors($validator, 'register');
        }

        $user = User::create([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'sex'      => $request->get('sex'),
            'rights'   => 'user',
            'locale'   => \Lang::getLocale(),
        ]);
        $user->billing()->create(['balance' => 0]);

        if ($request->input('need_login')) {
            \Auth::logout();
            \Auth::login($user);

            return redirect('/' . \Lang::getLocale() . '/cabinet');
        }

        return redirect()->back();
    }

    public function postEditUser(Request $request)
    {
        $requestData = $request->only('uid', 'name', 'email', 'password', 'password_confirmation');

        /**
         * @var $user Model
         * @var $user User
         */
        $user = User::find((int)$requestData['uid']);

        $rules = [];
        if ($user->name != $requestData['name']) {
            $rules['name'] = 'required|min:2|max:50';
        }
        if ($user->email != $requestData['email']) {
            $rules['email'] = 'required|email|min:6|max:50|unique:users';
        }
        if (!empty($requestData['password']) || !empty($requestData['password_confirmation'])) {
            $rules['password'] = 'required|confirmed|min:6';
        }

        $validator = Validator::make($requestData, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors($validator, 'edit');
        }

        $data = [];
        if ($user->name != $requestData['name']) {
            $data = ['name' => $requestData['name']];
        }
        if ($user->email != $requestData['email']) {
            $data = ['email' => $requestData['email']];
        }
        if (!empty($requestData['password'])) {
            $data['password'] = bcrypt($requestData['password']);
        }

        $user->fill($data);
        $user->save();

        return redirect()->back();
    }

    public function postDeleteUser(Request $request)
    {
        $userId = (int)$request->get('id');
        if ($userId < 1) {
            return json_encode(false);
        }
        if ($user = User::find($userId)) {
            if ($user->delete()) {
                rrmdir(public_path() . "/img/pr/$userId/");

                return json_encode(true);
            }
        } else {
            return json_encode(false);
        }
    }
    public function postAddMoneyUser(Request $request)
    {
        $userId = (int)$request->get('uid');
        $amount = (int)$request->get('amount');
        if ($userId < 1) {
            return json_encode(false);
        }
        if ($user = User::find($userId)) {
            //$amount = 1;
            $user_balance = $user->billing['balance'];

            $user->billing()->update(['balance' => $user_balance + $amount]);
            BillingLog::create([
                'billing_id' => $user->billing['id'],
             //  'partner_id' => null,
                'event' => 'recharge',
                'before' => $user_balance,
                'difference' => $amount,
                'after' => $user_balance + $amount,
            ]);
            return redirect('/' . \Lang::getLocale() . '/admin/users');
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }
    public function postWithdrawMoneyUser(Request $request)
    {
        $userId = (int)$request->get('uid');
        $amount = (int)$request->get('amount');
        if ($userId < 1) {
            return json_encode(false);
        }
        if ($user = User::find($userId)) {
            //$amount = 1;
            $user_balance = $user->billing['balance'];
            $result  =  $user_balance - $amount ;
            if( $result < 0){
                $result = 0.000 ;
            }

            $user->billing()->update(['balance' => $result]);

            BillingLog::create([
                'billing_id' => $user->billing['id'],
                //  'partner_id' => null,
                'event' => 'admin',
                'before' => $user_balance,
                'difference' => $amount,
                'after' => $result,
            ]);
            return redirect('/' . \Lang::getLocale() . '/admin/users');
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }
    public function postMoneyUser(Request $request)
    {
        $userId = (int)$request->get('id');
       // print_r(User::find($userId));
        /*return json_encode($userId );
        exit();*/
        if ($user = User::find($userId)) {
            //$amount = 1;
            $user_balance = $user->billing['balance'];

            return ($user_balance);
        } else {
            return (false);
        }
    }
    public function postLoginUser(Request $request)
    {
        if (User::find((int)$request->get('id'))) {
            \Auth::logout();
            \Auth::loginUsingId((int)$request->get('id'));

            return json_encode(true);
        }

        return json_encode(false);
    }

    private function saveUserImage($file, $imgName)
    {
        $imageDirUrl = "/upload/news/";
        $imageDirPath = public_path() . $imageDirUrl;
        $imagePath = $imageDirPath . '/' . $imgName;

        if (empty($file)) {
            if (file_exists($imagePath)) {
                return $imageDirUrl . $imgName;
            } else {
                return null;
            }
        }

        @mkdir($imageDirPath, 0755, true);

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
        Image::make($file)->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save($imagePath);

        return $imgName;
    }

    private static function generateMicroTimeString()
    {
        $mtime = explode(" ", microtime());
        $msec = explode(".", $mtime[0]);

        return $mtime[1] . '.' . $msec[1];
    }

    public function postRegisterNews(Request $request, News $NewsModel)
    {

        $validator = Validator::make($request->only('image', 'url_name', 'title', 'text', 'newsOrStories', 'password_confirmation'), [
            'title'         => 'required|min:2|max:200',
            'url_name'      => 'required|min:2|max:200',
            'newsOrStories' => 'required|min:1',
            'image'         => 'image',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->input())->withErrors($validator, 'addArticlesErrors');
        }

        $url = $request->get('url_name');
        if (News::where('newsOrStories', $request->get('newsOrStories'))->where('url_name', $url)->exists()) {
            $url .= '-' . time();
        }

        News::create([
            'image'         => $this->saveUserImage(Input::file('image'), self::generateMicroTimeString() . '.png'),
            'title'         => $request->get('title'),
            'url_name'      => $url,
            'text'          => $request->get('text'),
            'newsOrStories' => $request->get('newsOrStories'),
        ]);
        $newsOrStories = $request->get('newsOrStories');
        if($newsOrStories == 0){
            \DB::table('view_news')->delete();
        }
        return redirect('/' . \Lang::getLocale() . '/admin/articles');
    }


    public function postDeleteNews(Request $request, News $NewsModel)
    {
        $NewsId = (int)$request->get('id');
        if ($NewsId < 1) {
            return json_encode(false);
        }
        if ($News = News::find($NewsId)) {
            if ($News->delete()) {
                return json_encode(true);
            }
        } else {
            return json_encode(false);
        }

    }

    public function postEditNews(Request $request, News $NewsModel)
    {
        $requestData = $request->only('uid', 'title', 'url_name', 'text');

        $News = News::find((int)$requestData['uid']);

        $News->fill($requestData);
        $News->save();

        return redirect()->back();
    }

}
