<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserPhotos;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Image;
use Input;
use Socialize;
use Validator;

//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	public function __construct(Guard $auth) {
		$this->auth = $auth;
	}

	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function postRegisterMan(Request $request) {
		$validator = Validator::make($request->only('name', 'email', 'password', 'password_confirmation'), [
			'name'     => 'required|min:2|max:50',
			'email'    => 'required|email|min:6|max:50|unique:users',
			'password' => 'required|confirmed|min:6',
		]);

		if ($validator->fails())
			return redirect('/' . \Lang::getLocale())->withInput($request->input())->withErrors($validator, 'register');

		$user = User::create([
			'name'     => $request->get('name'),
			'email'    => $request->get('email'),
			'password' => bcrypt($request->get('password')),
			'sex'      => 'm',
			'rights'   => 'user',
			'locale'   => \Lang::getLocale(),
		]);
		$user->billing()->create(['balance' => 0]);

		$this->auth->login($user);

		return redirect('/' . \Lang::getLocale() . '/cabinet');
	}

	/**
	 * @return string Micro time string
	 */
	private static function generateMicroTimeString() {
		$mtime = explode(" ", microtime());
		$msec = explode(".", $mtime[0]);
		return $mtime[1] . '.' . $msec[1];
	}

	private function saveUserImage($file, $imgName) {
		$imageDirUrl = "/img/pr/" . \Auth::getUser()->id . "/";
		$imageDirPath = public_path() . $imageDirUrl;
		$imagePath = $imageDirPath . '/' . $imgName;

		if (empty($file))
			if (file_exists($imagePath))
				return $imageDirUrl . $imgName;
			else
				return NULL;

		@mkdir($imageDirPath, 0755, TRUE);

		if (file_exists($imagePath))
			unlink($imagePath);
		Image::make($file)->resize(1024, 1024, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		})->save($imagePath);

		return $imageDirUrl . $imgName;
	}

	/**
	 * @param $user_id       int
	 * @param $photosList    array
	 */
	private function deletePhotos($user_id, $photosList) {
		foreach ($photosList AS $value)
			if ($up = UserPhotos::find((int) $value))
				if ($up->user_id == $user_id) {
					if (!empty($up->img))
						if (file_exists(public_path() . $up->img))
							unlink(public_path() . $up->img);
					$up->delete();
				}
	}

	/**
	 * Edit user request handle.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function postEditUser(Request $request) {
		$data = $request->only([
			'name',
			'phone',
			'bdate',
			'address',
			'main_photo',
			'main_photo_del',
			'photo',
			'photos_to_del',
			'weight',
			'height',
			'zodiac',
			'eyes',
			'hair',
			'en_level',
			'about_me',
			'youtube',
		]);

		parseNullableValues($data, [
			'main_photo',
			'weight',
			'height',
			'zodiac',
			'eyes',
			'hair',
			'en_level',
			'about_me',
			'youtube',
		]);

		/**
		 * @var $user Model
		 * @var $user User
		 */
		$user = $this->auth->user();

		$validator = Validator::make($data, [
			'name'           => 'required|min:2|max:50',
			'phone'          => 'required|min:6|max:20',
			'bdate'          => 'required|date',
			'address'        => 'required|min:5|max:255',
			'main_photo'     => 'image',
			'main_photo_del' => 'integer|min:0|max:1',
			'weight'         => 'integer|min:40|max:255',
			'height'         => 'integer|min:100|max:255',
			'zodiac'         => 'min:2|max:50|in:' . implode(",", $user->getZodiacList()),
			'eyes'           => 'min:2|max:50|in:' . implode(",", $user->getEyesList()),
			'hair'           => 'min:2|max:50|in:' . implode(",", $user->getHairList()),
			'en_level'       => 'min:2|max:50|in:' . implode(",", $user->getEnLevelList()),
			'about_me'       => 'min:10|max:1000',
			'youtube'        => 'min:10|max:255',
		]);


		if ($validator->fails())
			$this->throwValidationException($request, $validator);


		// delete photos if need
		if ($data['main_photo_del'] == 1) {
			$data['main_photo'] = NULL;
			if (!empty($user->main_photo))
				if (file_exists(public_path() . $user->main_photo))
					unlink(public_path() . $user->main_photo);
		}
		if (!empty($data['photos_to_del']))
			$this->deletePhotos($user->id, explode(',', array_pull($data, 'photos_to_del')));


		// photos upload or ignore it
		$data['main_photo'] = $this->saveUserImage(Input::file('main_photo'), self::generateMicroTimeString() . '.png');


		// Additional photos handling
		if (isset($data['photo'])) {
			$additionalPhotos = [];
			foreach (array_pull($data, 'photo') AS $value) { // get photo and unset it from $data array
				if (!empty($value))
					if (!Validator::make(['photo' => $value], ['photo' => 'image'])->fails()) // validate current photo
						$additionalPhotos[] = new UserPhotos(['img' => $this->saveUserImage($value, self::generateMicroTimeString() . '.png')]); // save photo and use it name to create UserPhotos object
			}
			$user->photos()->saveMany($additionalPhotos); // save all additional photos
		}


		$user->fill($data);
		$user->save();

		return redirect('/' . \Lang::getLocale() . '/cabinet');
	}

	/**
	 * Edit account request handle.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function postEditAcc(Request $request) {
		$requestData = $request->only('email', 'password', 'password_confirmation');

		/**
		 * @var $user Model
		 * @var $user User
		 */
		$user = $this->auth->user();

		$rules = [];
		if ($user->email != $requestData['email'])
			$rules['email'] = 'required|email|min:6|max:50|unique:users';
		if (!empty($requestData['password']) || !empty($requestData['password_confirmation']))
			$rules['password'] = 'required|confirmed|min:6';

		$validator = Validator::make($requestData, $rules);

		if ($validator->fails())
			$this->throwValidationException($request, $validator);

		$data = [];
		if ($user->email != $requestData['email'])
			$data = ['email' => $requestData['email']];
		if (!empty($requestData['password']))
			$data['password'] = bcrypt($requestData['password']);

		$user->fill($data);
		$user->save();

		return redirect('/' . \Lang::getLocale() . '/cabinet');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request) {
		$validator = Validator::make($request->only('login', 'loginPassword'), [
			'login'         => 'required|email',
			'loginPassword' => 'required',
		]);

		if ($validator->fails())
			return redirect('/' . \Lang::getLocale())->withInput($request->input())->withErrors($validator, 'login');

		if ($this->auth->attempt([
			'email'    => $request->get('login'),
			'password' => $request->get('loginPassword'),
			'active'   => 1,
		], $request->has('remember'))
		) {
			if (in_array($request->user()->rights, ['superadmin', 'admin',]))
				return redirect(\Lang::getLocale() . \Config::get('app.default_admin_page'))->with([
					'flashMsg' => 'Welcome, boss ! ;)',
				]);
			else
				return redirect(\Lang::getLocale() . \Config::get('app.default_cabinet_page'))->with([
//					'flashMsg' => 'Welcome !',
				]);
		}

		return redirect('/' . \Lang::getLocale())->withInput($request->input())->withErrors([
			'email' => 'These credentials do not match our records.',
		], 'login');
	}

	public function getLogout() {
		$this->auth->logout();
		return redirect('/');
	}


	public function redirectToProvider() {
		return Socialize::with('facebook')->redirect();
	}

	public function registrerFacebook(User $UserModel) {
		$user = Socialize::with('facebook')->user();

		if ($UserIsset = $UserModel->searchUser($user->getEmail())) {
			$this->auth->loginUsingId($UserIsset->id);
			return redirect('/' . \Lang::getLocale() . '/cabinet');
		}
		else {
			$users = User::create([
				'name'     => $user->getName(),
				'email'    => $user->getEmail(),
				'password' => bcrypt($user->getId()),
				'sex'      => 'm',
				'rights'   => 'user',
				'locale'   => \Lang::getLocale(),
			]);
			$users->billing()->create(['balance' => 0]);
			$this->auth->login($users);
			return redirect('/' . \Lang::getLocale() . '/cabinet');
		}
	}
}
