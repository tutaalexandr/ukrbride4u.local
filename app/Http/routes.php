<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(array(
	'prefix' => '{language_prefix?}',
	), function () {
	Route::get('/', 'HomeController@getIndex');
	Route::get('w-reg', 'HomeController@getWReg');
	Route::get('artisan', 'HomeController@getartisan');
	Route::post('w-reg', 'HomeController@postSendMailWReg');
	Route::get('about-us', 'HomeController@getAboutUs');
	Route::get('support', 'HomeController@getSupport');
	Route::get('support2', 'HomeController@getSupport2');
	Route::post('support', 'HomeController@postSendMailSupport');
	Route::get('privacypolicy', 'HomeController@getPrivacyPolicy');
	Route::get('terms-and-conditions', 'HomeController@getTermsAndConditions');
	Route::get('news', 'HomeController@getIndex');
	Route::get('news/{news}', 'HomeController@getNews');

	Route::get('redirect', 'Auth\AuthController@redirectToProvider');
	Route::get('callback', 'Auth\AuthController@registrerFacebook');

	Route::get('love-story', 'HomeController@getLoveStorys');
	Route::get('love-story/{lovestory}', 'HomeController@getstory');

	Route::get('2co/approved', 'HomeController@twoCOApproved');

	Route::controllers([
		'admin'   => 'Admin\AdminController',
		'cabinet' => 'Cabinet\CabinetController',
		'mail'    => 'Cabinet\MailController',
		'chat'    => 'Cabinet\ChatController',
		'auth'    => 'Auth\AuthController',
		]);
});
