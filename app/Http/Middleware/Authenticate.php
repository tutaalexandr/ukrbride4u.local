<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard $auth
	 */
	public function __construct(Guard $auth) {
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (getCurrentControllerName() == 'AdminController') {
			if ($this->auth->guest())
				abort(404);
			if (!in_array($request->user()->rights, ['superadmin', 'admin',]))
				abort(404);
		}
		else if ($this->auth->guest()) {
			if ($request->ajax()) {
				\Session::flash('flashMsg', trans('main.please_login'));
				return response('Unauthorized.', 401);
			}
			else
				return redirect()->guest('/' . \Lang::getLocale());
		}

		if (!$request->ajax())
			$request->user()->updateLastVisit();

		return $next($request);
	}
}
