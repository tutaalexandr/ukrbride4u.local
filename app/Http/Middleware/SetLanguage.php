<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class SetLanguage implements Middleware {

	/**
	 * @param Application $app
	 * @param Redirector  $redirector
	 * @param Request     $request
	 */
	public function __construct(Application $app, Redirector $redirector, Request $request) {
		$this->app = $app;
		$this->redirector = $redirector;
		$this->request = $request;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 *
	 * @return mixed
	 */

	public function handle($request, Closure $next) {
		$locale = $request->segment(1);
		if (array_key_exists($locale, $this->app->config->get('app.supported_locale'))) { // если полученная локаль поддерживается/существует
			session()->put('lang', $locale);
			$this->app->setLocale($locale);
			return $next($request);
		}

		$locale = '';
		if (session()->has('lang')) { // если локаль сохранена в сессии
			$tmpLocale = session('lang');
			if (array_key_exists($tmpLocale, $this->app->config->get('app.supported_locale'))) // если сохраненная в сессии локаль поддерживается
				$locale = $tmpLocale;
		}
		if (empty($locale)) { // если с сессией ничего не вышло
			if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) { // если определась локаль браузера клиента
				$tmpLocale = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); // первый приоритетный язык
				if (array_key_exists($tmpLocale, $this->app->config->get('app.supported_locale'))) // если локаль браузера клиента поддерживается
					$locale = $tmpLocale;
			}
		}
		if (empty($locale)) // если никто ничего не знает о локали - взять локаль по умолчанию
			$locale = $this->app->config->get('app.fallback_locale');

		session()->put('lang', $locale);

		return $this->redirector->to($locale . '/' . $request->path(), 301);
	}
}