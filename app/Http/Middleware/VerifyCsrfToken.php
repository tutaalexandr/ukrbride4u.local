<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     *
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function handle($request, Closure $next)
    {
        $pathExceptionList = [
        ];

        if ($this->isReading($request) || $this->tokensMatch($request) || ends_with($request->getRequestUri(), $pathExceptionList)) {
            return $this->addCookieToResponse($request, $next($request));
        }

        return redirect()->back()->withInput($request->input())->with(['flashMsg' => trans('main.security_token_expired')]);
//		return parent::handle($request, $next);
    }

}
