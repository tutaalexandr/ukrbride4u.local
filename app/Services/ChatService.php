<?php namespace App\Services;

use App\Models\Chat;
use App\Models\ChatRooms;
use App\Models\User;
use Auth;
use Illuminate\Database\Eloquent\Model;

class ChatService {

	private static $smiles = [
		'::s1::'   => 'smile1.png',
		'::s10::'  => 'smile10.png',
		'::s100::' => 'smile100.png',
		'::s101::' => 'smile101.png',
		'::s102::' => 'smile102.png',
		'::s103::' => 'smile103.png',
		'::s104::' => 'smile104.png',
		'::s105::' => 'smile105.png',
		'::s106::' => 'smile106.png',
		'::s107::' => 'smile107.png',
		'::s108::' => 'smile108.png',
		'::s109::' => 'smile109.png',
		'::s11::'  => 'smile11.png',
		'::s110::' => 'smile110.png',
		'::s111::' => 'smile111.png',
		'::s112::' => 'smile112.png',
		'::s113::' => 'smile113.png',
		'::s114::' => 'smile114.png',
		'::s115::' => 'smile115.png',
		'::s116::' => 'smile116.png',
		'::s117::' => 'smile117.png',
		'::s118::' => 'smile118.png',
		'::s119::' => 'smile119.png',
		'::s12::'  => 'smile12.png',
		'::s120::' => 'smile120.png',
		'::s121::' => 'smile121.png',
		'::s122::' => 'smile122.png',
		'::s123::' => 'smile123.png',
		'::s124::' => 'smile124.png',
		'::s125::' => 'smile125.png',
		'::s126::' => 'smile126.png',
		'::s127::' => 'smile127.png',
		'::s128::' => 'smile128.png',
		'::s129::' => 'smile129.png',
		'::s13::'  => 'smile13.png',
		'::s130::' => 'smile130.png',
		'::s131::' => 'smile131.png',
		'::s132::' => 'smile132.png',
		'::s133::' => 'smile133.png',
		'::s134::' => 'smile134.png',
		'::s135::' => 'smile135.png',
		'::s136::' => 'smile136.png',
		'::s137::' => 'smile137.png',
		'::s138::' => 'smile138.png',
		'::s139::' => 'smile139.png',
		'::s14::'  => 'smile14.png',
		'::s140::' => 'smile140.png',
		'::s141::' => 'smile141.png',
		'::s142::' => 'smile142.png',
		'::s143::' => 'smile143.png',
		'::s144::' => 'smile144.png',
		'::s145::' => 'smile145.png',
		'::s146::' => 'smile146.png',
		'::s147::' => 'smile147.png',
		'::s148::' => 'smile148.png',
		'::s149::' => 'smile149.png',
		'::s15::'  => 'smile15.png',
		'::s150::' => 'smile150.png',
		'::s151::' => 'smile151.png',
		'::s16::'  => 'smile16.png',
		'::s17::'  => 'smile17.png',
		'::s18::'  => 'smile18.png',
		'::s19::'  => 'smile19.png',
		'::s2::'   => 'smile2.png',
		'::s20::'  => 'smile20.png',
		'::s21::'  => 'smile21.png',
		'::s22::'  => 'smile22.png',
		'::s23::'  => 'smile23.png',
		'::s24::'  => 'smile24.png',
		'::s25::'  => 'smile25.png',
		'::s26::'  => 'smile26.png',
		'::s27::'  => 'smile27.png',
		'::s28::'  => 'smile28.png',
		'::s29::'  => 'smile29.png',
		'::s3::'   => 'smile3.png',
		'::s30::'  => 'smile30.png',
		'::s31::'  => 'smile31.png',
		'::s32::'  => 'smile32.png',
		'::s33::'  => 'smile33.png',
		'::s34::'  => 'smile34.png',
		'::s35::'  => 'smile35.png',
		'::s36::'  => 'smile36.png',
		'::s37::'  => 'smile37.png',
		'::s38::'  => 'smile38.png',
		'::s39::'  => 'smile39.png',
		'::s4::'   => 'smile4.png',
		'::s40::'  => 'smile40.png',
		'::s41::'  => 'smile41.png',
		'::s42::'  => 'smile42.png',
		'::s43::'  => 'smile43.png',
		'::s44::'  => 'smile44.png',
		'::s45::'  => 'smile45.png',
		'::s46::'  => 'smile46.png',
		'::s47::'  => 'smile47.png',
		'::s48::'  => 'smile48.png',
		'::s49::'  => 'smile49.png',
		'::s5::'   => 'smile5.png',
		'::s50::'  => 'smile50.png',
		'::s51::'  => 'smile51.png',
		'::s52::'  => 'smile52.png',
		'::s53::'  => 'smile53.png',
		'::s54::'  => 'smile54.png',
		'::s55::'  => 'smile55.png',
		'::s56::'  => 'smile56.png',
		'::s57::'  => 'smile57.png',
		'::s58::'  => 'smile58.png',
		'::s59::'  => 'smile59.png',
		'::s6::'   => 'smile6.png',
		'::s60::'  => 'smile60.png',
		'::s61::'  => 'smile61.png',
		'::s62::'  => 'smile62.png',
		'::s63::'  => 'smile63.png',
		'::s64::'  => 'smile64.png',
		'::s65::'  => 'smile65.png',
		'::s66::'  => 'smile66.png',
		'::s67::'  => 'smile67.png',
		'::s68::'  => 'smile68.png',
		'::s69::'  => 'smile69.png',
		'::s7::'   => 'smile7.png',
		'::s70::'  => 'smile70.png',
		'::s71::'  => 'smile71.png',
		'::s72::'  => 'smile72.png',
		'::s73::'  => 'smile73.png',
		'::s74::'  => 'smile74.png',
		'::s75::'  => 'smile75.png',
		'::s76::'  => 'smile76.png',
		'::s77::'  => 'smile77.png',
		'::s78::'  => 'smile78.png',
		'::s79::'  => 'smile79.png',
		'::s8::'   => 'smile8.png',
		'::s80::'  => 'smile80.png',
		'::s81::'  => 'smile81.png',
		'::s82::'  => 'smile82.png',
		'::s83::'  => 'smile83.png',
		'::s84::'  => 'smile84.png',
		'::s85::'  => 'smile85.png',
		'::s86::'  => 'smile86.png',
		'::s87::'  => 'smile87.png',
		'::s88::'  => 'smile88.png',
		'::s89::'  => 'smile89.png',
		'::s9::'   => 'smile9.png',
		'::s90::'  => 'smile90.png',
		'::s91::'  => 'smile91.png',
		'::s92::'  => 'smile92.png',
		'::s93::'  => 'smile93.png',
		'::s94::'  => 'smile94.png',
		'::s95::'  => 'smile95.png',
		'::s96::'  => 'smile96.png',
		'::s97::'  => 'smile97.png',
		'::s98::'  => 'smile98.png',
		'::s99::'  => 'smile99.png',
	];

	public static function getSmiles() {
		return self::$smiles;
	}

	private static function convertSmilesFromCodes($string) {
		list($keys, $values) = array_divide(self::$smiles);

		foreach ($values AS $key => $value)
			$values[$key] = '<img src="' . asset('img/sm/ls/' . $value) . '" />';

		return str_replace($keys, $values, $string);
	}

	public static function prepareInputMsg($inputMsg, $nl2br = TRUE, $smiles = TRUE) {
		$res = $inputMsg;
		$res = trim($res);
		$res = htmlspecialchars($res);
		if ($nl2br)
			$res = nl2br($res);
		if ($smiles)
			$res = self::convertSmilesFromCodes($res);
		return $res;
	}

	public static function prepareMessages($data , $id = 0) {
		$res = '';
		foreach (array_reverse($data) AS $key => $value) {
			$time = date("H : i", strtotime($value->created_at));
			if (((int) $value->user_id == (int) Auth::getUser()->id) || ((int) $value->user_id == (int) $id ) )
				$res .= "
					<div class=\"you\" id=\"chat-msg-{$value->id}\" mid=\"{$value->id}\">
						<div class=\"time\">{$time}</div>
						<div class=\"letter\">{$value->msg}</div>
					</div>
				";
			else
				$res .= "
					<div class=\"partner\" id=\"chat-msg-{$value->id}\" mid=\"{$value->id}\">
						<div class=\"chat_avatar\"></div>
						<div class=\"letter\">{$value->msg}</div>
						<div class=\"time\">{$time}</div>
					</div>
				";
		}
		return $res;
	}

	public static function printAllMsgs($partner_id) {
		$resData = [
			'msgs' => self::prepareMessages(Chat::getMsgs((int) Auth::getUser()->id, (int) $partner_id)),
		];
		print json_encode($resData, JSON_UNESCAPED_UNICODE);
	}

	public static function printNewMsgs($partner_id, $last_id = 0) {
		$resData = [
			'msgs' => self::prepareMessages(Chat::getMsgs((int) Auth::getUser()->id, (int) $partner_id, $last_id)),
		];
		print json_encode($resData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * Check partner
	 *
	 * @param $user       User User model
	 * @param $partner_id int Partner id
	 *
	 * @return bool
	 */
	public static function checkPartner($user, $partner_id) {
		if ($partner_id < 1)
			return FALSE;

		if ($user->id == $partner_id)
			return FALSE;

		/**
		 * @var $partner Model
		 * @var $partner User
		 */
		$partner = User::find($partner_id);

		if (empty($partner))
			return FALSE;

		if ($user->sex == $partner->sex) // no homophobia ))
			return FALSE;

		return TRUE;
	}

	/**
	 * @param $request \Request
	 *
	 * @return bool
	 */
	public static function checkChatPageRequestData($request) {
		if ($request->segment(4) == NULL)
			return TRUE;

		return self::checkPartner($request->user(), (int) $request->segment(4));
	}

	public static function needStartCheck($partner_id) {
		if (Auth::getUser()->sex == 'w')
			return FALSE;
		return ChatRooms::needStartCheck(Auth::getUser()->id, $partner_id);
	}

	public static function needStopCheck($partner_id) {
		if (Auth::getUser()->sex == 'w')
			return FALSE;
		return !ChatRooms::needStartCheck(Auth::getUser()->id, $partner_id);
	}

}
