<?php namespace App\Services;

use App\Models\BillingLog;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PayPalService
{

    private $_api_context;

    public function __construct()
    {
        $paypal_conf = \Config::get('paypal');

        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    /**
     * @param float $total
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function payment($total)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        // add item to list
		$item_list = new ItemList();
		$item_list->setItems([
			(new Item())->setName('Balance recharge')->setCurrency('USD')->setQuantity(1)->setPrice($total),
		]);

        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($total);

        $transaction = new Transaction();
        $transaction->setAmount($amount)->setItemList($item_list)->
        setDescription('Payment from site');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(url(\Lang::getLocale() . '/cabinet/payment'))->setCancelUrl(url(\Lang::getLocale() . '/cabinet/payment'));

        $payment = new Payment();
        $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Unknown error occurred');
        }

        foreach ($payment->getLinks() as $link)
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }

        // add payment ID to session
        \Session::put('paypal_payment_id', $payment->getId());
        \Session::save();

        if (isset($redirect_url))
            // redirect to paypal
            return \Redirect::away($redirect_url);

        return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Unknown error occurred');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getPaymentStatus()
    {
        // Get the payment ID
        \Session::start();
        $payment_id = \Session::get('paypal_payment_id');
        \Session::forget('paypal_payment_id');

        // Check the payment ID
        if (empty($payment_id)) {
            if (\Config::get('app.debug'))
                \Log::error('PayPal error: not found \'paypal_payment_id\' in \App\Services\PayPalService::getPaymentStatus()');
            return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Request is expired');
        }

        if (empty(\Input::get('PayerID')) || empty(\Input::get('token')))
            return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Payment failed');

        try {
            $payment = Payment::get($payment_id, $this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Payment failed');
        }

        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId(\Input::get('PayerID'));

        //Execute the payment
        try {
            $result = $payment->execute($execution, $this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Payment failed');
        }

        if ($result->getState() == 'approved') { // payment made
            $amount = (float)$result->getTransactions()[0]->amount->total;
            $user = \Auth::getUser();
            $user_balance = $user->billing['balance'];

            $user->billing()->update(['balance' => $user_balance + $amount]);
            BillingLog::create([
                'billing_id' => $user->billing['id'],
                //				'partner_id' => 0,
                'event' => 'recharge',
                'before' => $user_balance,
                'difference' => $amount,
                'after' => $user_balance + $amount,
            ]);

            return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Payment success');
        }

        return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Payment failed');
    }
}
