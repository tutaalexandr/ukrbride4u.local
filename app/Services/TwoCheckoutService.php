<?php namespace App\Services;

use App\Models\BillingLog;
use Illuminate\Http\Request;

class TwoCheckoutService
{

    const CHECKOUT_URL = 'https://www.2checkout.com/checkout/purchase';
    const SANDBOX_CHECKOUT_URL = 'https://sandbox.2checkout.com/checkout/purchase';

    private $config;

    public function __construct()
    {
        $this->config = \Config::get('2checkout');
    }

    public function getStandardCheckoutForm()
    {
        $action = $this->config['sandbox'] ? self::SANDBOX_CHECKOUT_URL : self::CHECKOUT_URL;
        $sid = $this->config['sid'];
        $name = 'Balance recharge';
        $decription = 'Balance recharge from user ' . \Auth::user()->name . ' (' . \Auth::user()->id . ')';
        $email = \Auth::user()->email;
        $phone = \Auth::user()->phone;

        return "
        <form action='$action' method='post'>
            <input type='hidden' name='sid' value='$sid' >
            <input type='hidden' name='mode' value='2CO' >
            <input type='hidden' name='li_0_type' value='product' >
            <input type='hidden' name='li_0_name' value='$name' >
            <input type='hidden' name='li_0_description' value='$decription' >
            <input type='hidden' name='li_0_quantity' value='1' >
            <input type='number' name='li_0_price' required min='1' step='1' value='10' autofocus >
            <input type='hidden' name='li_0_tangible' value='N' >
            <input type='hidden' name='currency_code' value='USD' >
            <input type='hidden' name='email' value='$email' >
            <input type='hidden' name='phone' value='$phone' >
			<button type='submit'>
				Pay with <b>2Checkout</b>
			</button>
        </form>
        ";
    }

    /**
     * @param $request Request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function parseDirectReturnCallback($request)
    {
        $myKey = strtoupper(md5($this->config['secret_word'] . $this->config['sid'] . $request->get('order_number') . $request->get('total')));
        if ($request->get('key') == $myKey) {
            $amount = (float)$request->get('total');
            $user = $request->user();
            $user_balance = $user->billing['balance'];

            $user->billing()->update(['balance' => $user_balance + $amount]);
            BillingLog::create([
                'billing_id' => $user->billing['id'],
                'event'      => 'recharge',
                'before'     => $user_balance,
                'difference' => $amount,
                'after'      => $user_balance + $amount,
            ]);

            return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Payment success! =)');
        }

        \Log::error('error in payment', $request->all());

        return \Redirect::to(url(\Lang::getLocale() . '/cabinet/payments'))->with('flashMsg', 'Payment failed! =(');
    }
}
