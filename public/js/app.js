jQuery.fn.extend({
	insertAtCaret: function (myValue) {
		return this.each(function (i) {
			if (document.selection) {
				// Для браузеров типа Internet Explorer
				this.focus();
				var sel = document.selection.createRange();
				sel.text = myValue;
				this.focus();
			}
			else if (this.selectionStart || this.selectionStart == '0') {
				// Для браузеров типа Firefox и других Webkit-ов
				var startPos = this.selectionStart;
				var endPos = this.selectionEnd;
				var scrollTop = this.scrollTop;
				this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
				this.focus();
				this.selectionStart = startPos + myValue.length;
				this.selectionEnd = startPos + myValue.length;
				this.scrollTop = scrollTop;
			}
			else {
				this.value += myValue;
				this.focus();
			}
		})
	}
});


function scrollWindowTo(px) {
	$('body, html').stop(true).animate({scrollTop: px}, 500);
}

function scrollElToBottom(el) {
	el.stop(true).animate({scrollTop: el[0].scrollHeight}, 1000);
}

$(document).ready(function () {
	$('.flash-msg').show().delay(3000).fadeOut(3000);
});

$.strPadLeft = function (i, l, s) {
	var o = i.toString();
	if (!s) {
		s = '0';
	}
	while (o.length < l) {
		o = s + o;
	}
	return o;
};

function cyr2lat(str) {
	var cyr2latChars = new Array(
		['а', 'a'],
		['б', 'b'],
		['в', 'v'],
		['г', 'g'],
		['д', 'd'],
		['е', 'e'],
		['ё', 'yo'],
		['ж', 'zh'],
		['з', 'z'],
		['и', 'i'],
		['й', 'y'],
		['к', 'k'],
		['л', 'l'],
		['м', 'm'],
		['н', 'n'],
		['о', 'o'],
		['п', 'p'],
		['р', 'r'],
		['с', 's'],
		['т', 't'],
		['у', 'u'],
		['ф', 'f'],
		['х', 'h'],
		['ц', 'c'],
		['ч', 'ch'],
		['ш', 'sh'],
		['щ', 'shch'],
		['ъ', ''],
		['ы', 'y'],
		['ь', ''],
		['э', 'e'],
		['ю', 'yu'],
		['я', 'ya'],

		['А', 'a'],
		['Б', 'b'],
		['В', 'v'],
		['Г', 'g'],
		['Д', 'd'],
		['Е', 'e'],
		['Ё', 'yo'],
		['Ж', 'zh'],
		['З', 'z'],
		['И', 'i'],
		['Й', 'y'],
		['К', 'k'],
		['Л', 'l'],
		['М', 'm'],
		['Н', 'n'],
		['О', 'o'],
		['П', 'p'],
		['Р', 'r'],
		['С', 's'],
		['Т', 't'],
		['У', 'u'],
		['Ф', 'f'],
		['Х', 'h'],
		['Ц', 'c'],
		['Ч', 'ch'],
		['Ш', 'sh'],
		['Щ', 'shch'],
		['Ъ', ''],
		['Ы', 'y'],
		['Ь', ''],
		['Э', 'e'],
		['Ю', 'yu'],
		['Я', 'ya'],

		[' ', '-']
	);

	var newStr = new String();
	var ch = '';
	var newCh = '';

	for (var i = 0; i < str.length; i++) {
		ch = str.charAt(i);
		newCh = ch;

		for (var j = 0; j < cyr2latChars.length; j++) {
			if (ch == cyr2latChars[j][0]) {
				newCh = cyr2latChars[j][1];
				break;
			}
		}
		newStr += newCh;
	}
	newStr = newStr.replace(/[`]|[-]|[=]|[\\]|[~]|[!]|[@]|[#]|[$]|[%]|[\^]|[&]|[*]|[(]|[)]|[_]|[+]|[|]|[,]|[.]|[/]|[<]|[>]|[?]|["]|[']|[№]|[:]|[;]/gim, '-');

	return newStr.replace(/[-]{2,}/gim, '-').replace(/\n/gim, '');
}
