$(document).ready(function () {
	$('[title]').tooltip();

	var ch = $(window).height() - $('.header').height() - $('.footer').height();
	$('.content').css('min-height', ch);
});
