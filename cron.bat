@echo off

:::::::::::::::::::::::::::::::::::::::
set phpInterpreter=php
set timeInterval=60
:::::::::::::::::::::::::::::::::::::::

:start
echo Start time %TIME%
%phpInterpreter% artisan schedule:run
echo End time   %TIME%
echo.
timeout /t %timeInterval% /nobreak>nul
goto start