<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Config::get('app.debug'))
			$this->down();

		if (!Schema::hasTable('chat_rooms'))
			Schema::create('chat_rooms', function (Blueprint $table) {
				$table->bigIncrements('id');

				$table->integer('man_id');
				$table->integer('lady_id');

				$table->tinyInteger('active')->default('0');

				$table->tinyInteger('man_delete')->default('0');
				$table->tinyInteger('lady_delete')->default('0');
				$table->tinyInteger('man_erase')->default('0');
				$table->tinyInteger('lady_erase')->default('0');

				$table->timestamps();

				$table->index('man_id');
				$table->index('lady_id');
				$table->index('active');

				$table->unique(['man_id', 'lady_id']);
			});

		if (!Schema::hasTable('chat_rooms_log'))
			Schema::create('chat_rooms_log', function (Blueprint $table) {
				$table->bigIncrements('id');

				$table->unsignedBigInteger('chat_room_id');

				$table->timestamp('start_time');
				$table->timestamp('payment_time')->nullable();
				$table->timestamp('end_time')->nullable();
				$table->tinyInteger('paid_complete')->default(0);
				$table->decimal('paid', 9, 3)->default(0);

				$table->foreign('chat_room_id')->references('id')->on('chat_rooms')->onDelete('cascade');

				$table->index('chat_room_id');
				$table->index('paid_complete');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('chat_rooms_log');
		Schema::dropIfExists('chat_rooms');
	}

}
