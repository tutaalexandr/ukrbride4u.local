<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Config::get('app.debug'))
			$this->down();

		Schema::create('chat', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('user_id');
			$table->integer('partner_id');
			$table->text('msg');
			$table->tinyInteger('viewed')->default('0');
			$table->timestamp('created_at');

			$table->index('user_id');
			$table->index('partner_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if (Schema::hasTable('chat'))
			Schema::drop('chat');
	}
}
