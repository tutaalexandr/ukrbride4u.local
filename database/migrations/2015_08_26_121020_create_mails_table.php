<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Config::get('app.debug'))
			$this->down();

		Schema::create('mail', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('partner_id');
			$table->text('msg');
			$table->string('ArrayIdGifts', 255);
			$table->string('Imagedounload', 255);

			$table->tinyInteger('viewed')->default('0');
			$table->tinyInteger('user_delete')->default('0'); // letter in deleted list
			$table->tinyInteger('partner_delete')->default('0'); // letter in deleted list
			$table->tinyInteger('user_erase')->default('0'); // delete letter from deleted list
			$table->tinyInteger('partner_erase')->default('0'); // delete letter from deleted list

			$table->timestamp('created_at');

			$table->index('user_id');
			$table->index('partner_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if (Schema::hasTable('mail'))
			Schema::drop('mail');
	}

}
