<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChoiseAdminUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('choise_admin_users', function(Blueprint $table)
		{
			$table->increments('id_primary');
			$table->tinyInteger('id')->default('19');
			$table->string('sex', 5)->default('w');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('choise_admin_users');
	}

}
