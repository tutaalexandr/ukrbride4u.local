<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Config::get('app.debug'))
			$this->down();

		if (!Schema::hasTable('users'))
			Schema::create('users', function (Blueprint $table) {
				$table->increments('id');

				$table->string('email', 50);
				$table->string('password', 60);

				$table->string('phone', 20)->nullable();

				$table->string('name', 50);
				$table->enum('sex', array('m', 'w'));
				$table->date('bdate')->nullable();
				$table->string('address', 255)->nullable();
				$table->string('main_photo', 255)->nullable();
				$table->tinyInteger('weight')->nullable();
				$table->tinyInteger('height')->nullable();
				$table->enum('zodiac', array(
					'aquarius',
					'aries',
					'cancer',
					'capricorn',
					'gemini',
					'leo',
					'libra',
					'pisces',
					'sagittarius',
					'scorpio',
					'taurus',
					'virgo',
				))->nullable();
				$table->enum('eyes', array(
					'amber',
					'blue',
					'brown',
					'gray',
					'green',
					'hazel',
				))->nullable();
				$table->enum('hair', array(
					'black',
					'blond',
					'chestnutBrown',
					'darkBrown',
					'lightChestnutBrownHair',
					'mediumBrown',
					'red',
					'white',
					'bald',
				))->nullable();
				$table->enum('en_level', array(
					'beginner',
					'elementary',
					'preIntermediate',
					'intermediate',
					'upperIntermediate',
					'advanced',
					'native',
				))->nullable();
				$table->text('about_me')->nullable();
				$table->string('youtube', 255)->nullable();

				$table->enum('rights', array('superadmin', 'admin', 'user'));
				$table->string('locale', 2)->default(Lang::getLocale());
				$table->tinyInteger('viewer')->default('1');
				$table->tinyInteger('active')->default('1');
				$table->rememberToken();
				$table->timestamps();
				$table->timestamp('last_visit_at');

				$table->index('email');
				$table->index('sex');
			});

		if (!Schema::hasTable('user_photos'))
			Schema::create('user_photos', function (Blueprint $table) {
				$table->increments('id');

				$table->unsignedInteger('user_id');
				$table->string('img', 255);

				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

				$table->index('user_id');
			});

		if (!Schema::hasTable('user_favorites'))
			Schema::create('user_favorites', function (Blueprint $table) {
				$table->increments('id');

				$table->unsignedInteger('user_id');
				$table->unsignedInteger('fav_id');

				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
				$table->foreign('fav_id')->references('id')->on('users')->onDelete('cascade');

				$table->unique(['user_id', 'fav_id']);
				$table->index('user_id');
			});

		if (!Schema::hasTable('billing'))
			Schema::create('billing', function (Blueprint $table) {
				$table->increments('id');

				$table->unsignedInteger('user_id');
				$table->decimal('balance', 9, 3);

				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

				$table->timestamps();

				$table->index('user_id');
			});

		if (!Schema::hasTable('billing_log'))
			Schema::create('billing_log', function (Blueprint $table) {
				$table->bigIncrements('id');

				$table->unsignedInteger('billing_id');
				$table->unsignedInteger('partner_id')->nullable();

				$table->enum('event', [
					'real_gift',
					'virt_gift',
					'chat',
					'recharge',
					'mail',
				]);

				$table->decimal('before', 9, 3);
				$table->decimal('difference', 9, 3);
				$table->decimal('after', 9, 3);

				$table->timestamp('created_at');

				$table->foreign('billing_id')->references('id')->on('billing')->onDelete('cascade');
				$table->foreign('partner_id')->references('id')->on('users')->onDelete('cascade');

				$table->index('billing_id');
				$table->index('partner_id');
			});

		$this->addDefaultUsers();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('billing_log');
		Schema::dropIfExists('billing');
		Schema::dropIfExists('user_favorites');
		Schema::dropIfExists('user_photos');
		Schema::dropIfExists('users');

		Cache::forget('users_zodiac_list');
		Cache::forget('users_eyes_list');
		Cache::forget('users_hair_list');
		Cache::forget('users_en_level_list');
		Cache::forget('users_rights_list');

		if (file_exists(public_path() . "/img/pr/"))
			rrmdir(public_path() . "/img/pr/");
	}

	protected function addDefaultUsers() {
		\App\Models\User::create([
			'name'     => 'xZ1mEFx',
			'sex'      => 'm',
			'email'    => 'vitaliizagorodniuk@gmail.com',
			'password' => Hash::make('P@$$w0rd'),
			'phone'    => '+380506148847',
			'address'  => 'Ukraine, Odessa',
			'bdate'    => '1985-02-10',
			'rights'   => 'superadmin',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => 100000]));

		\App\Models\User::create([
			'name'     => 'Upperline',
			'sex'      => 'm',
			'email'    => 'vk@upperlinestudio.com',
			'password' => Hash::make('P@$$w0rd'),
			'phone'    => '+000000000000',
			'address'  => 'Ukraine, Odessa',
			'bdate'    => '2000-01-01',
			'rights'   => 'superadmin',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => 100000]));
	}

}
