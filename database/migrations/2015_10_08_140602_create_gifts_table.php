<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGiftsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Config::get('app.debug'))
			$this->down();

		Schema::create('gifts', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title', 255)->nullable();
			$table->string('price', 255)->nullable();
			$table->string('oldprice', 255)->nullable();
			$table->tinyInteger('isreal')->default('0');
			$table->string('image', 255)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if (Schema::hasTable('gifts'))
			Schema::drop('gifts');
	}

}
