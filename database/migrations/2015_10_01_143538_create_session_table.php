<?php

use Illuminate\Database\Migrations\Migration;

class CreateSessionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Config::get('app.debug'))
			$this->down();

		Schema::create('sessions', function ($t) {
			$t->string('id')->unique();
			$t->text('payload');
			$t->integer('last_activity');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if (Schema::hasTable('sessions'))
			Schema::drop('sessions');
	}

}
