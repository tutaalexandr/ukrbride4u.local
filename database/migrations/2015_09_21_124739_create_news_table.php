<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		if (Config::get('app.debug'))
			$this->down();

		Schema::create('News', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title', 255)->nullable();
			$table->text('preview')->nullable();
			$table->text('text')->nullable();
			$table->string('meta_title', 255)->nullable();
			$table->string('meta_description', 255)->nullable();
			$table->string('meta_keywords', 255)->nullable();
			$table->string('url_name', 255)->nullable();
			$table->string('image')->nullable();
			$table->string('video')->nullable();
			$table->tinyInteger('author')->default('0');
			$table->tinyInteger('news_active')->default('0');
			$table->tinyInteger('newsOrStories')->default('0');
			$table->dateTime('news_date')->nullable();
			$table->dateTime('update_news_date')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		if (Schema::hasTable('News'))
			Schema::drop('News');
	}

}
