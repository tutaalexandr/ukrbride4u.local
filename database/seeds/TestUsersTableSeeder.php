<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class TestUsersTableSeeder extends Seeder {

	public function run() {

		User::create([
			'name'     => 'James',
			'sex'      => 'm',
			'email'    => 'test_m1@test.com',
			'password' => Hash::make('test_m1'),
			'phone'    => '+0000000000',
			'rights'   => 'user',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => rand(100, 1000)]));

		User::create([
			'name'     => 'John',
			'sex'      => 'm',
			'email'    => 'test_m2@test.com',
			'password' => Hash::make('test_m2'),
			'phone'    => '+0000000000',
			'rights'   => 'user',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => rand(100, 1000)]));

		User::create([
			'name'     => 'Nick',
			'sex'      => 'm',
			'email'    => 'test_m3@test.com',
			'password' => Hash::make('test_m3'),
			'phone'    => '+0000000000',
			'rights'   => 'user',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => rand(100, 1000)]));

		#####################################################################

		User::create([
			'name'     => 'Ирина',
			'sex'      => 'w',
			'email'    => 'test_w1@test.com',
			'password' => Hash::make('test_w1'),
			'phone'    => '+0000000000',
			'rights'   => 'user',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => rand(100, 1000)]));

		User::create([
			'name'     => 'Ольга',
			'sex'      => 'w',
			'email'    => 'test_w2@test.com',
			'password' => Hash::make('test_w2'),
			'phone'    => '+0000000000',
			'rights'   => 'user',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => rand(100, 1000)]));

		User::create([
			'name'     => 'Светлана',
			'sex'      => 'w',
			'email'    => 'test_w3@test.com',
			'password' => Hash::make('test_w3'),
			'phone'    => '+0000000000',
			'rights'   => 'user',
			'viewer'   => 0,
			'active'   => 1,
		])->billing()->save(new \App\Models\Billing(['balance' => rand(150, 1000)]));

	}
}
