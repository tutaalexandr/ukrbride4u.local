<?php
use App\Models\Gifts;
use Illuminate\Database\Seeder;

class TestGiftsTableSeeder extends Seeder {

	public function run() {
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => '2kolca.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'cvety.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'kolco.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'mischka.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'zayac.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'buket.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'izumrud.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'lubov.png',
		]);
		Gifts::create([
			'title'    => '1',
			'price'    => '0.1',
			'oldprice' => '0.1',
			'isreal'   => 0,
			'image'    => 'poceluy.png',
		]);
		/*Real Gifts*/
		Gifts::create([
			'title'    => 'Basket of flowers',
			'price'    => '50',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Basket of flowers.jpg',
		]);
		Gifts::create([
			'title'    => 'Basket of food',
			'price'    => '60',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Basket of food.jpg',
		]);
		Gifts::create([
			'title'    => 'Basket of fruits',
			'price'    => '30',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Basket of fruits.jpg',
		]);
		Gifts::create([
			'title'    => 'Big toy',
			'price'    => '40',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Big toy.jpg',
		]);
		Gifts::create([
			'title'    => 'Black Opium, Yves Saint Laurent, 50ml',
			'price'    => '90',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Black Opium, Yves Saint Laurent, 50ml.jpg',
		]);
		Gifts::create([
			'title'    => 'Chanel Chance Eau Vive',
			'price'    => '80',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Chanel Chance Eau Vive.jpg',
		]);
		Gifts::create([
			'title'    => 'Dolce Floral Drops, Dolce&Gabbana (50 ml)',
			'price'    => '90',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Dolce Floral Drops, Dolce&Gabbana (50 ml).jpg',
		]);
		Gifts::create([
			'title'    => 'Huge toy',
			'price'    => '100',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'huge toy.jpg',
		]);
		Gifts::create([
			'title'    => 'Chocolate Candies',
			'price'    => '10',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Chocolate Candies.jpg',
		]);
		Gifts::create([
			'title'    => 'Flower composition',
			'price'    => '50',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Flower composition.jpg',
		]);
		Gifts::create([
			'title'    => 'Medium toy',
			'price'    => '30',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Medium toy.jpg',
		]);
		Gifts::create([
			'title'    => 'Si Giorgio Armani, 50 ml',
			'price'    => '90',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Si Giorgio Armani, 50 ml.jpg',
		]);
		Gifts::create([
			'title'    => 'Small toy',
			'price'    => '15',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'Small toy.jpg',
		]);
		Gifts::create([
			'title'    => 'White pink roses',
			'price'    => '40',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'White pink roses.jpg',
		]);
		Gifts::create([
			'title'    => 'White small roses',
			'price'    => '40',
			'oldprice' => '0.1',
			'isreal'   => 1,
			'image'    => 'White small roses.jpg',
		]);
	}
}
