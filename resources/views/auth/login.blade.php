@if (count($errors->login) > 0)
	<div>
		<ul>
			@foreach ($errors->login->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="home-login-form">
	<h2>Authorization</h2>
	<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/auth/login') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div>
			<label>E-Mail Address</label>
			<input type="email" name="email" value="{{ old('email') }}">
		</div>

		<div>
			<label>Password</label>
			<input type="password" name="password">
		</div>

		<div>
			<input type="checkbox" name="remember">
			<label>Remember me</label>
		</div>

		<div>
			<button type="submit">
				Login
			</button>
		</div>
	</form>
</div>
