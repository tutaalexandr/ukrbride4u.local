@if (count($errors) > 0)
	<div>
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="home-reg-form">
	<h2>Registration</h2>
	<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/auth/register-man') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div>
			<label>Name</label>
			<input type="text" name="name" value="{{ old('name') }}">
		</div>

		<div>
			<label>E-Mail Address</label>
			<input type="email" name="email" value="{{ old('email') }}">
		</div>

		<div>
			<label>Password</label>
			<input type="password" name="password" value="{{ old('password') }}">
		</div>

		<div>
			<label>Confirm Password</label>
			<input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
		</div>

		<div>
			<button type="submit">
				Register
			</button>
		</div>
	</form>
</div>
