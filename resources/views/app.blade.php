<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}">
	<head>
		<meta charset="utf-8">
		<title>{{ $pageTitle or trans('main.pageTitle' )}}</title>

		{{-- SEO languages --}}
		<link rel="alternate" hreflang="en" href="{{ asset('en') }}">
		<link rel="alternate" hreflang="ru" href="{{ asset('ru') }}">

		{{-- styles --}}
		<link href="{{ asset('js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css') }}" rel="stylesheet">
		<link rel="stylesheet" href="{{ asset('js/jquery/fancybox/source/jquery.fancybox.css') }}" type="text/css" media="screen" />
		<link rel="stylesheet" href="{{ asset('js/jquery/perfect-scrollbar/css/perfect-scrollbar.min.css') }}" type="text/css" media="screen" />
		<link href="{{ asset('css/app.css') }}" rel="stylesheet" />
		@yield('css')

		{{-- scripts --}}
		<script type="text/javascript" src="{{ asset('js/jquery/jquery-2.1.4.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery/fancybox/source/jquery.fancybox.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/jquery/autosize-master/dist/autosize.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
		@yield('js')
		{{--<script src="http://mendog.ru/media/?t=video;w=425;h=350;f=http%3A%2F%2Fmendog.ru%2Fswf%2Fnewyear.swf" type="text/javascript"></script>--}}
	</head>
	<body>

		@if (Session::has('flashMsg'))
			<div class="flash-msg">{{ Session::get('flashMsg') }}</div>
		@endif
		<div class="wrapper">
			<div class="header">
				@yield('header')
			</div>
			<div class="content">
				@yield('content')
			</div>
			<div class="footer">
				@yield('footer')
			</div>
		</div>
		<div>
			@yield('additional_data')
		</div>
	</body>
</html>
