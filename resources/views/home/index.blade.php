@extends('...app')


@section('css')
	<link href="{{ asset('css/home/index.css') }}" rel="stylesheet" />
@stop


@section('js')
	@include('home.inc.js')
	<script type="text/javascript">
		$(document).ready(function () {
			;
		});
	</script>
@stop


@section('header')
	@include('home.inc.header')
@stop


@section('content')
	<div class="online">
		<h1>@lang('home.online_ladies_title')</h1>
		@foreach ($onlineUsersList as $user)
			<div class="girls" style="background-image: url('{{ empty($user->main_photo) ? asset('img/no_image_' . $user->sex . '.png') : asset($user->main_photo) }}');">
				<a href="#popupLogin">
					<ul class="name">
						<li class="names">{{ $user->name }}</li>
						<li class="old">{{ $user->age }}</li>
					</ul>
				</a>
			</div>
		@endforeach
	</div>

	<div class="story">
		<div>
			<h2>@lang('home.suc_stories_title')</h2>
		</div>
		<div id="Main">
			@if(!empty($Story))
				<p>{{ str_limit(strip_tags($Story->text), 500)  }}</p>
				<p><strong>{{ $Story->news_date  }}</strong></p>
			@endif
		</div>
		<a href="{{ url(Lang::getLocale() . '/love-story') }}">@lang('home.button_more_stories')</a>
	</div>

	<div class="news">
		<div id="left">
			<h1>@lang('home.news_title')</h1>
			@foreach($News as $New)
				<div class="news1">
					<h4>
						<a href="{{ url(Lang::getLocale() . '/news/'.$New->url_name) }}" style="color: #80217e;">{{ $New->title }}</a>
					</h4>
					<p>{{ str_limit(strip_tags($New->text), 120)  }}</p>
				</div>
			@endforeach
		</div>
		<!-- 		<div id="right"><h1>@lang('home.video_title')</h1>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/qlT0xNAnuSU" frameborder="0" allowfullscreen></iframe>
</div> -->
	</div>
@stop


@section('footer')
	@include('home.inc.footer')
@stop


@section('additional_data')
	@include('home.inc.login-popup')
@stop
