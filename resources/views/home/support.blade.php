@extends('...app')


@section('css')
<link href="{{ asset('css/home/w-reg.css') }}" rel="stylesheet" />
@stop


@section('js')
@include('home.inc.js')

@stop


@section('header')
@include('home.inc.header')
@stop


@section('content')
<script type="text/javascript">

function mail() {
	if (document.getElementById('name').value < 2) {
		document.getElementById('name').style.borderColor = "red";
		err = true;
	}
	else {
		document.getElementById('name').style.borderColor = "#009bb2";
		err = false;
	}
	if (document.getElementById('mesege').value < 2) {
		document.getElementById('mesege').style.borderColor = "red";
		err1 = true;
	}
	else {
		document.getElementById('mesege').style.borderColor = "#009bb2";
		err1 = false;
	}
	if (!err || !err1) {
		var name = document.getElementById('name');
		name = name.value;
		var mesege = document.getElementById('mesege');
		mesege = mesege.value;
		$.ajax({
			async:      true,
			url:        '{{ url(Lang::getLocale() . '/support') }}',
			type:       'POST',
			data:       '_token={{ csrf_token() }}&name=' + name + '&' + 'mesege=' + mesege,
			beforeSend: function () {
			},
			success:    function (data) {
				if (data === 'true') {					
					window.location = '{{ url(Lang::getLocale()) }}';
				}
				else {
					window.location = '{{ url(Lang::getLocale()) }}';
				}
			}
		});

	}
	return false ;
};

</script>
<h1>OUR SUPPORT</h1>
<form id="myform" method="POST" action="{{ url(Lang::getLocale() . '/support') }}" onSubmit="return checkedForm(this)">
	<div style = "float:none;">
		<label id="labelphone" for="telephone">@lang('w-reg.Your_name')</label>
		<br />
		<input name="name" id="name" value="">
		<br />
	</div>
	<div style = "float:none;">
		<label id="labelname" for="name">COMMENTS</label>
		<br />
		<textarea style ="
		float:none;
		border: 1px solid #009BB2;
		padding: 1px 3px;
		font-size: 12px;
		width: 433px;
		height: 102px;
		" name="mesege" id="mesege"  value=""></textarea>
	</div>
</form>

<a  id = 'send' onclick = "mail(); return false ; ">
	<div>@lang('w-reg.Lorem_Send')</div>
</a>
<script type="text/javascript">

/*function run(){
	var phone = document.getElementById('telephone').value = '';
	var phone = document.getElementById('name').value = '';
	return false ; 
};*/

</script>
@stop


@section('footer')
@include('home.inc.footer')
@stop


@section('additional_data')
@include('home.inc.login-popup')
@stop
