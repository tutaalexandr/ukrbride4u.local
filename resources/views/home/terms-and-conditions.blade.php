@extends('...app')


@section('css')
	<link href="{{ asset('css/home/about-us.css') }}" rel="stylesheet">
@stop


@section('js')
	@include('home.inc.js')
	<script type="text/javascript">
		$(document).ready(function () {
			;
		});
	</script>
@stop


@section('header')
	@include('home.inc.header')
@stop


@section('content')
	<div class="left_content">
		@if(isset($News))
			@foreach($News as $New)
				<div class="text">
					<h1>{{ $New->title }}</h1>
					<div class="first">{!! $New->text  !!}</div>
				</div>
			@endforeach
		@else

        <p><strong>This agreement is an e-variant of document that determines and regulates the rules of membership in site "UkrBride4U.com".</strong></p>

<p><strong>After registration in site you automatically agree with all terms described below and you are responsible for their non-observance. Having accepted this Agreement you guarantee that:</strong>- your age is not under 18<br />
- you agree to receive notifying from site UkrBride4U<br />
- you weren't convicted or were not under examination for moral or sexual crimes<br />
- you don't prosecute commercial purposes<br />
- you are responsible for keeping materials and information published in our site "UkrBride4U.com"; for safety and confidentiality of data for your authorization in site.<br />
- you are under an obligation not to publish in the open access of e-mail, post address, telephone numbers, references and other contacts and also:<br />
- unprintable statements and also materials that evidently or by implication offend human dignity of Users and third sides;<br />
- porno materials.<br />
<br />
<br />
<strong>The Administration of site reserves to itself the right to:</strong>-make changes in this Agreement in unilateral way.<br />
-modify Site at its discretion<br />
-edit or delete materials, that you have published in Site, if they are not correspondent to the terms of this Agreement and hurt Site and third sides.<br />
-interrupt or delete your profile, if it isn't correspondent to the terms of this Agreement and hurts Site or third sides.<br />
-change the types of services, terms of their action.<br />
All changes come into force from the moment of their publishing in site.<br />
<br />
<br />
<strong>The Administration of Site isn't responsible for:</strong>-keeping materials published by you, their authenticity and accuracy.<br />
-making harm, injury, loosing of information or making any other damages that were made during using of service.<br />
-any actions made by you deliberately or accidentally;<br />
-delays or breaks in access to our services;<br />
-events that are not subjects of our control.<br />
If you are citizen of the USA or live there and you want to get contact information or meet personally non-american citizen, you must fill the form IMBRA (International Marriage Broker Regulation Act), according to the USA legislation.<br />
You can get to know with this document and fill the form here:<br />
Thank you for use of our site!<br />
<br />
<br />
<strong>The administration of UkrBride4U provides services of sending presents and communication with women</strong>All purchases are not liable to return.<br />
The whole sum of money spending on services won’t be repaid.<br />
Your money will be paid back only for the credits that you haven’t spent in case your turning to the support of site.<br />
Read the instructions attentively and carefully before making purchases.<br />
<br />
Yours sincerely, UkrBride4U</p>

		@endif
	</div>
	<div class="right_content">
		<h1>Our girls</h1>
		@foreach($ladiesList as $lady)
			<div class="girls" style="background-image: url('{{ empty($lady->main_photo) ? asset('img/no_image_' . $lady->sex . '.png') : asset($lady->main_photo) }}');">
				<a href="#popupLogin">
					<div class="name_right">
						<ul class="name">
							<li class="names">{{ $lady->name }}</li>
							<li class="old">{{ $lady->age }}</li>
						</ul>
					</div>
				</a>
			</div>
		@endforeach
	</div>
@stop


@section('footer')
	@include('home.inc.footer')
@stop


@section('additional_data')
	@include('home.inc.login-popup')
@stop
