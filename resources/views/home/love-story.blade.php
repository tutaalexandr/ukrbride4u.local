@extends('...app')


@section('css')
	<link href="{{ asset('css/home/love-story.css') }}" rel="stylesheet" />
@stop


@section('js')
	@include('home.inc.js')
	<script type="text/javascript">
		$(document).ready(function () {
			;
		});
	</script>
@stop


@section('header')
	@include('home.inc.header')
@stop


@section('content')
	<div class="left_content">
		<h1>love story</h1>
		@foreach($ggetstory as $story)
			<div class="story">
				@if (isset($story->image))
					<div class="story-img" style="background-image: url('{{ asset('upload/news') .'/'. $story->image}}');"></div>
				@endif
				<h2{!! empty($story->image) ? '' : ' class="story-title-padding"' !!}>
					<a href="{{ url(Lang::getLocale() . '/love-story/'.$story->url_name) }}" style="color: #80217e;">{{ $story->title }}</a>
				</h2>
				<div{!! empty($story->image) ? '' : ' class="story-content-margin"' !!}>
					{{ str_limit(strip_tags($story->text), 380)  }}
				</div>
			</div>
			<div class="clear"></div>
		@endforeach
	</div>
	<div class="right_content">
		<h1>Our girls</h1>
		@foreach($ladiesList as $lady)
			<div class="girls" style="background-image: url('{{ empty($lady->main_photo) ? asset('img/no_image_' . $lady->sex . '.png') : asset($lady->main_photo) }}');">
				<a href="#popupLogin">
					<div class="name_right">
						<ul class="name">
							<li class="names">{{ $lady->name }}</li>
							<li class="old">{{ $lady->age }}</li>
						</ul>
					</div>
				</a>
			</div>
		@endforeach
	</div>
@stop


@section('footer')
	@include('home.inc.footer')
@stop


@section('additional_data')
	@include('home.inc.login-popup')
@stop
