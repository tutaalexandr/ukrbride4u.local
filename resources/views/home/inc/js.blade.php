<script type="text/javascript">
	$(document).ready(function () {
		$('#last').bind('click', function () {
			$('.registration form').trigger('submit');
		});

		$('a[href="#popupLogin"]').fancybox({
			'scrolling': 'no',
			'titleShow': false,
		});
		@if (count($errors->login) > 0)
			$('a[href="#popupLogin"]').trigger('click');
		@endif;
	});
</script>
