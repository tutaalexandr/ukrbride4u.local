<div>
	<ul class="header-social-buttons">
		<li class="social">
			<a href="#"><img src="{{ asset('img/facebook.png') }}" alt="facebook"></a>
		</li>
		<li class="social">
			<a href="#"><img src="{{ asset('img/twitter.png') }}" alt="twitter"></a>
		</li>
		<li class="social">
			<a href="#"><img src="{{ asset('img/facebook.png') }}" alt="facebook"></a>
		</li>
		<li class="social">
			<a href="#"><img src="{{ asset('img/twitter.png') }}" alt="twitter"></a>
		</li>
	</ul>
	<div class="lang">
		<a class="ru" href="{{ url('ru') }}">rus</a>
		<a class="eng" href="{{ url('en') }}">eng</a>
	</div>
</div>

<div id="logo">
	<a href="{{ url(Lang::getLocale()) }}">
		<img src="{{ asset('img/melodyOfLove.svg') }}" alt="online-dating">
	</a>
</div>

<div class="registration">
	<h5>@lang('home.reg_form_title')</h5>
	<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/auth/register-man') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div>
			<label for="registerName">@lang('home.reg_form_name')</label>
			<input id="registerName" type="text" name="name" value="{{ old('name') }}">
		</div>
		<div>
			<label for="registerEmail">@lang('home.reg_form_email')</label>
			<input id="registerEmail" type="text" name="email" value="{{ old('email') }}">
			<p>@lang('home.reg_form_will_be_used_as_login')</p>
		</div>
		<div>
			<label for="registerPassword">@lang('home.reg_form_password')</label>
			<input id="registerPassword" type="password" name="password" value="{{ old('password') }}">
		</div>
		<div>
			<label for="registerPasswordConfirm">@lang('home.reg_form_password_confirm')</label>
			<input id="registerPasswordConfirm" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
		</div>
	</form>
	<span>
		<p>@lang('home.reg_form_sign_in_with')</p>
	</span>

	<a href="{{ url(\Lang::getLocale() . '/redirect') }}">
		<div class="button"></div>
	</a>
	<div>
		@if (count($errors->register) > 0)
		<ul class="errors">
			@foreach ($errors->register->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
		@else
		<p class="agreement-text"><a href="{{ url(\Lang::getLocale() . '/privacypolicy') }}" >@lang('home.reg_form_agreement')</a></p>
		@endif
	</div>
</div>
<div>
	<a href="{{ url(\Lang::getLocale() . '/w-reg') }}" id="first" class="head">
		@lang('home.button_for_ladies')
	</a>
	<div>
		<a class="head" href="{{ url(\Lang::getLocale() . '/about-us') }}">@lang('home.button_about_us')</a>
	</div>
	<div>
		<a class="head" href="#popupLogin">
			@lang('home.button_login')
		</a>
	</div>
</div>
<div id="blanket" style="display:none;"></div>

<div id="last">
	@lang('home.button_join_now')
</div>

</div>

@if (!Auth::guest())
@if (Auth::getUser()->rights == 'superadmin')
<div class="sa-menu">
	<h3>SuperAdmin Menu</h3>
	<hr />
	<a class="sa-button{{ (getCurrentControllerName() == 'HomeController') ? ' sa-button-selected' : '' }}" href="{{ url(\Lang::getLocale()) }}">Go To Home</a>
	<a class="sa-button{{ (getCurrentControllerName() == 'CabinetController') ? ' sa-button-selected' : '' }}" href="{{ url(\Lang::getLocale() . \Config::get('app.default_cabinet_page')) }}">Go To Cabinet</a>
	<a class="sa-button{{ (getCurrentControllerName() == 'AdminController') ? ' sa-button-selected' : '' }}" href="{{ url(\Lang::getLocale() . \Config::get('app.default_admin_page')) }}">Go To Admin</a>
</div>
@endif
@endif
