<div id="popupLogin" style="display: none;">
	<div class="home-login-form">
		<h1>@lang('home.login_form_title')</h1>
		@if (count($errors->login) > 0)
			<div>
				<ul class="login-errors">
					@foreach ($errors->login->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/auth/login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="mail">
				<label for="loginEmail">@lang('home.login_form_email')</label>
				<br />
				<input id="loginEmail" type="email" name="login" value="{{ old('login') }}">
				<br />
			</div>
			<div class="password">
				<label for="loginPassword">@lang('home.login_form_password')</label>
				<br />
				<input id="loginPassword" type="password" name="loginPassword" value="{{ old('loginPassword') }}">
			</div>
			<div class="rememberme">
				<input id="loginRemember" type="checkbox" name="remember"{{ old('remember') ? ' checked=""' : '' }}>
				<label for="loginRemember"><span></span>@lang('home.login_form_remember_me')</label>
			</div>
			<div>
				<button type="submit">
					@lang('home.button_login')
				</button>
			</div>
		</form>
	</div>
</div>
