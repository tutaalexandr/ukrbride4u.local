<div id="SmallLogo">
	<!--<img src="{{ asset('img/melodyOfLoveSmall.svg') }}" alt="logo" />-->
</div>
<!-- <div>
	<ul class="footer-social-buttons">
		<li class="social">
			<a href="#"><img src="{{ asset('img/facebook-gray.png') }}" alt="facebook"></a>
		</li>
		<li class="social">
			<a href="#"><img src="{{ asset('img/twitter-gray.png') }}" alt="twitter"></a>
		</li>
		<li class="social">
			<a href="#"><img src="{{ asset('img/facebook-gray.png') }}" alt="facebook"></a>
		</li>
		<li class="social">
			<a href="#"><img src="{{ asset('img/twitter-gray.png') }}" alt="twitter"></a>
		</li>
	</ul>
</div> -->
<div class="footer-links">
	<a href="{{ url(\Lang::getLocale() . '/privacypolicy') }}" >Privacy Policy</a></br>
	<a href="{{ url(\Lang::getLocale() . '/terms-and-conditions') }}" >Terms and conditions</a></br>
	<a href="{{ url(\Lang::getLocale() . '/support') }}" >Support</a>
</div>
<div class="footer-copyright">
	Copyright (с) by
	<a href="http://appartika.com">
		Appartika
	</a>
	<br />
	All rights reserved
	<br />
	<img src="{{ asset('img/telephone.png') }}" alt="telephone">
	048 777 00 00
</div>
