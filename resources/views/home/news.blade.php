@extends('...app')


@section('css')
	<link href="{{ asset('css/home/love-story.css') }}" rel="stylesheet" />
@stop


@section('js')
	@include('home.inc.js')
	<script type="text/javascript">
		$(document).ready(function () {
			;
		});
	</script>
@stop


@section('header')
	@include('home.inc.header')
@stop


@section('content')
		<div class="left_content">
			@foreach($News as $New)
				<h1>{{ $New->title }}</h1>
				<p class="first"> {{ $New->text  }} </p>
			@endforeach
		</div>
@stop


@section('footer')
	@include('home.inc.footer')
@stop


@section('additional_data')
	@include('home.inc.login-popup')
@stop
