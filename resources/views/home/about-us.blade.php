@extends('...app')


@section('css')
	<link href="{{ asset('css/home/about-us.css') }}" rel="stylesheet">
@stop


@section('js')
	@include('home.inc.js')
	<script type="text/javascript">
		$(document).ready(function () {
			;
		});
	</script>
@stop


@section('header')
	@include('home.inc.header')
@stop


@section('content')
	<div class="left_content">
		@if(isset($News))
			@foreach($News as $New)
				<div class="text">
					<h1>{{ $New->title }}</h1>
					<div class="first">{!! $New->text  !!}</div>
				</div>
			@endforeach
		@else
			<h1>About us</h1>
			<h3> Dear friends!</h3>
			<p class="first">Maybe you are our client already. Maybe you only plan to become a client. In any case I would like to tell something about our company.
Our company is unusual. Maybe from the first sight you will think that we do the same business that other sites do. But this is not really true.
We worked before. When we didn't have site. And we know what is it to be a foreigner in our country.</p>
<p>Site is only the first step. When you start communication on the site you have to understand that all girls are real. No girl can be registered on the site by some agency or by herself. All girls are registered by us after she makes special profile and when she is sure that she really wants to get married.
The second step is your trip to Ukraine. You have to understand that when you come here for our people you are wallet. I don't think that this is a problem. Because when people from Ukraine come to Italy it is the same. LOL. So you will need help. And we don't want to leave you alone like other marriage sites do. Or if they help they take a lot of money from you.  We want to help you. We want to meet you in the airport. We want to find appartments or good hotel for you (depends on what you want). We want to buy you Ukrainian number, so you will  be connected with ukrainians if you need to. And you will need to believe me. We want to show you restaurants where you can go and restaurants where you must not go. Of course we will not help for free, but for very small money. Because person who will help you needs to eat too:)
We want to do all of this, because we are really interested in making couples. We want you to have good opinion about Ukraine, about marriage business and ukrainian brides. That's why we work!
The third step. We want to make marriages fast. So 2 times in a year we will make socials. What is it? It is opportunity to meet many women in few days. Like fast dates in your country. Compared to other marriage agencies we will not take thousands dollars for that. We plan the social in August 2016. In spring we will tell more about it. But if you had plans to come to Ukraine this time, you should try socials. This is funnier and faster way to find someone serious.
I hope to become friends with all of us!</p>
<p>Be patient! To find a wife is not so easy, but not so hard. So if you sit on the site for 2-3 years and can't find anyone, means that you have to change something. Don't be shame or shy to connect with us!</p>
<p>Your friends, UkrBride4U Team</p>
		@endif
	</div>
	<div class="right_content">
		<h1>Our girls</h1>
		@foreach($ladiesList as $lady)
			<div class="girls" style="background-image: url('{{ empty($lady->main_photo) ? asset('img/no_image_' . $lady->sex . '.png') : asset($lady->main_photo) }}');">
				<a href="#popupLogin">
					<div class="name_right">
						<ul class="name">
							<li class="names">{{ $lady->name }}</li>
							<li class="old">{{ $lady->age }}</li>
						</ul>
					</div>
				</a>
			</div>
		@endforeach
	</div>
@stop


@section('footer')
	@include('home.inc.footer')
@stop


@section('additional_data')
	@include('home.inc.login-popup')
@stop
