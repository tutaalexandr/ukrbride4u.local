@extends('...app')


@section('css')
<link href="{{ asset('css/home/w-reg.css') }}" rel="stylesheet" />
@stop


@section('js')
@include('home.inc.js')
<script type="text/javascript">
function mail() {
	var phone = document.getElementById('telephone');
	phone = phone.value;
	var ereg = /^([0-9()+ ,-])+$/i;
	if (!ereg.test(phone)) {
		document.getElementById('telephone').style.borderColor = "red";
		err = true;
	}
	else {
		document.getElementById('telephone').style.borderColor = "#009bb2";
		err = false;
	}

	if (document.getElementById('name').value < 2) {
		document.getElementById('name').style.borderColor = "red";
		err = true;
	}
	else {
		document.getElementById('name').style.borderColor = "#009bb2";
		err = false;
	}
	if (!err) {
		var name = document.getElementById('name');
		name = name.value;
		var phone = document.getElementById('telephone');
		phone = phone.value;
		$.ajax({
			async:      true,
			url:        '{{ url(Lang::getLocale() . '/w-reg') }}',
			type:       'POST',
			data:       '_token={{ csrf_token() }}&name=' + name + '&' + 'phone=' + phone,
			beforeSend: function () {
			},
			success:    function (data) {
				if (data === 'true') {
					alert("Mail is send");
					window.location = '{{ url(Lang::getLocale()) }}';
				}
				else {
					alert("Mail is not send");
					window.location = '{{ url(Lang::getLocale()) }}';
				}
			}
		});
	}
}
;
$(document).ready(function () {

});
</script>
@stop


@section('header')
@include('home.inc.header')
@stop


@section('content')
<h1>@lang('w-reg.Instruction_for_our')</h1>
<form id="myform" method="GET" action="{{ url(Lang::getLocale() . '/w-reg') }}" onSubmit="return checkedForm(this)">
	<div>
		<label id="labelphone" for="telephone">@lang('w-reg.Your_telephone')</label>
		<br />
		<input name="phone" id="telephone" placeholder="+38 (___)" value="">
		<br />
	</div>
	<div>
		<label id="labelname" for="name">@lang('w-reg.Your_name')</label>
		<br />
		<input name="name" id="name" placeholder="@lang('w-reg.name_Alina')" value="">
	</div>
</form>
<h3>@lang('w-reg.Instructions_step_per')</h3>
<p>@lang('w-reg.Lorem_ipsum')</p>
<p>@lang('w-reg.Lorem_ipsum2')</p>
<a href="#" onclick="mail(); return false;">
	<div>@lang('w-reg.Lorem_Send')</div>
</a>
@stop


@section('footer')
@include('home.inc.footer')
@stop


@section('additional_data')
@include('home.inc.login-popup')
@stop
