@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/users-online-list.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')

	<div class="profile">
		<div class="h1">
			{{ ($userSex == 'm') ? trans('cabinet.users_lady_all_ladies') : trans('cabinet.users_lady_all_ladies') }}
		</div>
		@include('cabinet.inc.timer')
		<div class="main">
			@foreach ($onlineUsersList as $user)
				<div class="inbox">
					<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $user->id) }}">
						<div class="avatar" style="background-image: url('{{ empty($user->main_photo) ? asset('img/no_image_' . $user->sex . '.png') : asset($user->main_photo) }}');">
							{{--<div class="online">
								<p>@lang('cabinet.users_online_online')</p>
							</div>--}}
						</div>
					</a>
					<div class="info">
						<div class="name">
							<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $user->id) }}">
								<div>
									<h3>{{ $user->name }}</h3>
								</div>
							</a>
							<div class="id">
								<h2>@lang('cabinet.users_online_partner_id'):</h2>
							</div>
							<div class="id_number">
								<h2>{{ str_pad($user->id, 5, "0", STR_PAD_LEFT) }}</h2>
							</div>
							<div class="age">
								@if ($user->age)
									@lang('cabinet.users_online_partner_age'): {{ $user->age }}
								@endif
							</div>
							<div class="mail">
								{{ str_limit($user->about_me, $limit = 150, $end = '...') }}
							</div>
						</div>
						<div class="bookmarkers">
							<ul>
								<li>
									<a title="@lang('cabinet.users_online_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $user->id) }}">
										<img src="{{ asset('img/letter-purple.png') }}" alt="@lang('cabinet.users_online_write_letter')">
									</a>
								</li>
								@if ($user->sex == 'w')
									<li>
										<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $user->id) }}">
											<img src="{{ asset('img/gift-purple.png') }}"></a>
									</li>
									<li>
										<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $user->id) }}">
											<img src="{{ asset('img/diamant-purple.png') }}"></a>
									</li>
								@endif
								<li>
									<a title="@lang('cabinet.users_online_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $user->id) }}">
										<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.users_online_start_chat')">
									</a>
								</li>
								<li>
									<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $user->id) }}">
										<img src="{{ asset('img/hearth-purple.png') }}">
									</a>
								</li>
								@if ($user->sex == 'w')
									<li>
										<a title="@lang('cabinet.users_online_bay_fav')" href="{{ url(Lang::getLocale() . '/cabinet/bay-fav/' . $user->name) }}">
											<img src="{{ asset('img/cart_btn.png') }}"></a>
									</li>
								@endif
							</ul>
						</div>
					</div>
				</div>
				<hr>
			@endforeach
		</div>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
