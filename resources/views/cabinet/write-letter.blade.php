@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/write-letter.css') }}" rel="stylesheet" />
	<style type="text/css">
	</style>
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			var letterTextArea = $('textarea[name="msg"]');

			autosize(letterTextArea);

			letterTextArea.on('keyup change', function (e) {
				var el = $(this);
				// fix user data
				if (el.val().match(/^[\r\n]/g))
					el.val(el.val().replace(/^[\r\n]/g, ""));
				if (el.val().match(/[\r\n]{3,}/g))
					el.val(el.val().replace(/[\r\n]{3,}/g, "\r\n\r\n"));
				if (el.val().length > 1000)
					el.val(el.val().substr(0, 1000));

				autosize.update(letterTextArea);
				$('.msg-size span').html(letterTextArea.val().length);
			});

			$('#msgForm').find('button').on('click', function (e) {
				e.preventDefault();
				$(this).attr("disabled", "disabled").css("opacity", "0.8");
				$('#msgForm').submit();
			});

			var profileEditForm = $("#profileEditForm");
			var mainPhotoBlock = $('#mainPhotoBlock');
			var photosBlock = $('#photosBlock');

			{{-- проверка и предпоказ дополнительной картинки --}}
			mainPhotoBlock.on("change", '.online input', function () {
				{{-- предпоказ --}}
				var input = $(this)[0];
				var onlineBlock = $(this).closest('.online');
				if (input.files && input.files[0])
					if (input.files[0].type.match('image/*')) {
						var reader = new FileReader();
						reader.onload = function (e) {
							mainPhotoBlock.find('p').hide();
							mainPhotoBlock.find('a img').fadeIn();
							onlineBlock.css('background-image', 'url(' + e.target.result + ')');
							onlineBlock.css('background-size', 'contain');
						};
						reader.readAsDataURL(input.files[0]);
						{{-- все ок --}}
						return;
					}
			});

			mainPhotoBlock.on("click", '.online', function (e) {
				{{-- открываем диалог для загрузки нового изображения --}}
				mainPhotoBlock.find('.online input').trigger('click');
			});

			{{-- остановить всплытие событий --}}
			mainPhotoBlock.on('click', '.online input', function (e) {
				e.stopPropagation();
			});

			{{-- удаление фоток --}}
			mainPhotoBlock.on('click', '.online a', function (e) {
				e.preventDefault();
				e.stopPropagation();
				mainPhotoBlock.find('.online input').val('');
				{{-- скрываем предпоказ --}}
				mainPhotoBlock.find('p').fadeIn(1000);
				mainPhotoBlock.find('a img').hide();
				mainPhotoBlock.find('.online').css('background-image', '');
				mainPhotoBlock.find('.online').css('background-size', '');
			});
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')
	<div class="main">
		<div class="h1">
			@lang('cabinet.write_letter_title')
		</div>
		@include('cabinet.inc.timer')
		<div class="inbox">
			<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_info->id) }}">
				<div class="avatar" style="background-image: url('{{ empty($partner_info->main_photo) ? asset('img/no_image_' . $partner_info->sex . '.png') : asset($partner_info->main_photo) }}');">
					@if ($partner_info->online)
						<div class="online">
							<p>@lang('cabinet.write_letter_online')</p>
						</div>
					@endif
				</div>
			</a>
			<div class="info">
				<div class="name">
					<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_info->id) }}">
						<div>
							<h3>{{ $partner_info->name }}</h3>
						</div>
					</a>
					<div class="id">
						<h2>@lang('cabinet.write_letter_partner_id'):</h2>
					</div>
					<div class="id_number">
						<h2>{{ str_pad($partner_info->id, 5, "0", STR_PAD_LEFT) }}</h2>
					</div>
					<div class="age">
						@if ($partner_info->age)
							@lang('cabinet.write_letter_partner_age'): {{ $partner_info->age }}
						@endif
					</div>
					<div class="mail">{{ $partner_info->about_me }}</div>
				</div>
				<div class="bookmarkers">
					<ul>
						@if ($partner_info->sex == 'w')
							<li>
								<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $partner_id) }}">
									<img src="{{ asset('img/gift-purple.png') }}"></a>
							</li>
							<li>
								<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $partner_id) }}">
									<img src="{{ asset('img/diamant-purple.png') }}"></a>
							</li>
						@endif
						<li>
							<a title="@lang('cabinet.write_letter_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $partner_id) }}">
								<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.write_letter_start_chat')">
							</a>
						</li>
						<li>
							<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $partner_id) }}">
								<img src="{{ asset('img/hearth-purple.png') }}"></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="input">
			<div>
				<p>@lang('cabinet.write_letter_compose_letter')</p>
			</div>
			@if (count($errors->letter) > 0)
				<ul class="letter-errors">
					@foreach ($errors->letter->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif
			<form id="msgForm" action="{{ url(Lang::getLocale() . '/mail/send') }}" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="partner_id" value="{{ $partner_id }}">
				<textarea name="msg" placeholder="@lang('cabinet.write_letter_textarea_placeholder')">{{ old('msg') }}</textarea>
				<div id="mainPhotoBlock">
					<div class="online online2 online2-plus">
						<a href="#">
							<img class="display-none" src="{{ asset('img/close.png') }}" width="11px" height="11px">
						</a>
						<p>Click to attach image</p>
						<input type="file" name="image" accept="image/jpeg,image/png,image/gif" style="display:none;">
					</div>
				</div>
				<div class="msg-size">
					<span>0</span>
					/ 1000
				</div>
				<button type="submit">@lang('cabinet.write_letter_send_letter')</button>
			</form>
		</div>
	</div>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
