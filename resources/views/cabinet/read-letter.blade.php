@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/read-letter.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			autosize($('textarea'));
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')
	<div class="main">
		<div class="h1">
			@if ($user_id == $msg->user_id)
				@lang('cabinet.read_letter_your_sent_message')
			@else
				@lang('cabinet.read_letter_received_message')
			@endif
			@if ($msg->delete == 1)
				@lang('cabinet.read_letter_deleted_letter')
			@endif
		</div>
		@include('cabinet.inc.timer')
		<div class="inbox">
			<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $msg->user_id) }}">
				<div class="avatar" style="background-image: url('{{ empty($msg->partner_photo) ? asset('img/no_image_' . $msg->partner_sex . '.png') : asset($msg->partner_photo) }}');">
					@if ($msg->partner_online)
						<div class="online">
							<p>@lang('cabinet.read_letter_online')</p>
						</div>
					@endif
				</div>
			</a>
			<div class="info">
				<div class="name">
					<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $msg->user_id) }}">
						<div>
							<h3>{{ $msg->partner_name }}</h3>
						</div>
					</a>
					<div class="id">
						<h2>@lang('cabinet.read_letter_partner_id'):</h2>
					</div>
					<div class="id_number">
						<h2>{{ str_pad($msg->user_partner_id, 5, "0", STR_PAD_LEFT) }}</h2>
					</div>
					<div class="age">
						@if ($msg->partner_age)
							@lang('cabinet.read_letter_partner_age'): {{ $msg->partner_age }}
						@endif
					</div>
					<div class="mail">{{ $msg->partner_about_me }}</div>
				</div>
				<form action="{{ url(Lang::getLocale() . '/mail/' . (($msg->delete == 1) ? 'letter-erase' : 'letter-delete')) }}" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="msg_id" value="{{ $msg->id }}">
					<button type="submit" class="delete_button">{{ ($msg->delete == 1) ? trans('cabinet.read_letter_erase_letter') : trans('cabinet.read_letter_delete_letter') }}</button>
				</form>
				<div class="bookmarkers">
					<ul>
						<li>
							<a title="@lang('cabinet.read_letter_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $msg->user_partner_id) }}">
								<img src="{{ asset('img/letter-purple.png') }}" alt="@lang('cabinet.read_letter_write_letter')">
							</a>
						</li>
						@if ($msg->partner_sex == 'w')
							<li>
								<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $msg->partner_id) }}">
									<img src="{{ asset('img/gift-purple.png') }}"></a>
							</li>
							<li>
								<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $msg->partner_id) }}">
									<img src="{{ asset('img/diamant-purple.png') }}"></a>
							</li>
						@endif
						<li>
							<a title="@lang('cabinet.read_letter_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $msg->user_partner_id) }}">
								<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.read_letter_start_chat')">
							</a>
						</li>
						<li>
							<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $msg->user_partner_id) }}">
								<img src="{{ asset('img/hearth-purple.png') }}"></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		@if(!empty($msg->msg))
			<div class="input">
				<div>
					<p>@lang('cabinet.read_letter_message_title'):</p>
				</div>
				<div class="massage">
					{{ $msg->msg }}
				</div>
				@if(isset($image) && !empty($image))
					<div><img src="{{ asset('upload/mailfotos/'.$image) }}" class="image-massage"></div>
				@endif
				<a href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $msg->user_partner_id) }}" class="button">@lang('cabinet.read_letter_write_letter')</a>
			</div>
		@endif
		@if(!empty($Gifts))
			<p class="gifts-topic">Your gifts:</p>
			@foreach ($Gifts as $Gift)
				<div class="gift"><img src="{{ asset('img/vm/'.$Gift->image) }}"></div>
			@endforeach
		@endif
		@if(empty($msg->msg))
			<div class="clear"></div>
			<a href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $msg->user_partner_id) }}" class="button-without-msg">@lang('cabinet.read_letter_write_letter')</a>
		@endif
	</div>
@stop



@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
