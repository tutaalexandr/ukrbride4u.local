@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/chat.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			@if ($partner_id)
			var chatMsgsBlock = $('.chat_window');
			var chatScroller = $('#scroller');
			var chatSendMsgForm = $('#chatSendMsgForm');
			var chatSendMsgButton = $('#chatSendMsgButton');
			var chatMyMsg = $('#chatMyMsg');
			var smButton = $('.sm-button');
			var smArea = $('.sm-area');

			autosize(chatMyMsg);

			chatScroller.perfectScrollbar();

			function updateTextArea() {
				autosize.update(chatMyMsg);
				$('.msg-size span').html(chatMyMsg.val().length);
			}

			var canSendMsg = true;

			function submitForm() {
				$.ajax({
					async:      true,
					url:        '{{ url(Lang::getLocale() . '/chat/add') }}',
					type:       'POST',
					data:       chatSendMsgForm.serialize(),
					beforeSend: function () {
						chatSendMsgButton.attr("disabled", "disabled").css("opacity", "0.8");
						canSendMsg = false;
					},
					success:    function () {
					},
					error:      function (request, status, error) {
						if (request.status === 401)
							window.location = '{{ url(Lang::getLocale()) }}';
					},
					complete:   function () {
						chatSendMsgButton.removeAttr("disabled").css("opacity", "1");
						canSendMsg = true;
						chatMyMsg.focus();
					},
				});
			}

			chatSendMsgForm.bind('submit', function (e) {
				e.preventDefault();
				if (!canSendMsg)
					return;
				if ($.trim(chatMyMsg.val()).length === 0)
					return;
				submitForm();
				chatMyMsg.val("");
				smArea.fadeOut();
				updateTextArea();
			});

			chatMyMsg.keydown(function (e) {
				if (e.ctrlKey && e.keyCode === 13) { // ctrl + enter
					chatMyMsg.insertAtCaret("\n");
					scrollElToBottom(chatScroller);
				}
				else if (!e.ctrlKey && e.keyCode === 13) { // enter
					e.preventDefault();
					if (!canSendMsg)
						return;
					if ($.trim(chatMyMsg.val()).length === 0)
						return;
					submitForm();
					chatMyMsg.val("");
					smArea.fadeOut();
					updateTextArea();
				}
			});

			chatMyMsg.on('keyup change', function (e) {
				var el = $(this);
				// fix user data
				if (el.val().match(/^[\r\n]/g))
					el.val(el.val().replace(/^[\r\n]/g, ""));
				if (el.val().match(/[\r\n]{3,}/g))
					el.val(el.val().replace(/[\r\n]{3,}/g, "\r\n\r\n"));
				if (el.val().length > 1000)
					el.val(el.val().substr(0, 1000));
				updateTextArea();
			});

			$('.sm-area img').bind('click', function (e) {
				e.preventDefault();
				chatMyMsg.insertAtCaret(' ' + $(this).attr('c') + ' ').trigger('change');
				chatMyMsg.focus();
			});

			smButton.bind('click', function (e) {
				e.preventDefault();

				smArea.is(':visible') ? smArea.fadeOut() : smArea.fadeIn();
			});

			chatMyMsg.bind('click', function (e) {
				e.preventDefault();

				if (smArea.is(':visible'))
					smArea.fadeOut();
			});
			@endif

			setInterval(function () {
				$.ajax({
					async:   true,
					url:     '{{ url(Lang::getLocale() . '/cabinet/chat-recommended') }}',
					type:    'POST',
					data:    '_token={{ csrf_token() }}&partner_id={{ $partner_id }}',
					success: function (data) {
						$('div.right_menu').fadeOut(1000, function () {
							$('div.right_menu').html(data).tooltip().fadeIn(1000);
						});
					},
					error:   function (request, status, error) {
						if (request.status === 401)
							window.location = '{{ url(Lang::getLocale()) }}';
					}
				});
			}, 60000);

			scrollWindowTo(190);
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	<div class="chat">
		<div class="header_chat">
			<a id="turn" href="{{ url(Lang::getLocale() . $chat_previous_url) }}"></a>
			<a id="close" href="{{ url(Lang::getLocale() . '/cabinet/close-chat') }}"></a>
		</div>
		<div class="sidebar">
			<input type="hidden" value="{{ $chatRoomsKeyList }}" name="crkl" />
			<div class="chat-rooms-sidebar">
				@include('cabinet.inc.chat-rooms')
			</div>
			<div class="histoty">
				<div class="clock"></div>
				<div class="history_text">
					<a href="{{ url(Lang::getLocale() . '/cabinet/chat-history') }}">@lang('cabinet.chat_history')</a>
				</div>
			</div>
		</div>
		<div class="right_menu">
			@include('cabinet.inc.chat-recommended')
		</div>
		<div class="main_window">
			@if ($partner_id)
				<div class="header_window">
					<div class="info">
						<a class="name_girl" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_id) }}">
							<div class="big_avatar" style="background-image: url('{{ empty($partner_info->main_photo) ? asset('img/no_image_' . $partner_info->sex . '.png') : asset($partner_info->main_photo) }}');"></div>
						</a>
						<div class="contat_info">
							<a class="name_girl" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_id) }}">
								{{ $partner_info->name }}
							</a>
							{!! "<span class=\"chat-partner-status\" pid=\"$partner_id\">" . ($partner_info->online ? trans('cabinet.chat_online') : trans('cabinet.chat_offline')) . '</span>' !!}
							<br />
							@if ($partner_info->age)
								<span class="age">@lang('cabinet.chat_partner_age'): <span>{{ $partner_info->age }}</span></span>
								<br />
							@endif
							<span>{{ $partner_info->address }}</span>
							<br />
							<a class="stop-chat-button{{ $needStop ? '' : ' display-none' }}" href="{{ url(Lang::getLocale() . '/cabinet/stop-chat/' . $partner_id) }}">@lang('cabinet.chat_stop')</a>
						</div>
					</div>
					<div class="gifts" style="{{ $partner_info->sex == 'w' ? '' : 'width: auto;' }}">
						<div class="send_center">
							<a title="@lang('cabinet.read_letter_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $partner_id) }}">
								<img src="{{ asset('img/letter-purple.png') }}" alt="@lang('cabinet.read_letter_write_letter')">
							</a>
						</div>
						@if ($partner_info->sex == 'w')
							<div class="send_center">
								<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $partner_id) }}">
									<img src="{{ asset('img/gift-purple.png') }}"></a>
							</div>
							<div class="send_center">
								<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $partner_id) }}">
									<img src="{{ asset('img/diamant-purple.png') }}"></a>
							</div>
						@endif
						<div class="send_center">
							<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $partner_id) }}">
								<img src="{{ asset('img/hearth-purple.png') }}">
							</a>
						</div>
					</div>
				</div>
				<div class='scroller' id='scroller'>
					<div class='chat_window'>
					</div>
				</div>
				<div class="textarea textarea-send-msg-form{{ $needStart ? ' display-none' : '' }}">
					<div class="sm-area">
						@foreach($smiles_list AS $key => $smile)
							<img c="{{ $key }}" src="{{ asset('img/sm/ls/' . $smile) }}" />
						@endforeach
					</div>
					<a href="#" class="sm-button"></a>
					<form id="chatSendMsgForm">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="partner_id" value="{{ $partner_id }}">
						<div id="chatMyMsgBlock">
							<textarea id="chatMyMsg" name="chatMyMsg" wrap="soft" placeholder="@lang('cabinet.chat_placeholder_msg')"></textarea>
						</div>
						<button type="submit" id="chatSendMsgButton">@lang('cabinet.chat_send')</button>
					</form>
					<div class="msg-size">
						<span>0</span>
						/ 1000
					</div>
				</div>
				<div class="textarea textarea-start-buttons{{ $needStart ? '' : ' display-none' }}">
					<a class="start-chat-button{{ $canStartChat ? '' : ' display-none' }}" href="{{ url(Lang::getLocale() . '/cabinet/start-chat/' . $partner_id) }}">@lang('cabinet.chat_start')</a>
					<span title="User offline or not enough money in the account to start a new chat" class="start-chat-button-disabled{{ $canStartChat ? ' display-none' : '' }}" href="{{ url(Lang::getLocale() . '/cabinet/start-chat/' . $partner_id) }}">@lang('cabinet.chat_start')</span>
				</div>
			@else
				<div class="select-partner-text">
					<<- @lang('cabinet.chat_select_partner_msg') ->>
				</div>
			@endif
		</div>

	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
