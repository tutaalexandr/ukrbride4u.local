@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/profile-edit.css') }}" rel="stylesheet" />
	<style type="text/css">
		.online input {
			visibility: hidden;
			display: none;
		}

		.online-plus {
			display: none;
		}
	</style>
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			var profileEditForm = $("#profileEditForm");
			var mainPhotoBlock = $('#mainPhotoBlock');
			var photosBlock = $('#photosBlock');

			{{---------------------------------------------------------------------------------}}

			$('input[name="bdate"]').datepicker({
				changeMonth: true,
				changeYear:  true,
				yearRange:   '-100:-16',
				dateFormat:  "yy-mm-dd",
				defaultDate: "-16y",
			});

			if (photosBlock.find('.online').length < {{ Config::get('app.profile_photos_count') }})
				$('.online-plus').fadeIn();

			$('input[name="address"]').autocomplete({
				source:    '{{ url(Lang::getLocale() . '/cabinet/autocomplete-address') }}',
				minLength: 2
			});

			{{---------------------------------------------------------------------------------}}

			{{-- проверка и предпоказ дополнительной картинки --}}
			mainPhotoBlock.on("change", '.online input', function () {
				{{-- помечаем картинку на удаление --}}
				$('input[name="main_photo_del"]').val(1);

				{{-- предпоказ --}}
				var input = $(this)[0];
				var onlineBlock = $(this).closest('.online');
				if (input.files && input.files[0])
					if (input.files[0].type.match('image/*')) {
						var reader = new FileReader();
						reader.onload = function (e) {
							onlineBlock.css('background-image', 'url(' + e.target.result + ')');
						};
						reader.readAsDataURL(input.files[0]);
						{{-- все ок --}}
						return;
					}
				{{-- если это не изображение --}}
				onlineBlock.remove();
			});

			mainPhotoBlock.on("click", '.online', function (e) {
				{{-- открываем диалог для загрузки нового изображения --}}
				mainPhotoBlock.find('.online input').trigger('click');
			});

			{{-- остановить всплытие событий --}}
			mainPhotoBlock.on('click', '.online input', function (e) {
				e.stopPropagation();
			});

			{{-- удаление фоток --}}
			mainPhotoBlock.on('click', '.online a', function (e) {
				e.preventDefault();
				e.stopPropagation();

				{{-- помечаем картинку на удаление --}}
				$('input[name="main_photo_del"]').val(1);

				{{-- скрываем предпоказ --}}
				mainPhotoBlock.find('.online').css('background-image', '');
			});

			{{---------------------------------------------------------------------------------}}

			$('.online-plus').bind('click', function () {
				var photoBlocksCnt = photosBlock.find('.online').length;

				{{-- проверка кол-ва фоток --}}
				if (photoBlocksCnt + 1 === {{ Config::get('app.profile_photos_count') }})
					$('.online-plus').hide();
				if (photoBlocksCnt + 1 > {{ Config::get('app.profile_photos_count') }})
					return;

				var i = parseInt(photoBlocksCnt < 1 ? 0 : photosBlock.find('.online:last-child').attr('i')) + 1;

				var html = '';
				html += '<div class="online" i="' + i + '">';
				html += '   <a href="#">';
				html += '       <img src="{{ asset('img/close.png') }}" width="11px" height="11px">';
				html += '   </a>';
				html += '   <input type="file" name="photo[' + i + ']" accept="image/jpeg,image/png,image/gif">';
				html += '</div>';

				photosBlock.append(html);

				$('input[name="photo[' + i + ']"]').trigger('click');
			});

			{{-- проверка и предпоказ дополнительной картинки --}}
			photosBlock.on("change", '.online input', function () {
				{{-- отмечаем старую фотку на удаление если она есть --}}
				if ($(this).parent().attr('pid')) {
					var val = $('input[name="photos_to_del"]').val();
					$('input[name="photos_to_del"]').val(val + (val.length > 0 ? ',' : '') + $(this).parent().attr('pid'));
					$(this).parent().removeAttr('pid');
				}

				{{-- предпоказ --}}
				var input = $(this)[0];
				var onlineBlock = $(this).closest('.online');
				if (input.files && input.files[0])
					if (input.files[0].type.match('image/*')) {
						var reader = new FileReader();
						reader.onload = function (e) {
							onlineBlock.css('background-image', 'url(' + e.target.result + ')');
						};
						reader.readAsDataURL(input.files[0]);
						{{-- все ок --}}
						return;
					}
				{{-- если это не изображение --}}
				onlineBlock.remove();
			});

			photosBlock.on("click", '.online', function (e) {
				{{-- открываем диалог для загрузки нового изображения --}}
				$('input[name="photo[' + $(this).attr('i') + ']"]').trigger('click');
			});

			{{-- остановить всплытие событий скрытых полей для фоток --}}
			photosBlock.on('click', 'input[name^="photo"]', function (e) {
				e.stopPropagation();
			});

			{{-- удаление фоток --}}
			photosBlock.on('click', '.online a', function (e) {
				e.preventDefault();
				e.stopPropagation();

				if ($(this).parent().attr('pid')) {
					var val = $('input[name="photos_to_del"]').val();
					$('input[name="photos_to_del"]').val(val + (val.length > 0 ? ',' : '') + $(this).parent().attr('pid'));
				}

				$(this).parent().fadeOut(function () {
					$(this).remove();
					if (photosBlock.find('.online').length <= {{ Config::get('app.profile_photos_count') }})
						$('.online-plus').fadeIn();
				});
			});
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')
	<div class="profile">
		<div class="h1">
			@lang('cabinet.profile_title')
		</div>
		@include('cabinet.inc.timer')
		@if (count($errors) >0)
			<div class="errors">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form id="profileEditForm" action="{{ url(Lang::getLocale() . '/auth/edit-user') }}" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="input">
				<div class="center_input center_input_no_padding">
					<label for="name">@lang('cabinet.profile_name')</label>
					<br />
					<input type="text" id="name" name="name" value="{{ old('name') == NULL ? $user['name'] : old('name') }}">
				</div>
				<div class="left_input">
					<label for="bdate">@lang('cabinet.profile_birthday')</label>
					<br />
					<input type="text" id="bdate" name="bdate" value="{{ old('bdate') == NULL ? $user['bdate'] : old('bdate') }}">
				</div>
				<div class="right_input">
					<label for="phone">@lang('cabinet.profile_phone')</label>
					<br />
					<input type="text" id="phone" name="phone" value="{{ old('phone') == NULL ? $user['phone'] : old('phone') }}">
				</div>
				<div class="center_input">
					<label for="address">@lang('cabinet.profile_address')</label>
					<br />
					<input type="text" id="address" name="address" value="{{ old('address') == NULL ? $user['address'] : old('address') }}">
				</div>
				<div class="left_input">
					<label for="enLevel">@lang('cabinet.profile_english')</label>
					<br />
					<div class="new-select-style-wpandyou">
						<?php $en_level = old('en_level') == NULL ? $user['en_level'] : old('en_level'); ?>
						<select id="enLevel" name="en_level">
							@if (empty($en_level))
								<option></option>
							@endif
							@foreach ($enLevelList AS $value)
								<option value="{{ $value }}"{{ ($en_level == $value) ? ' selected' : '' }}>@lang('cabinet.' . $value)</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="right_input">
					<label for="height">@lang('cabinet.profile_height')</label>
					<br />
					<input type="text" id="height" name="height" value="{{ old('height') == NULL ? $user['height'] : old('height') }}">
				</div>
				<div class="left_input">
					<label for="weight">@lang('cabinet.profile_weight')</label>
					<br />
					<input type="text" id="weight" name="weight" value="{{ old('weight') == NULL ? $user['weight'] : old('weight') }}">
				</div>
				<div class="right_input">
					<label for="hair">@lang('cabinet.profile_hair')</label>
					<br />
					<div class="new-select-style-wpandyou">
						<select id="hair" name="hair">
							<?php $hair = old('hair') == NULL ? $user['hair'] : old('hair'); ?>
							@if (empty($hair))
								<option></option>
							@endif
							@foreach ($hairList AS $value)
								<option value="{{ $value }}"{{ ($hair == $value) ? ' selected' : '' }}>@lang('cabinet.' . $value)</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="left_input">
					<label for="eyes">@lang('cabinet.profile_eye')</label>
					<br />
					<div class="new-select-style-wpandyou">
						<select id="eyes" name="eyes">
							<?php $eyes = old('eyes') == NULL ? $user['eyes'] : old('eyes'); ?>
							@if (empty($eyes))
								<option></option>
							@endif
							@foreach ($eyesList AS $value)
								<option value="{{ $value }}"{{ ($eyes == $value) ? ' selected' : '' }}>@lang('cabinet.' . $value)</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="right_input">
					<label for="zodiac">@lang('cabinet.profile_zodiac')</label>
					<br />
					<div class="new-select-style-wpandyou">
						<select id="zodiac" name="zodiac">
							<?php $zodiac = old('zodiac') == NULL ? $user['zodiac'] : old('zodiac'); ?>
							@if (empty($zodiac))
								<option></option>
							@endif
							@foreach ($zodiacList AS $value)
								<option value="{{ $value }}"{{ ($zodiac == $value) ? ' selected' : '' }}>@lang('cabinet.' . $value)</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="center_input">
					<label for="aboutMe">@lang('cabinet.profile_about_me')</label>
					<br />
					<textarea id="aboutMe" name="about_me">{{ old('about_me') == NULL ? $user['about_me'] : old('about_me') }}</textarea>
				</div>
			</div>
			<div class="update">
				<h3>@lang('cabinet.profile_your_photo')</h3>
				<div class="foto">
					<div id="mainPhotoBlock">
						<div class="online" style="background-image: url('{{ asset($user['main_photo']) }}');">
							<a href="#">
								<img src="{{ asset('img/close.png') }}" width="11px" height="11px">
							</a>
							<p>@lang('cabinet.profile_main_photo')</p>
							<input type="file" name="main_photo" accept="image/jpeg,image/png,image/gif">
							<input type="hidden" name="main_photo_del" value="0">
						</div>
					</div>

					<div id="photosBlock">
						@foreach ($photos AS $key => $photo)
							<div class="online" i="{{ $key }}" style="background-image: url('{{ asset($photo['img']) }}');" pid="{{ $photo['id'] }}">
								<a href="#">
									<img src="{{ asset('img/close.png') }}" width="11px" height="11px">
								</a>
								<input type="file" name="photo[{{ $key }}]" accept="image/jpeg,image/png,image/gif">
							</div>
						@endforeach
					</div>
					<div class="online online-plus"></div>

					<input type="hidden" name="photos_to_del">
				</div>

				<h3>@lang('cabinet.profile_youtube')</h3>
				<div class="center_input center_input_no_padding">
					<input id="youtube" name="youtube" value="{{ old('youtube') == NULL ? $user['youtube'] : old('youtube') }}">
				</div>
				<button class="save" type="submit">@lang('cabinet.profile_save')</button>
			</div>
		</form>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
