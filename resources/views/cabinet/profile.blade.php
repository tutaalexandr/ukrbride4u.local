@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/profile.css') }}" rel="stylesheet" />
	{{--<style type="text/css"></style>--}}
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')
	@include('cabinet.inc.timer')
	<div class="profile">
		<div class="card">
			<div class="name">{{ $userProfile->name }}</div>
			<div class="avatar" style="background-image: url('{{ empty($userProfile->main_photo) ? asset('img/no_image_' . $userProfile->sex . '.png') : asset($userProfile->main_photo) }}')">
			</div>
			<div class="table">
				<div>
					<div class="line_left">ID <span>{{ $userProfile->id }}</span></div>
					<div class="line_right">Eye colore<span>{{ $userProfile->eyes }}</span></div>
				</div>
				<div>
					<div class="line_left">Birth Date <span>{{ $userProfile->bdate }}</span></div>
					<div class="line_right">English Level<span>{{ $userProfile->en_level }}</span></div>
				</div>
				<div>
					<div class="line_left">Zodiac <span>{{ $userProfile->zodiac }}</span></div>
					<div class="line_right">City<span>{{ $userProfile->address }}</span></div>
				</div>
				<div>
					<div class="line_left">Height <span>{{ $userProfile->height }}</span></div>
					<div class="line_right">Weight <span>{{ $userProfile->weight }}</span></div>
				</div>
			</div>
			<div class="gifts">
				<a title="@lang('cabinet.users_online_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $userProfile->id) }}">
					<img src="{{ asset('img/letter-purple.png') }}"></a>
				@if ($userProfile->sex == 'w')
					<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $userProfile->id) }}">
						<img src="{{ asset('img/gift-purple.png') }}"></a>
					<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $userProfile->id) }}">
						<img src="{{ asset('img/diamant-purple.png') }}"></a>
				@endif
				<a title="@lang('cabinet.write_letter_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $userProfile->id) }}">
					<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.write_letter_start_chat')"></a>
				<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $userProfile->id) }}">
					<img src="{{ asset('img/hearth-purple.png') }}"></a>
				@if ($userProfile->sex == 'w')
					<a title="@lang('cabinet.users_online_bay_fav')" href="{{ url(Lang::getLocale() . '/cabinet/bay-fav/' . $userProfile->name) }}">
						<img src="{{ asset('img/cart_btn.png') }}"></a>
				@endif
			</div>
			<div class="about">
				<h3>About me</h3>
				<p>{{ $userProfile->about_me }}</p>
			</div>
			<div class="video">
				<iframe width="450" height="250" src='{{ str_replace("watch?v=", "embed/", $userProfile->youtube ) }}' frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="foto">
				@if($userFoto)
					@foreach ($userFoto AS $Foto)
						<img src="{{ asset( $Foto->img ) }}">
					@endforeach
				@endif
			</div>
		</div>
	</div>

@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
