@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/chat-history.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			@if ($partner_id)
				$('#scroller').perfectScrollbar();
			@endif

			setInterval(function () {
				$.ajax({
					async:   true,
					url:     '{{ url(Lang::getLocale() . '/cabinet/chat-recommended') }}',
					type:    'POST',
					data:    '_token={{ csrf_token() }}&partner_id={{ $partner_id }}',
					success: function (data) {
						$('div.right_menu').fadeOut(1000, function () {
							$('div.right_menu').html(data).tooltip().fadeIn(1000);
						});
					},
					error:   function (request, status, error) {
						if (request.status === 401)
							window.location = '{{ url(Lang::getLocale()) }}';
					}
				});
			}, 60000);

			scrollWindowTo(190);
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	<div class="chat">
		<div class="header_chat">
			<a id="turn" href="{{ url(Lang::getLocale() . $chat_previous_url) }}"></a>
			<a id="close" href="{{ url(Lang::getLocale() . '/cabinet/close-chat') }}"></a>
		</div>
		<div class="sidebar">
			<input type="hidden" value="{{ $chatHistoryRoomsKeyList }}" name="chrkl" />
			<div class="chat-history-rooms-sidebar">
				@include('cabinet.inc.chat-rooms')
			</div>
			<div class="histoty">
				<div class="clock"></div>
				<div class="history_text">
					<a href="{{ url(Lang::getLocale() . '/cabinet/chat') }}">@lang('cabinet.chat_history_chat')</a>
				</div>
			</div>
		</div>
		<div class="right_menu">
			@include('cabinet.inc.chat-recommended')
		</div>
		<div class="main_window">
			@if ($partner_id)
				<div class="header_window">
					<div class="info">
						<a class="name_girl" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_id) }}">
							<div class="big_avatar" style="background-image: url('{{ empty($partner_info->main_photo) ? asset('img/no_image_' . $partner_info->sex . '.png') : asset($partner_info->main_photo) }}');"></div>
						</a>
						<div class="contat_info">
							<a class="name_girl" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_id) }}">
								{{ $partner_info->name }}
							</a>
							{!! "<span class=\"chat-partner-status\" pid=\"$partner_id\">" . ($partner_info->online ? trans('cabinet.chat_online') : trans('cabinet.chat_offline')) . '</span>' !!}
							<br />
							@if ($partner_info->age)
								<span class="age">@lang('cabinet.chat_partner_age'): <span>{{ $partner_info->age }}</span></span>
								<br />
							@endif
							<span>{{ $partner_info->address }}</span>
						</div>
					</div>
					<div class="gifts" style="{{ $partner_info->sex == 'w' ? '' : 'width: auto;' }}">
						<div class="send_center">
							<a title="@lang('cabinet.read_letter_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $partner_id) }}">
								<img src="{{ asset('img/letter-purple.png') }}" alt="@lang('cabinet.read_letter_write_letter')">
							</a>
						</div>
						@if ($partner_info->sex == 'w')
							<div class="send_center">
								<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $partner_id) }}">
									<img src="{{ asset('img/gift-purple.png') }}"></a>
							</div>
							<div class="send_center">
								<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $partner_id) }}">
									<img src="{{ asset('img/diamant-purple.png') }}"></a>
							</div>
						@endif
						<div class="send_center">
							<a title="@lang('cabinet.users_online_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $partner_id) }}">
								<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.users_online_start_chat')">
							</a>
						</div>
						<div class="send_center">
							<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $partner_id) }}">
								<img src="{{ asset('img/hearth-purple.png') }}">
							</a>
						</div>
					</div>
				</div>
				<div class='scroller' id='scroller'>
					<div class='chat_window'></div>
				</div>
				<div class="textarea textarea-start-buttons">
					<a class="restore-chat-button" href="{{ url(Lang::getLocale() . '/cabinet/restore-chat/' . $partner_id) }}">@lang('cabinet.chat_restore')</a>
				</div>
			@else
				<div class="select-partner-text">
					<<- @lang('cabinet.chat_history_select_partner_msg')
				</div>
			@endif
		</div>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
