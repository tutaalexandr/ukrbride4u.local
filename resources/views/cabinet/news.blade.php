@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/account.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')

	<div class="profile">
		<div class="h1">
			{{--@lang('cabinet.account_title')--}}
		</div>
		@include('cabinet.inc.timer')
		@if (count($errors) > 0)
			<div class="errors">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		@if(!isset($actions))
			@if(!isset($stories))
			<h1>@lang('home.news_title')</h1>
			@else
			<h1>Storyes</h1>
			@endif
		@foreach($News as $New)
			<div class="news1">
				<h4>
					@if(!isset($stories))
						<a href="{{ url(Lang::getLocale() . '/cabinet/news/'.$New->url_name) }}" style="color: #80217e;">{{ $New->title }}</a>
					@else
						<a href="{{ url(Lang::getLocale() . '/cabinet/stories/'.$New->url_name) }}" style="color: #80217e;">{{ $New->title }}</a>
					@endif
				</h4>
				<p>{{ str_limit(strip_tags($New->text), 120)  }}</p>
			</div>
		@endforeach
		@else
			@foreach($News as $New)
				<div class="text">
					<h1>{{ $New->title }}</h1>
					<div class="first">{!! $New->text  !!}</div>
				</div>
			@endforeach
		@endif



	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
