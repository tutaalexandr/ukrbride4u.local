<div class="timer display-none">
	<input type="hidden" value="" id="timerStartTime" />
	<a href="{{ url(Lang::getLocale() . '/cabinet/chat') }}" title="@lang("cabinet.menu_live_chat")">
		<div class="timer-clock">
			<p class="timer-value"></p>
		</div>
	</a>
</div>
