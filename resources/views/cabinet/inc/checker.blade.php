<script type="text/javascript">
	$(document).ready(function () {
		function startTimer(startDateStr) {
			var timerEl = $('.timer-value');
			var passed = 0, h, m, s, res;

			function tick() {
				passed = Math.floor((new Date() - new Date(startDateStr)) / 1000);
				h = Math.floor(passed / 3600);
				passed -= h * 3600;
				m = Math.floor(passed / 60);
				passed -= m * 60;
				s = passed;

				res = $.strPadLeft(h, 2) + ':' + $.strPadLeft(m, 2) + ':' + $.strPadLeft(s, 2);
				if (timerEl.html().length == 0)
					timerEl.hide().html(res).fadeIn();
				else
					timerEl.show().html(res);
			}

			tick();

			clearInterval(timerEl.data('timer'));
			timerEl.data('timer', setInterval(tick, 1000));
		}

		function stopTimer() {
			var timerEl = $('.timer-value');
			clearInterval(timerEl.data('timer'));
			timerEl.html('');
		}

		function checker() {
			var data = {_token: '{{ csrf_token() }}', to: (new Date()).getTimezoneOffset()};

			{{-- timer --}}
			if ($('.timer').length)
				data.tst = $('#timerStartTime').val();

			{{-- get partner chat messages --}}
			if ($('.chat_window').length) {
				data.gpcm = 1;
				data.pid = $('input[name="partner_id"]').val();
				data.lmid = $('div[id^="chat-msg-"]:last').attr('mid');
			}

			{{-- get mails count --}}
			if ($('.menu-mails-cnt').length)
				data.mc = parseInt($('.menu-mails-cnt').text());

			{{-- get chat messages count --}}
			if ($('.menu-chat-msgs-cnt').length)
				data.cmc = parseInt($('.menu-chat-msgs-cnt').text());
            {{-- get chat messages count --}}
			if ($('.menu-chat-msgs-cnt-news').length)
				data.cmcnews = parseInt($('.menu-chat-msgs-cnt-news').text());

			{{-- get chat partner status --}}
			if ($('.chat-partner-status').length) {
				data.cps = parseInt($('.chat-partner-status').text() === '@lang('cabinet.chat_online')' ? 1 : 0);
				data.pid = $('.chat-partner-status').attr('pid');
			}

			{{-- get chat rooms updates --}}
			if ($('input[name="crkl"]').length)
				data.crkl = $('input[name="crkl"]').val();

			{{-- get chat history rooms updates --}}
			if ($('input[name="chrkl"]').length)
				data.chrkl = $('input[name="chrkl"]').val();

			{{-- get chat buttons update --}}
			if ($('.stop-chat-button').length)
				data.cria = $('.stop-chat-button').is(':visible') ? 1 : 0;

			$.ajax({
				timeout:  60 * 1000 * 5,
				async:    true,
				url:      '{{ url(Lang::getLocale() . '/cabinet/checker') }}',
				type:     'POST',
				data:     data,
				dataType: 'json',
				success:  function (res) {
					{{-- set timer time --}}
					if (res.facrt != undefined) {
						$('#timerStartTime').val(res.facrt);
						if (res.facrt == '') {
							$('.timer').fadeOut(function () {
								stopTimer();
							});
						}
						else {
							$('.timer').fadeOut(function () {
								startTimer(res.facrt);
								$('.timer').fadeIn();
							});
						}
					}

					{{-- set partner chat messages --}}
					if (res.pcms) {
						$('.chat_window').append(res.pcms);
						scrollElToBottom($('#scroller'));
						$('#scroller').perfectScrollbar('update');
					}

					{{-- set new mails count --}}
					if (res.nmc != undefined) {
						if (res.nmc == 0)
							$('.menu-mails-cnt').html(res.nmc).fadeOut();
						else
							$('.menu-mails-cnt').html(res.nmc).fadeIn();
					}
					if (res.ncmcnews != undefined) {
						if (res.ncmcnews == 0)
							$('.menu-chat-msgs-cnt-news').html(res.ncmcnews).fadeOut();
						else
							$('.menu-chat-msgs-cnt-news').html(res.ncmcnews).fadeIn();

                        if( parseInt($('.menu-chat-msgs-cnt-news').text()) == 1)
                            $('.menu-chat-msgs-cnt-news').hide();
					}
					{{-- set new chat messages count --}}
					if (res.ncmc != undefined) {
						if (res.ncmc == 0)
							$('.menu-chat-msgs-cnt').html(res.ncmc).fadeOut();
						else
							$('.menu-chat-msgs-cnt').html(res.ncmc).fadeIn();
					}


					{{-- set new chat partner status --}}
					if (res.ncps != undefined) {
						if (res.ncps == 0) {
							$('.chat-partner-status').html('@lang('cabinet.chat_offline')');
							$('.start-chat-button').hide();
							$('.start-chat-button-disabled').show();
						}
						else {
							$('.chat-partner-status').html('@lang('cabinet.chat_online')');
							$('.start-chat-button-disabled').hide();
							$('.start-chat-button').show();
						}
					}

					{{-- set new rooms key list --}}
					if (res.nrkl != undefined) {
						$('input[name="crkl"]').val(res.nrkl);
						$.ajax({
							async:   true,
							url:     '{{ url(Lang::getLocale() . '/cabinet/rooms-list') }}',
							type:    'POST',
							data:    '_token={{ csrf_token() }}',
							success: function (data) {
								$('.chat-rooms-sidebar').fadeOut(500, function () {
									$('.chat-rooms-sidebar').html(data).tooltip().fadeIn(500);
								});
							},
							error:   function (request, status, error) {
								if (request.status === 401)
									window.location = '{{ url(Lang::getLocale()) }}';
							}
						});
					}

					{{-- set new history rooms key list --}}
					if (res.nhrkl != undefined) {
						$('input[name="chrkl"]').val(res.nhrkl);
						$.ajax({
							async:   true,
							url:     '{{ url(Lang::getLocale() . '/cabinet/history-rooms-list') }}',
							type:    'POST',
							data:    '_token={{ csrf_token() }}',
							success: function (data) {
								$('.chat-history-rooms-sidebar').fadeOut(500, function () {
									$('.chat-history-rooms-sidebar').html(data).tooltip().fadeIn(500);
								});
							},
							error:   function (request, status, error) {
								if (request.status === 401)
									window.location = '{{ url(Lang::getLocale()) }}';
							}
						});
					}

					{{-- new start chat button status --}}
					if (res.nscbs != undefined) {
						if (res.nscbs == 1) {
							$('.stop-chat-button').show();
							$('.textarea-send-msg-form').show();
							$('.textarea-start-buttons').hide();
						}
						else {
							$('.stop-chat-button').hide();
							$('.textarea-send-msg-form').hide();
							$('.textarea-start-buttons').show();
						}
					}
				},
				complete: function () {
					// setTimeout(checker , 4000);
				},
				error:    function (request, status, error) {
					if (request.status === 401 || request.status === 500)
						window.location = '{{ url(Lang::getLocale()) }}';
				}
			});
		}

		setInterval(checker , 3500) ;
	});
</script>
