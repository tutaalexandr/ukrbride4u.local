<div class="menu">
	<ul>
		<li class="main_line">
			@lang("cabinet.menu_mailing_system")
		</li>
		<li class="line_img">
			<a href="#"><img src="{{ asset('img/letter-blue.png') }}" alt="letter" width="22px" height="16px">
				<p class="menu-mails-cnt">0</p>
			</a>
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/inbox-list') }}">@lang("cabinet.menu_inbox")</a>
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/sent-list') }}">@lang("cabinet.menu_sent_mails")</a>
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/deleted-list') }}">@lang("cabinet.menu_deleted_mails")</a>
		</li>

		<li class="main_line">
			@if (Auth::getUser()->sex == 'm')
				@lang("cabinet.menu_ladies")
			@else
				@lang("cabinet.menu_mans")
			@endif
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/search') }}">@lang("cabinet.menu_search")</a>
		</li>
		<li class="line_img">
			<a href="#"><img src="{{ asset('img/hearth-blue.png') }}" alt="hearth" width="22px" height="20px"></a>
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/users-online-list') }}">@lang("cabinet.menu_my_favorite_list")</a>
		</li>
		<li class="extra_line last">
			<a href="{{ url(Lang::getLocale() . '/cabinet/users-online-list') }}">
				@if (Auth::getUser()->sex == 'm')
					@lang("cabinet.menu_lady_online")
				@else
					@lang("cabinet.menu_man_online")
				@endif
			</a>
		</li>
		@if (Auth::getUser()->sex == 'm')
			<li class="extra_line">
				<a href="{{ url(Lang::getLocale() . '/cabinet/users-all-list') }}">@lang("cabinet.menu_lady_all")</a>
			</li>
		@else

		@endif


		<li class="main_line">
			@lang("cabinet.menu_services")
		</li>
		<li class="line_img">
			<a href="#"><img src="{{ asset('img/mail-blue.png') }}" alt="mail" width="22px" height="16px">
				<p class="menu-chat-msgs-cnt">0</p>
			</a>
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/chat') }}">@lang("cabinet.menu_live_chat")</a>
		</li>
		<!-- 		@if (Auth::getUser()->sex == 'm')
		<li class="line_img">
			<a href="#">
				<img src="{{ asset('img/diamant-blue.png') }}" alt="mail" width="22px" height="16px">
			{{--<p>3</p>--}}
		</a>
	</li>
	<li class="extra_line">
		<a href="#">@lang("cabinet.menu_presents")</a>
	</li>
	<li class="line_img">
		<a href="#">
			<img src="{{ asset('img/gift-blue.png') }}" alt="mail" width="22px" height="16px">
			{{--<p>3</p>--}}
		</a>
	</li>
	<li class="extra_line last">
		<a href="#">@lang("cabinet.menu_virtual_gifts")</a>
	</li>
@endif -->

		<li class="main_line">
			@lang("cabinet.menu_my_account")
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/profile') }}">@lang("cabinet.menu_my_profile")</a>
		</li>
		@if (Auth::getUser()->sex == 'm')
			<li class="extra_line">
				<a href="{{ url(Lang::getLocale() . '/cabinet/payments') }}">@lang("cabinet.menu_payments")</a>
			</li>
		@endif
		<li class="extra_line last">
			<a href="{{ url(Lang::getLocale() . '/cabinet/account') }}">@lang("cabinet.menu_my_account_information")</a>
		</li>

		<li class="main_line">
			Company
			{{--@lang("cabinet.menu_my_account")--}}
		</li>
		<li class="extra_line">
			<a href="{{ url(Lang::getLocale() . '/cabinet/news') }}">News</a>
			{{@$tuta}}
			<p class="menu-chat-msgs-cnt-news">0</p>
		</li>
		<li class="extra_line last">
			<a href="{{ url(Lang::getLocale() . '/cabinet/stories') }}">Stories</a>
		</li>
	</ul>
</div>
