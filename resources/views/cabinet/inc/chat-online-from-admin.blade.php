@foreach($onlineUsersList as $user)
	<div class="girls" style="background-image: url('{{ empty($user->main_photo) ? asset('img/no_image_' . $user->sex . '.png') : asset($user->main_photo) }}');">
		<div class="name_right">
			<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $user->id) }}">
				<div class="names">
					{{ $user->name }}
				</div>
				<div class="old">
					{{ $user->age }}
				</div>
			</a>
		</div>
		<div class="form">
			<div class="send-right">
				<a title="@lang('cabinet.rec_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat-admin/' . $user->id.'?idledis='.$idledis) }}">
					<img src="{{ asset('img/letter-small.png') }}" alt="facebook">
				</a>
			</div>
		</div>
	</div>
@endforeach
