@foreach($onlineUsersList as $user)
	<div class="girls" style="background-image: url('{{ empty($user->main_photo) ? asset('img/no_image_' . $user->sex . '.png') : asset($user->main_photo) }}');">
		<div class="name_right">
			<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $user->id) }}">
				<div class="names">
					{{ $user->name }}
				</div>
				<div class="old">
					{{ $user->age }}
				</div>
			</a>
		</div>
		<div class="form">
			<div class="send-right">
				<a title="@lang('cabinet.delete_letters_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $user->id) }}">
					<img src="{{ asset('img/mail-small.png') }}" alt="@lang('cabinet.delete_letters_write_letter')">
				</a>
			</div>
			@if ($user->sex == 'w')
				<div class="send-right">
					<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $user->id) }}">
						<img src="{{ asset('img/gifts-icon.png') }}" alt="twitter" width="14px" height="15px"></a>
				</div>
				<div class="send-right">
					<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $user->id) }}">
						<img src="{{ asset('img/diamant-small.png') }}" alt="twitter"></a>
				</div>
			@endif
			<div class="send-right">
				<a title="@lang('cabinet.rec_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $user->id) }}">
					<img src="{{ asset('img/letter-small.png') }}" alt="facebook">
				</a>
			</div>
			<div class="send-right">
				<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $user->id) }}">
					<img src="{{ asset('img/hearth-small.png') }}">
				</a>
			</div>
		</div>
	</div>
@endforeach
