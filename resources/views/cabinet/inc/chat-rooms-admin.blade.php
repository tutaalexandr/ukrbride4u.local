@if (isset($chatRoomsList))
	@foreach($chatRoomsList as $chatRoom)
		<div class="status">
			<a class="status-link" href="{{ url(Lang::getLocale() . '/cabinet/chat-admin/' . $chatRoom->partner_id) }}?idledis={{$idledis}}">
				<div class="{{ $chatRoom->active == 1 ? 'online' : 'offline' }}"></div>
				<div class="small_avatar" style="background-image: url('{{ empty($chatRoom->partner_main_photo) ? asset('img/no_image_' . $chatRoom->partner_sex . '.png') : asset($chatRoom->partner_main_photo) }}');">{!! $chatRoom->new_msgs_cnt ? "<div>$chatRoom->new_msgs_cnt</div>" : '' !!}</div>
				<div class="name">
					<p>
						{{ $chatRoom->partner_name }}
					</p>
				</div>
			</a>
			<a href="{{ url(Lang::getLocale() . '/cabinet/delete-chat-room-admin/' . $chatRoom->id.'?idledis='.$idledis) }}">
				<div class="send close"></div>
			</a>
		</div>
	@endforeach
@endif

@if (isset($chatHistoryRoomsList))
	@foreach($chatHistoryRoomsList as $chatRoom)
		<div class="status">
			<a class="status-link" href="{{ url(Lang::getLocale() . '/cabinet/chat-history/' . $chatRoom->partner_id) }}">
				<div class="small_avatar" style="background-image: url('{{ empty($chatRoom->partner_main_photo) ? asset('img/no_image_' . $chatRoom->partner_sex . '.png') : asset($chatRoom->partner_main_photo) }}');">{!! $chatRoom->new_msgs_cnt ? "<div>$chatRoom->new_msgs_cnt</div>" : '' !!}</div>
				<div class="name">
					<p>
						{{ $chatRoom->partner_name }}
					</p>
				</div>
			</a>
			<a href="{{ url(Lang::getLocale() . '/cabinet/delete-history-chat-room/' . $chatRoom->id) }}">
				<div title="@lang('cabinet.delete_letters_erase_letter')" class="send close"></div>
			</a>
		</div>
	@endforeach
@endif
