<style>
	#popupComplain form {
		font-family: UbuntuRegular, sans-serif;
	}

	#popupComplain label {
		margin-bottom: 3px;
	}

	#popupComplain label,
	#popupComplain textarea,
	#popupComplain input {
		float: left;
		width: 289px;
		font-size: 13px;
	}

	#popupComplain textarea,
	#popupComplain input {
		font-size: 12px;
		padding: 3px;
	}

	#popupComplain textarea {
		resize: vertical;
		max-height: 300px;
		min-height: 150px;
	}

	#popupComplain label,
	#popupComplain button {
		margin-top: 15px;
	}

	#popupComplain button {
		background: #951b81;
		padding: 2px 26px;
		float: right;
		margin-right: 3px;
		border: none;
		color: #ffffff;
	}
</style>
<div id="popupComplain" style="display: none;">
	<form>
		<label for="tema">Причина жалобы</label>
		<br />
		<input id="tema">
		<br />
		<label for="text">Текст жалобы</label>
		<br />
		<textarea id="text" placeholder=""></textarea>
		<br />
		<button type="submit">Пожаловаться</button>
	</form>
</div>