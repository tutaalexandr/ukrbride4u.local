@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/account.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')

	<div class="profile">
		<div class="h1">
			@lang('cabinet.account_title')
		</div>
		@include('cabinet.inc.timer')
		@if (count($errors) > 0)
			<div class="errors">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form id="profileEditForm" action="{{ url(Lang::getLocale() . '/auth/edit-acc') }}" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="input">
				<div class="left_input">
					<label>@lang('cabinet.account_email')</label>
					<br />
					<input type="text" name="email" value="{{ $user['email'] }}">
				</div>
				<div class="right_input">
					<label>@lang('cabinet.account_password')</label>
					<br />
					<input type="password" name="password" value="{{ old('password') }}">
				</div>
				<div class="right_input">
					<label>@lang('cabinet.account_password_confirm')</label>
					<br />
					<input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
				</div>
				<div class="clear">
					<button type="submit" class="ok">
						@lang('cabinet.account_save')
					</button>
				</div>
			</div>
		</form>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
