@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/deleted-list.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')

	<div class="profile">
		<div class="h1">
			@lang('cabinet.delete_letters_title')
		</div>
		@include('cabinet.inc.timer')
		<div class="main">
			@foreach($deleteList AS $msg)
				<div class="inbox">
					<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $msg->partner_id) }}">
						<div class="avatar" style="background-image: url('{{ empty($msg->partner_photo) ? asset('img/no_image_' . $msg->partner_sex . '.png') : asset($msg->partner_photo) }}');">
							@if ($msg->partner_online)
								<div class="online">
									<p>@lang('cabinet.delete_letters_online')</p>
								</div>
							@endif
						</div>
					</a>
					<div class="info">
						<div class="name">
							<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $msg->partner_id) }}">
								<div>
									<h3>{{ $msg->partner_name }}</h3>
								</div>
							</a>
							<div class="id">
								<h2>@lang('cabinet.delete_letters_partner_id'):</h2>
							</div>
							<div class="id_number">
								<h2>{{ str_pad($msg->partner_id, 5, "0", STR_PAD_LEFT) }}</h2>
							</div>
							<div class="age">
								@if ($msg->partner_age)
									@lang('cabinet.delete_letters_partner_age'): {{ $msg->partner_age }}
								@endif
							</div>
							<div class="mail">{{ str_limit($msg->msg, $limit = 150, $end = '...') }}</div>
						</div>
						<div class="sent_button">{{ ($msg->is_sent) ? trans('cabinet.delete_letters_sent_letter') : trans('cabinet.delete_letters_received_letter') }}</div>
						<div class="bookmarkers">
							<ul>
								<li>
									<a title="@lang('cabinet.delete_letters_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $msg->partner_id) }}">
										<img src="{{ asset('img/letter-purple.png') }}" alt="@lang('cabinet.delete_letters_write_letter')">
									</a>
								</li>
								@if ($msg->partner_sex == 'w')
									<li>
										<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $msg->partner_id) }}">
											<img src="{{ asset('img/gift-purple.png') }}"></a>
									</li>
									<li>
										<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' .  $msg->partner_id) }}">
											<img src="{{ asset('img/diamant-purple.png') }}"></a>
									</li>
								@endif
								<li>
									<a title="@lang('cabinet.delete_letters_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $msg->partner_id) }}">
										<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.delete_letters_start_chat')">
									</a>
								</li>
								<li>
									<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $msg->partner_id) }}">
										<img src="{{ asset('img/hearth-purple.png') }}">
									</a>
								</li>
							</ul>
						</div>

						<form action="{{ url(Lang::getLocale() . '/mail/letter-erase') }}" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="msg_id" value="{{ $msg->id }}">
							<button type="submit" class="delete_button">{{ trans('cabinet.delete_letters_erase_letter') }}</button>
						</form>
						<a href="{{ url(Lang::getLocale() . '/cabinet/read-letter/' . $msg->id) }}" class="read_button">@lang('cabinet.delete_letters_read_letter')</a>
					</div>
				</div>
				<hr>
			@endforeach
		</div>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
