@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/chat.css') }}" rel="stylesheet" />
	<style>
		.girls-triangle{
			position: absolute;
			border-color: rgba(0, 0, 255, 0) rgba(0, 0, 255, 0) rgba(0, 0, 255, 0) rgb(85, 151, 185);
			border-style: solid;
			border-width: 35px;
			content: " ";
			left: 210px;
			top: 50%;
			transform: translateY(-50%);
			cursor: pointer;
		}
		.girls-panel{
			position: fixed;
			left: -211px;
			border-color: rgb(85, 151, 185);
			width: 210px;
			background-color: white;
			border-style: solid;
			display: block;
			border-width: 1px;
			transition: all, 1s;
			z-index: 10000;
			top: 50%;
			transform: translateY(-50%);
		}
		.girl-wrap{
			overflow-x: hidden;
			width: 100%;
			height: 600px;
			overflow-y: scroll;
			margin-bottom:7px;
		}
		.girls-panel-icon{
			color: white;
			background: rgb(85, 151, 185) none repeat scroll 0% 0%;
			position: absolute;
			right: 16px;
			top: -10px;
		}
		.add{
			background-repeat: no-repeat;
			background-image: url("{{ asset('img/send-add.png') }}");
		}
	</style>
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker-admin')
	<script type="text/javascript">
		$(document).ready(function () {
			@if ($partner_id)
			var chatMsgsBlock = $('.chat_window');
			var chatScroller = $('#scroller');
			var chatSendMsgForm = $('#chatSendMsgForm');
			var chatSendMsgButton = $('#chatSendMsgButton');
			var chatMyMsg = $('#chatMyMsg');
			var smButton = $('.sm-button');
			var smArea = $('.sm-area');

			autosize(chatMyMsg);

			chatScroller.perfectScrollbar();

			function updateTextArea() {
				autosize.update(chatMyMsg);
				$('.msg-size span').html(chatMyMsg.val().length);
			}

			var canSendMsg = true;

			function submitForm() {
				$.ajax({
					async:      true,
					url:        '{{ url(Lang::getLocale() . '/chat/add-admin') }}',
					type:       'POST',
					data:       chatSendMsgForm.serialize(),
					beforeSend: function () {
						chatSendMsgButton.attr("disabled", "disabled").css("opacity", "0.8");
						canSendMsg = false;
					},
					success:    function () {
					},
					error:      function (request, status, error) {
						if (request.status === 401)
							window.location = '{{ url(Lang::getLocale()) }}';
					},
					complete:   function () {
						chatSendMsgButton.removeAttr("disabled").css("opacity", "1");
						canSendMsg = true;
						chatMyMsg.focus();
					},
				});
			}

			chatSendMsgForm.bind('submit', function (e) {
				e.preventDefault();
				if (!canSendMsg)
					return;
				if ($.trim(chatMyMsg.val()).length === 0)
					return;
				submitForm();
				chatMyMsg.val("");
				smArea.fadeOut();
				updateTextArea();
			});

			chatMyMsg.keydown(function (e) {
				if (e.ctrlKey && e.keyCode === 13) { // ctrl + enter
					chatMyMsg.insertAtCaret("\n");
					scrollElToBottom(chatScroller);
				}
				else if (!e.ctrlKey && e.keyCode === 13) { // enter
					e.preventDefault();
					if (!canSendMsg)
						return;
					if ($.trim(chatMyMsg.val()).length === 0)
						return;
					submitForm();
					chatMyMsg.val("");
					smArea.fadeOut();
					updateTextArea();
				}
			});

			chatMyMsg.on('keyup change', function (e) {
				var el = $(this);
				// fix user data
				if (el.val().match(/^[\r\n]/g))
					el.val(el.val().replace(/^[\r\n]/g, ""));
				if (el.val().match(/[\r\n]{3,}/g))
					el.val(el.val().replace(/[\r\n]{3,}/g, "\r\n\r\n"));
				if (el.val().length > 1000)
					el.val(el.val().substr(0, 1000));
				updateTextArea();
			});

			$('.sm-area img').bind('click', function (e) {
				e.preventDefault();
				chatMyMsg.insertAtCaret(' ' + $(this).attr('c') + ' ').trigger('change');
				chatMyMsg.focus();
			});

			smButton.bind('click', function (e) {
				e.preventDefault();

				smArea.is(':visible') ? smArea.fadeOut() : smArea.fadeIn();
			});

			chatMyMsg.bind('click', function (e) {
				e.preventDefault();

				if (smArea.is(':visible'))
					smArea.fadeOut();
			});
			@endif

			setInterval(function () {
				$.ajax({
					async:   true,
					url:     '{{ url(Lang::getLocale() . '/cabinet/chat-online-from-admin') }}',
					type:    'POST',
					data:    '_token={{ csrf_token() }}&partner_id={{ $partner_id }}',
					success: function (data) {
						$('div.right_menu').fadeOut(1000, function () {
							$('div.right_menu').html(data).tooltip().fadeIn(1000);
						});
					},
					error:   function (request, status, error) {
						if (request.status === 401)
							window.location = '{{ url(Lang::getLocale()) }}';
					}
				});
			}, 60000);
			setInterval(function () {
				$.ajax({
					async:   true,
					url:     '{{ url(Lang::getLocale() . '/cabinet/mass-arr-mass') }}',
					type:    'POST',
					data:    '_token={{ csrf_token() }}&idledis={{$idledis}}',
					success: function (data) {
						$('#girls-panel').fadeOut(500, function () {
							$('#girls-panel').html(data).tooltip().fadeIn(500);
						});
					},
					error:   function (request, status, error) {
						if (request.status === 401)
							window.location = '{{ url(Lang::getLocale()) }}';
					}
				});
			}, 5000);
			scrollWindowTo(190);
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	<div class="chat">
		<div class="header_chat">
			<a id="turn" href="{{ url(Lang::getLocale() . $chat_previous_url) }}"></a>
			<a id="close" href="{{ url(Lang::getLocale() . '/cabinet/close-chat') }}"></a>
		</div>
		<div class="sidebar">
		<div class="sidebar-title">You are:</div>
			<div id = "girls-panel" class="girl-wrap">
				@include('cabinet.girls-panel')
			</div>
			{{--<div class="chat-rooms-sidebar">
				@include('cabinet.girls-panel')
			</div>--}}
		<div class="sidebar-title">Men's List:</div>
			<input type="hidden" value="{{ $chatRoomsKeyList }}" name="crkl" />
			<div class="chat-rooms-sidebar">
				@include('cabinet.inc.chat-rooms-admin')
			</div>
{{--			<div class="histoty">
				<div class="clock"></div>
				<div class="history_text">
					<a href="{{ url(Lang::getLocale() . '/cabinet/chat-history') }}">@lang('cabinet.chat_history')</a>
				</div>
			</div>--}}
		</div>
        <div id="nav-one"></div>
        <audio id="beep-one" >
            <source src="{{ asset('img') }}/audio/beep.mp3"></source>
        </audio>
        <script>
            function flag_sound() {
                $("#nav-one").bind("click");
            }
            var beepOne = $("#beep-one")[0];
            $("#nav-one").click(function() {
                        beepOne.play();
                    });
        </script>
		<div class="right_menu" style="overflow: auto">
			<span class="online-header">Men Online</span>
			@include('cabinet.inc.chat-online-from-admin')
		</div>
		<div class="main_window">
			@if ($partner_id)
				<div class="header_window">
					<div class="info">
						<a class="name_girl" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_id) }}">
							<div class="big_avatar" style="background-image: url('{{ empty($partner_info->main_photo) ? asset('img/no_image_' . $partner_info->sex . '.png') : asset($partner_info->main_photo) }}');"></div>
						</a>
						<div class="contat_info">
							<a class="name_girl" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_id) }}">
								{{ $partner_info->name }}
							</a>
							{!! "<span class=\"chat-partner-status\" pid=\"$partner_id\">" . ($partner_info->online ? trans('cabinet.chat_online') : trans('cabinet.chat_offline')) . '</span>' !!}
							<br />
							@if ($partner_info->age)
								<span class="age">@lang('cabinet.chat_partner_age'): <span>{{ $partner_info->age }}</span></span>
								<br />
							@endif
							<span>{{ $partner_info->address }}</span>
							<br />
							<a class="stop-chat-button{{ $needStop ? '' : ' display-none' }}" href="{{ url(Lang::getLocale() . '/cabinet/stop-chat/' . $partner_id) }}">@lang('cabinet.chat_stop')</a>
						</div>
					</div>
					<div class="gifts" style="{{ $partner_info->sex == 'w' ? '' : 'width: auto;' }}">
						<div class="send_center">
							<a title="@lang('cabinet.read_letter_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $partner_id) }}">
								<img src="{{ asset('img/letter-purple.png') }}" alt="@lang('cabinet.read_letter_write_letter')">
							</a>
						</div>
						<div class="send_center">
							<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $partner_id) }}">
								<img src="{{ asset('img/hearth-purple.png') }}">
							</a>
						</div>
					</div>
				</div>
				<div class='scroller' id='scroller'>
					<div class='chat_window'>
					</div>
				</div>
				<div class="textarea textarea-send-msg-form{{ $needStart ? ' display-none' : '' }}">
					<div class="sm-area">
						@foreach($smiles_list AS $key => $smile)
							<img c="{{ $key }}" src="{{ asset('img/sm/ls/' . $smile) }}" />
						@endforeach
					</div>
					<a href="#" class="sm-button"></a>
					<form id="chatSendMsgForm">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="partner_id" value="{{ $partner_id }}">
						<input type="hidden" name="idledis" value="{{$idledis}}">
						<div id="chatMyMsgBlock">
							<textarea id="chatMyMsg" name="chatMyMsg" wrap="soft" placeholder="@lang('cabinet.chat_placeholder_msg')"></textarea>
						</div>
						<button type="submit" id="chatSendMsgButton">@lang('cabinet.chat_send')</button>
					</form>
					<div class="msg-size">
						<span>0</span>
						/ 1000
					</div>
				</div>
				<div class="textarea textarea-start-buttons{{ $needStart ? '' : ' display-none' }}">
					<a class="start-chat-button{{ $canStartChat ? '' : ' display-none' }}" href="{{ url(Lang::getLocale() . '/cabinet/start-chat/' . $partner_id) }}">@lang('cabinet.chat_start')</a>
					<span title="User offline or not enough money in the account to start a new chat" class="start-chat-button-disabled{{ $canStartChat ? ' display-none' : '' }}" href="{{ url(Lang::getLocale() . '/cabinet/start-chat/' . $partner_id) }}">@lang('cabinet.chat_start')</span>
				</div>
			@else
				<div class="select-partner-text">
					<<- @lang('cabinet.chat_select_partner_msg') ->>
				</div>
			@endif
		</div>
        <!------------------------  Цикл вывода девушек начинается отсюдда ---------------------------->
        <div class="girls-panel">
			{{--<div id = "girls-panel" class="girl-wrap">
				@include('cabinet.girls-panel')
			</div>--}}
            <hr>
			<div class="girl-wrap">
				@foreach($girl_wrap AS $key => $value)
					<div class="status">
						<a class="status-link" href="{{ asset('/') }}cabinet/write-add/{{$value->id}}">
							{{--<div class="small_avatar" style="background-image: url('http://ukrbride4u.com/{{$value->main_photo}}');"></div>--}}
							<div class="name">
								<p>
									{{$value->name}}
								</p>
							</div>
						</a>
						<a href="{{ asset('/') }}cabinet/write-add/{{$value->id}}">
							<div class="send add"></div>
						</a>
					</div>
				@endforeach

			</div>

			<div class="girls-triangle"><i class="girls-panel-icon"><img width="16" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAEgklEQVRYR8VXMUwqSRieIVETxYRrhO446ImWrg0Wu9A9iC2or0Ob9y4utkgrGN81xA6jtrpeB7smUmn5gB611FicCWBkDc7l2+ySYQUWLnm5SSYSd+efb/7/m+//lpL/eVCn/SVJilFKvzDGwoQQv+39F8ZY1eVy/d3pdI4rlcqLUzz786EARFHcpJRmvF6vXxAEgun1eo1pjXq9ThqNBtE0zfhLCPmh63p2EiCfAITDYc/U1JTi8/nCyWSSiKI41qEA5vT0lNTr9ReXyxUvlUqVcRb2ARBFcZFSqgiC4N/Z2SFut3ucGH3vKIpCjo6OCGPsq6Zpx04BegBw8unp6Z+iKPplWR647unpiWBaIxQKDXwP5djd3SXNZtMRRA+AKIrXKysr4Uwm8ykoToVpbl5ljL1QSsPI0PLyMkGpeG4gAHiRz+dRjqVSqfQwLBMGABDO5/MVC4VCX9pxkoODAxDsgTGWfX9/v+QJhpK5XK5vc3Nzm6lU6hNfUIqLi4uKpmmrIwFIknQvy7KfJ5yVxna7fVwul7+OqmU0Gg1/fHwosix7+BitVotsbGyQ19fX1WGkpLjnXq9XOTk56dtje3ub3N3dOW5uLTIJ/DOXyxGeG8jC5eXl0Dg0EokUY7GYkUJrmEx+UFX1DycW888lSdoLhUIZgLAGMonDqKo6UHOQgftcLufnUa+vr5PHx0dHBtvBmTfpvlAoeILBYO/x2tra0DIAAEP6LRaD6QCg6/pvkyiatZskSUoymYwlEokegHQ6TWq12sADGQDK5XKfvKbT6aqqqkuTpJ8DsCcIQoa/zgBQr9ezqqru2WMOBCDL8sirMwrYIB5MDAALhpHGKSsgdSKR2LSXYFQG/snlch6ehCBNs9lc0jSt6rSh/TlInclkjA5qDXDq+fl5oBZQSPDW1lY4Ho/3FuTzeXJ1dTW2BlgLIUizs7PX5+fnvVhOpIYOfA8EAoeQYWtgEe5uq9WKq6p6OU4WrGaWSqX8/GFMTRlKamouRBn6FMxqJoyxVadSmDGuBUFYtDczJ00x1EmSpMNQKPSdVzD83wSBn1ld138M0gXLOQ3yEOgFaMuNRqOq6/rqoPUGAEvBUqmUh08fnkFKoedwPIQQlKOGH5TS3+ET3W63H4y3r+PLBk5pmoaOGrdns6fPZke7tpeC1/Tb21tINGm32wRSGwgEDK9oH8gcBt8ZcQhFUWBi+0pqt2QwokU4onG9oH3zs7MzcnNzQ3hSW+9YJeXt2qcOhfZMCCmKouhBh5zEF4LxAIBy8ELEgwQ4mJxWq2VI88AWGY1G/d1utzg/Px9GJjD57sYHxJVFUNOywXqBJ5t2c8Kv4czOnyM/TMALxtgGYwxZ6VNLBORMKnziX5YLNs3JNbI4zOACdDabfXH8MuJVrtvt+imlxtcRpRTGtPr29lYdcj1h8YvQhmEWPxKJkLEBjKOG9ncsgQoGg4v7+/t9fLIk+pcCsDRmZmbmcGFhAbww1BabQxtqtVrllwOwsgKfQAj5Bi6ZJTzudDqjSfhf0j7pmn8Bn7CaRWlrBRUAAAAASUVORK5CYII=" alt=""></i></div>
		</div>
		<script>
			//Скриптик для открывания-закрывания девушек
			$('.girls-triangle').on('click', function(e){
				if($('.girls-panel').get(0).style.left!='0px') {
					$('.girls-panel').get(0).style.left = '0px';
				}else{
					$('.girls-panel').get(0).style.left = '-211px';
				}
			})

		</script>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
