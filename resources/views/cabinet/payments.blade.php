@extends('app')


@section('css')
    <link href="{{ asset('css/cabinet/payments.css') }}" rel="stylesheet" />
@stop


@section('js')
    <script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
    @include('cabinet.inc.checker')
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                location.reload();
            }, 1000 * 60 * 5);

            $('a[href="#payForm"]').fancybox({
                'scrolling': 'no',
                'titleShow': false,
                'closeBtn':  false,
                'afterShow': function () {
                    $('#payForm').find('input[name$="_price"]').focus();
                }
            });

            $('.scroller').perfectScrollbar();

        //    scrollElToBottom($('.scroller'));

            $('.payments_button').click(function() {
             var custom = $('#custom').val();
             $.ajax({
             async:      true,
             url:        '{{ url(Lang::getLocale() . '/cabinet/payments-session') }}',
             type:       'GET',
             data:       'custom=' + custom,
             beforeSend: function () {
             },
             success:    function (data) {
          /*   if (data === 'true')
             alert('sesion on');
             else
             alert('sesion of');*/
             },
             error:      function (request, status, error) {
             if (request.status === 401)
             window.location = '{{ url(Lang::getLocale()) }}';
             }
             });
             //   return false;
           //  $( "#form" ).submit();
             });
        });
    </script>
@stop


@section('header')
    @include('cabinet.inc.header')
@stop


@section('content')
    @include('cabinet.inc.menu')

    <div class="profile">
        <div class="balance">
            <div>@lang('cabinet.payments_your_balance')</div>
            <div>{{ $balance }} $</div>
        </div>
        <a href="#payForm" class="payments_button">
            @lang('cabinet.payments_add_funds')
        </a>
        <div class="h1">
            @lang('cabinet.payments_title')
        </div>
        @include('cabinet.inc.timer')
        <div class="clear"></div>
        <table style="margin-top: 28px;">
            <tr>
                <th style="width: 120px;">@lang('cabinet.payments_list_date')</th>
                <th style="width: 320px;">@lang('cabinet.payments_list_event')</th>
                <th style="width: 170px;">@lang('cabinet.payments_partner')</th>
                <th style="">@lang('cabinet.payments_paid')</th>
            </tr>
        </table>
        <div class="clear"></div>
        <div class="scroller">
            <table>
                @foreach($historyData AS $action)
                    <tr class="blue">
                        <td style="text-align: center; width: 120px;">{{ $action->date }}</td>
                        <td style="width: 320px;">@lang('cabinet.event_' . $action->event)</td>
                        <td style="text-align: center; width: 170px;">
                            @if (empty($action->partner_id))
                                --
                            @else
                                <a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $action->partner_id) }}">{{ $action->partner_name }}</a>
                            @endif
                        </td>
                        <td style="text-align: right;">{{ ($action->event == 'recharge' ? '+' : '-') . $action->paid }} $</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div id="payForm" style="display: none;">
            <div class="pay-form-h1">The amount of replenishment:</div>
           {{-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="PEH2ZXDXATSS2">
                <table>
                    <tr><td><input type="hidden" name="on0" value="Pricing - UKR Bride 4 U">Pricing - UKR Bride 4 U</td></tr><tr><td><select name="os0">
                                <option value="Live Chat - per minute">Live Chat - per minute $0,01 USD</option>
                                <option value="Letter">Letter $3,00 USD</option>
                                <option value="Meet in Person">Meet in Person $20,00 USD</option>
                            </select> </td></tr>
                </table>
                <input type="hidden" name="currency_code" value="USD">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>--}}

                        <form id="form" method="post" action= "https://www.paypal.com/cgi-bin/webscr">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="business" value="Ukrbride4U@gmail.com">
                            <input type="hidden" id="custom" name="custom" value="{{ rand(10000 , 1000000) }}">
                            <input type="hidden" name="item_name" value="ADD Money To Ukrbride4u">
                            <input type="hidden" name="item_number" value="{{ $id_user }}">
                            <input type="text" name="amount" value="0.01">
                            <input type="hidden" name="no_shipping" value="1">
                            <input type="submit" id = "submit" value="Buy Now">
                        </form>
            {{--<form action="{{ url(Lang::getLocale() . '/cabinet/paymentspaypal') }}" method="get">
                <input type='number' name='total' required min='1' step='1' value='10' autofocus >
                <button type="submit">
                    Pay Now
                </button>
            </form>--}}
        </div>

        <div class="clear"></div>
    </div>

@stop


@section('footer')
    @include('cabinet.inc.footer')
@stop


@section('additional_data')
    @include('cabinet.inc.complain')
@stop