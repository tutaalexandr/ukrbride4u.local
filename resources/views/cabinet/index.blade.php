@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/inc/header.css') }}" rel="stylesheet" />
	<link href="{{ asset('css/cabinet/inc/menu.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')
@stop


{{--@section('footer')--}}
{{--@stop--}}


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
