@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/search.css') }}" rel="stylesheet">
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			$('input[name="address"]').autocomplete({
				source:    '{{ url(Lang::getLocale() . '/cabinet/autocomplete-address') }}',
				minLength: 2
			});
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	@include('cabinet.inc.menu')

	<div class="profile">
		<div class="h1">
			@lang('cabinet.search_title')
		</div>
		@include('cabinet.inc.timer')
		@if (count($errors) >0)
			<div class="errors">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<div class="main">
			<form action="{{ url(Lang::getLocale() . '/cabinet/search') }}" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="left">

					{{-- BEGIN AGE --}}
					<label>@lang('cabinet.search_age')</label>
					<br />
					<input name="fromAge" placeholder="@lang('cabinet.search_from')" value="{{ old('fromAge') }}">
					<input name="toAge" placeholder="@lang('cabinet.search_to')" value="{{ old('toAge') }}">
					<br />

					{{-- HEIGHT --}}
					<label>@lang('cabinet.search_height')</label>
					<br />
					<input name="fromHeight" placeholder="@lang('cabinet.search_from')" value="{{ old('fromHeight') }}">
					<input name="toHeight" placeholder="@lang('cabinet.search_to')" value="{{ old('toHeight') }}">
					<br />

					{{-- WEIGHT --}}
					<label>@lang('cabinet.search_weight')</label>
					<br />
					<input name="fromWeight" placeholder="@lang('cabinet.search_from')" value="{{ old('fromWeight') }}">
					<input name="toWeight" placeholder="@lang('cabinet.search_to')" value="{{ old('toWeight') }}">
					<br />

				</div>

				<div class="center">

					{{-- COUNTRY --}}
					<label>@lang('cabinet.search_address')</label>
					<br />
					<input name="address" placeholder="" value="{{ old('address') }}">
					<br />

					{{-- ENGLISH --}}
					<label>@lang('cabinet.search_en_lvl')</label>
					<br />
					<select name="en_level">
						@if (old('en_level') == NULL)
							<option></option>
						@endif
						@foreach ($enLevelList AS $value)
							<option value="{{ $value }}"{{ (old('en_level') == $value) ? ' selected' : '' }}>@lang('cabinet.' . $value)</option>
						@endforeach
					</select>

				</div>

				<div class="right">

					{{-- HAIR COLOR --}}
					<label>@lang('cabinet.search_hair_color')</label>
					<br />
					<select name="hair">
						@if (old('hair') == NULL)
							<option></option>
						@endif
						@foreach ($hairList AS $value)
							<option value="{{ $value }}"{{ (old('hair') == $value) ? ' selected' : '' }}>@lang('cabinet.' . $value)</option>
						@endforeach
					</select>

					{{-- EYES COLOR --}}
					<label>@lang('cabinet.search_eyes_color')</label>
					<br />
					<select name="eyes">
						@if (old('eyes') == NULL)
							<option></option>
						@endif
						@foreach ($eyesList AS $value)
							<option value="{{ $value }}"{{ (old('eyes') == $value) ? ' selected' : '' }}>@lang('cabinet.' . $value)</option>
						@endforeach
					</select>
				</div>

				<div class="button_cheaked">
					<p>@lang('cabinet.search_only_show')</p>
					<div class="cheaked">
						<div>
							<input name="withVideo" id="withVideo" type="checkbox"{{ old('withVideo') ? ' checked=""' : '' }}>
							<label for="withVideo"><span></span>@lang('cabinet.search_with_videos')</label>
						</div>
						<div>
							<input name="isOnline" id="isOnline" type="checkbox"{{ old('isOnline') ? ' checked=""' : '' }}>
							<label for="isOnline"><span></span>@lang('cabinet.search_who_online')</label>
						</div>
					</div>
					<button type="submit">@lang('cabinet.search_search')</button>
				</div>
			</form>
			<div class="clear"></div>
			@if (!empty($usersList))
				<br />
				<hr />
				@foreach ($usersList as $user)
					<div class="inbox">
						<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $user->id) }}">
							<div class="avatar" style="background-image: url('{{ empty($user->main_photo) ? asset('img/no_image_' . $user->sex . '.png') : asset($user->main_photo) }}');">
								@if ($user->online)
									<div class="online">
										<p>@lang('cabinet.users_online_online')</p>
									</div>
								@endif
							</div>
						</a>
						<div class="info">
							<div class="name">
								<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $user->id) }}">
									<div>
										<h3>{{ $user->name }}</h3>
									</div>
								</a>
								<div class="id">
									<h2>@lang('cabinet.users_online_partner_id'):</h2>
								</div>
								<div class="id_number">
									<h2>{{ str_pad($user->id, 5, "0", STR_PAD_LEFT) }}</h2>
								</div>
								<div class="age">
									@if ($user->age)
										@lang('cabinet.users_online_partner_age'): {{ $user->age }}
									@endif
								</div>
								<div class="mail">
									{{ str_limit($user->about_me, $limit = 150, $end = '...') }}
								</div>
							</div>
							<div class="bookmarkers">
								<ul>
									<li>
										<a title="@lang('cabinet.users_online_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $user->id) }}">
											<img src="{{ asset('img/letter-purple.png') }}" alt="@lang('cabinet.users_online_write_letter')">
										</a>
									</li>
									@if ($user->sex == 'w')
										<li>
											<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $user->id) }}">
												<img src="{{ asset('img/gift-purple.png') }}"></a>
										</li>
										<li>
											<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $user->id) }}">
												<img src="{{ asset('img/diamant-purple.png') }}"></a>
										</li>
									@endif
									<li>
										<a title="@lang('cabinet.users_online_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $user->id) }}">
											<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.users_online_start_chat')">
										</a>
									</li>
									<li>
										<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $user->id) }}">
											<img src="{{ asset('img/hearth-purple.png') }}">
										</a>
									</li>
									@if ($user->sex == 'w')
									<li>
										<a title="@lang('cabinet.users_online_bay_fav')" href="{{ url(Lang::getLocale() . '/cabinet/bay-fav/' . $user->name) }}">
											<img src="{{ asset('img/cart_btn.png') }}"></a>
									</li>
									@endif
								</ul>
							</div>
						</div>
					</div>
					<hr>
				@endforeach
			@endif
		</div>
	</div>
@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
