@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/send-gift.css') }}" rel="stylesheet" />
	<style type="text/css">
	</style>
@stop


@section('js')
	<script src="{{ asset('js/checkbox.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			var letterTextArea = $('textarea[name="msg"]');

			autosize(letterTextArea);

			letterTextArea.on('keyup change', function (e) {
				var el = $(this);
				// fix user data
				if (el.val().match(/^[\r\n]/g))
					el.val(el.val().replace(/^[\r\n]/g, ""));
				if (el.val().match(/[\r\n]{3,}/g))
					el.val(el.val().replace(/[\r\n]{3,}/g, "\r\n\r\n"));
				if (el.val().length > 1000)
					el.val(el.val().substr(0, 1000));

				autosize.update(letterTextArea);
				$('.msg-size span').html(letterTextArea.val().length);
			});

			$('.checklist a').bind('click', function (e) {
				if ($('.checklist li.selected').length > 0)
					$('#submitGiftForm').removeClass('total-disable').addClass('total');
				else
					$('#submitGiftForm').removeClass('total').addClass('total-disable');
			});

			var giftIsSend = false;
			$('#submitGiftForm').bind('click', function (e) {
				e.preventDefault();
				if (giftIsSend || $('.checklist li.selected').length == 0)
					return;
				giftIsSend = true;
				$(this).removeClass('total').addClass('total-disable');
				$('#myform').submit();
			});
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	
	@include('cabinet.inc.menu')
	@include('cabinet.inc.timer')

	<div class="card">
		<div class="h1">
			TO SEND GIFT
		</div>
		<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_info->id) }}">
			<div class="avatar" style="background-image: url('{{ empty($partner_info->main_photo) ? asset('img/no_image_' . $partner_info->sex . '.png') : asset($partner_info->main_photo) }}');">
				@if ($partner_info->online)
					<div class="online">
						<p>@lang('cabinet.write_letter_online')</p>
					</div>
				@endif
			</div>
		</a>
		<form id="myform" name="formSendMail" action="{{ url(Lang::getLocale() . '/mail/sendgifts') }}" method="POST" enctype="multipart/form-data">
			<div class="info">
				<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_info->id) }}">
					<h3>{{ $partner_info->name }}</h3>
				</a>
				<div class="gifts">
					<a title="@lang('cabinet.users_online_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $partner_id) }}">
						<img src="{{ asset('img/letter-purple.png') }}"></a>
					<a title="@lang('cabinet.users_send_gift_real')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift-real/' . $partner_id) }}">
						<img src="{{ asset('img/gift-purple.png') }}"></a>
					<a title="@lang('cabinet.write_letter_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $partner_id) }}">
						<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.write_letter_start_chat')"></a>
					<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $partner_id) }}">
						<img src="{{ asset('img/hearth-purple.png') }}"></a>
				</div>
				@if(isset($partner_info->age))
					<div class="age">
						Age:
						<div class="age_number">{{ $partner_info->age }}</div>
					</div>
				@endif
				<div class="about">
					@if ($partner_info->address)
						<span>{{ $partner_info->address }}</span>
						<br />
					@endif
					@if ($partner_info->eyes || $partner_info->hair)
						Divorced lady with
					@endif
					@if ($partner_info->eyes)
						<span>{{ $partner_info->eyes . ' eyes'}}</span>
					@endif
					@if ($partner_info->eyes && $partner_info->hair)
						and
					@endif
					@if ($partner_info->hair)
						<span>{{ $partner_info->hair . ' hair.'}}</span>
					@endif
				</div>
				<div class="comment">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="partner_id" value="{{ $partner_id }}">
					<label for="comment">Comment</label>
					<br />
					<textarea id="comment" name="msg">{{ old('msg') }}</textarea>
					<div class="msg-size">
						<span>0</span>
						/ 1000
					</div>
				</div>
			</div>
			<h2>Virtual Gift Form</h2>
			<p class="season">Select a Gift</p>
			@foreach ($gifts as $gift)
				<div class="checkbox">
					<h3></h3>
					<label for="1"><img src="{{ asset('img/vm/'.$gift->image) }}" width='85' height="88"></label>
					<ul class="checklist">
						<li>
							<input type="checkbox" name="gifts[]" value="{{ $gift->id }}" />
							<div></div>
							<div><span>{{ $gift->price }} $</span></div>
							<a class="checkbox-deselect" href="#" style="background-image: url('{{ asset('img/checkbox-ok.png') }}')"></a>
							<a class="checkbox-select" href="#" style="background-image: url('{{ asset('img/checkbox-none.png') }}')"></a>
						</li>
					</ul>
				</div>
			@endforeach
		</form>
	</div>
	<a id="submitGiftForm" href="#" class="total-disable">Checkout</a>

@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
