@extends('app')


@section('css')
	<link href="{{ asset('css/cabinet/send-gift.css') }}" rel="stylesheet" />
	<style type="text/css">
		#popupComplain2 > label {
			margin-top: 30px;
			float: left;
			color: #4D97A7;
		}

		#popupComplain2 > .total,
		#popupComplain2 > .total-disable {
			margin-right: 94px;
			margin-top: 24px;
		}

		#popupComplain2 form {
			font-family: UbuntuRegular, sans-serif;
		}

		#popupComplain2 label {
			margin-bottom: 3px;
		}

		#popupComplain2 label,
		#popupComplain2 textarea,
		#popupComplain2 input {
			float: left;
			width: 289px;
			font-size: 13px;
		}

		#popupComplain2 textarea,
		#popupComplain2 input {
			font-size: 12px;
			padding: 3px;
		}

		#popupComplain2 textarea {
			resize: vertical;
			max-height: 300px;
			min-height: 150px;
		}

		#popupComplain2 label,
		#popupComplain2 button {
			margin-top: 15px;
		}

		#popupComplain2 button {
			background: #951b81;
			padding: 2px 26px;
			float: right;
			margin-right: 3px;
			border: none;
			color: #ffffff;
		}
	</style>
@stop


@section('js')
	<script src="{{ asset('js/checkbox.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/cabinet.js') }}"></script>
	@include('cabinet.inc.checker')
	<script type="text/javascript">
		$(document).ready(function () {
			var letterTextArea = $('textarea[name="msg"]');

			autosize(letterTextArea);

			$('a[href="#popupComplain2"]').fancybox({
				'scrolling': 'no',
				'titleShow': false,
				beforeLoad:  function () {
					return $('.checklist li.selected').length > 0;
				}
			});

			letterTextArea.on('keyup change', function (e) {
				var el = $(this);
				// fix user data
				if (el.val().match(/^[\r\n]/g))
					el.val(el.val().replace(/^[\r\n]/g, ""));
				if (el.val().match(/[\r\n]{3,}/g))
					el.val(el.val().replace(/[\r\n]{3,}/g, "\r\n\r\n"));
				if (el.val().length > 1000)
					el.val(el.val().substr(0, 1000));

				autosize.update(letterTextArea);
				$('.msg-size span').html(letterTextArea.val().length);
			});

			$('.checklist a').bind('click', function (e) {
				if ($('.checklist li.selected').length > 0)
					$('a[href="#popupComplain2"]').removeClass('total-disable').addClass('total');
				else
					$('a[href="#popupComplain2"]').removeClass('total').addClass('total-disable');
			});

			var giftIsSend = false;
			$('#submitPopup').bind('click', function (e) {
				e.preventDefault();
				if (giftIsSend || $('.checklist li.selected').length == 0)
					return;
				giftIsSend = true;
				$(this).removeClass('total').addClass('total-disable');
				$('#myform').submit();
			});
		});
	</script>
@stop


@section('header')
	@include('cabinet.inc.header')
@stop


@section('content')
	
	@include('cabinet.inc.menu')
	@include('cabinet.inc.timer')

	<div class="card">
		<div class="h1">
			TO SEND GIFT
		</div>
		<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_info->id) }}">
			<div class="avatar" style="background-image: url('{{ empty($partner_info->main_photo) ? asset('img/no_image_' . $partner_info->sex . '.png') : asset($partner_info->main_photo) }}');">
				@if ($partner_info->online)
					<div class="online">
						<p>@lang('cabinet.write_letter_online')</p>
					</div>
				@endif
			</div>
		</a>
		<form id="myform" name="formSendMail" action="{{ url(Lang::getLocale() . '/mail/sendgiftsreal') }}" method="POST" enctype="multipart/form-data">
			<div class="info">
				<a href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $partner_info->id) }}">
					<h3>{{ $partner_info->name }}</h3>
				</a>
				<div class="gifts">
					<a title="@lang('cabinet.users_online_write_letter')" href="{{ url(Lang::getLocale() . '/cabinet/write-letter/' . $partner_id) }}">
						<img src="{{ asset('img/letter-purple.png') }}"></a>
					<a title="@lang('cabinet.users_send_gift')" href="{{ url(Lang::getLocale() . '/cabinet/send-gift/' . $partner_id) }}">
						<img src="{{ asset('img/diamant-purple.png') }}"></a>
					<a title="@lang('cabinet.write_letter_start_chat')" href="{{ url(Lang::getLocale() . '/cabinet/chat/' . $partner_id) }}">
						<img src="{{ asset('img/mail-purple.png') }}" alt="@lang('cabinet.write_letter_start_chat')"></a>
					<a title="@lang('cabinet.users_online_add_to_fav')" href="{{ url(Lang::getLocale() . '/cabinet/add-fav/' . $partner_id) }}">
						<img src="{{ asset('img/hearth-purple.png') }}"></a>
				</div>
				@if(isset($partner_info->age))
					<div class="age">
						Age:
						<div class="age_number">{{ $partner_info->age }}</div>
					</div>
				@endif
				<div class="about">
					@if ($partner_info->address)
						<span>{{ $partner_info->address }}</span>
						<br />
					@endif
					@if ($partner_info->eyes || $partner_info->hair)
						Divorced lady with
					@endif
					@if ($partner_info->eyes)
						<span>{{ $partner_info->eyes . ' eyes'}}</span>
					@endif
					@if ($partner_info->eyes && $partner_info->hair)
						and
					@endif
					@if ($partner_info->hair)
						<span>{{ $partner_info->hair . ' hair.'}}</span>
					@endif
				</div>
				<div class="comment">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="partner_id" value="{{ $partner_id }}">
					<label for="comment">Comment</label>
					<br />
					<textarea name="msg">{{ old('msg') }}</textarea>
					<div class="msg-size">
						<span>0</span>
						/ 1000
					</div>
				</div>
			</div>
			<h2>Choose Present for Your Lady</h2>
			<p class="season">Season gifts + Special Presents</p>
			@foreach ($gifts as $gift)
				<div class="checkbox">
					<h3>{{ $gift->title }}</h3>
					<label for="1"><img src="{{ asset('img/vm/'.$gift->image) }}" width='170' height="110"></label>
					<ul class="checklist">
						<li>
							<input type="checkbox" name="gifts[]" value="{{ $gift->id }}" />
							<div></div>
							<div><span>{{ $gift->price }} $</span></div>
							<a class="checkbox-deselect" href="#" style="background-image: url('{{ asset('img/checkbox-ok.png') }}')"></a>
							<a class="checkbox-select" href="#" style="background-image: url('{{ asset('img/checkbox-none.png') }}')"></a>
						</li>
					</ul>
				</div>
			@endforeach
		</form>
	</div>
	<a href="#popupComplain2" class="total-disable">Checkout</a>

	<div id="popupComplain2" style="display: none;">
		<label for="text">You have successfully sent the gift for girl. We will deliver it as soon as possible and send you a happy photo of the girl with your gift!</label>
		<br />
		<a id="submitPopup" href="#" class="total">Checkout</a>
	</div>

@stop


@section('footer')
	@include('cabinet.inc.footer')
@stop


@section('additional_data')
	@include('cabinet.inc.complain')
@stop
