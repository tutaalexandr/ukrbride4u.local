@extends('app')


@section('css')
	<link href="{{ asset('css/admin/statistics.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			setTimeout(function () {
				$('#periodEditForm').trigger('submit');
			}, 1000 * 60 * 3);

			$("#from").datepicker({
				minDate:     '-3m',
				maxDate:     '0',
				dateFormat:  "yy-mm-dd",
				defaultDate: "0",
				onSelect:    function () {
					$('#periodEditForm').trigger('submit');
				}
			}).datepicker('setDate', '{{ old('from') ? old('from') : 'today' }}').datepicker("option", "maxDate", '{{ old('to') ? old('to') : 'today' }}');
			$("#to").datepicker({
				minDate:     '-3m',
				maxDate:     '0',
				dateFormat:  "yy-mm-dd",
				defaultDate: "0",
				onSelect:    function () {
					$('#periodEditForm').trigger('submit');
				}
			}).datepicker('setDate', '{{ old('to') ? old('to') : 'today' }}').datepicker("option", "minDate", '{{ old('from') ? old('from') : 'today' }}');
		});
	</script>
@stop


@section('header')
	@include('admin.inc.header')
@stop


@section('content')
	<h1>work with men</h1>
	<div class="calendar">
		<form id="periodEditForm" action="{{ url(Lang::getLocale() . '/admin/statistics') }}" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<p>Period</p>
			<div>
				<label for="from"></label>
				<input type="text" id="from" name="from" value="{{ old('from') }}">
				<img src="{{ asset('img/calendar.png') }}">
			</div>
			<div>
				<label for="to"></label>
				<input type="text" id="to" name="to" value="{{ old('to') }}">
				<img src="{{ asset('img/calendar.png') }}">
			</div>
		</form>
	</div>
	<table>
		<tr>
			<th>Name</th>
			<th>Current balance</th>
			<th>Beginning of period</th>
			<th>End of period</th>
			<th>Total refill</th>
			<th>Total paid</th>
			<th>Time on chat</th>
			<th>Paid for chat</th>
			<th>Real gifts paid</th>
			<th>Virtual gifts paid</th>
			<th>Mails paid</th>
			<th>Detailed</th>
		</tr>
		<?php $totalRefill = 0; ?>
		@foreach($statistics AS $userStatistic)
			<?php $totalRefill += $userStatistic->total_refill; ?>
			<tr class="blue">
				<td>
					<a class="user-name" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $userStatistic->user_id) }}" target="_blank">{{$userStatistic->user_name}}</a>
					@if($userStatistic->user_online)
						<span class="chat-user-status">@lang('cabinet.chat_online')</span>
					@endif
				</td>
				<td class="text-data">{{ number_format($userStatistic->user_balance, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ number_format($userStatistic->beginning_of_period, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ number_format($userStatistic->end_of_period, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ number_format($userStatistic->total_refill, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ number_format($userStatistic->total_paid, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ round($userStatistic->chat_mins) }} m</td>
				<td class="text-data">{{ number_format($userStatistic->chat_paid, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ number_format($userStatistic->real_gifts_paid, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ number_format($userStatistic->virt_gifts_paid, 2, '.', ' ') }} $</td>
				<td class="text-data">{{ number_format($userStatistic->mails_paid, 2, '.', ' ') }} $</td>
				<td>
					<a class="user-name" href="{{ url(Lang::getLocale() . '/admin/show-detailed/' . $userStatistic->user_id. '/' . $userStatistic->user_name) }}" target="_blank">Show</a>
				</td>
			</tr>
		@endforeach
		@if (empty($statistics))
			<tr class="blue">
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
				<td style="text-align: center;">&mdash;</td>
			</tr>
		@endif
	</table>
	<div>
		<div>income:</div>
		<div>{{ number_format($totalRefill, 2, '.', ' ') }} $</div>
	</div>
@stop


@section('footer')
	@include('admin.inc.footer')
@stop


@section('additional_data')
@stop
