@extends('app')


@section('css')
	<link href="{{ asset('css/admin/articles.css') }}" rel="stylesheet" />
@stop

@section('js')
	<script src="{{ asset('/js/ckeditor/ckeditor.js') }}" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('a[href="#addNews"]').fancybox({
				scrolling:  'no',
				titleShow:  false,
				beforeShow: function () {
					try {
						CKEDITOR.instances['textarea1'].destroy();
					} catch (e) {
					}
					CKEDITOR.replace('textarea1', {
						language: '{{ Lang::getLocale() }}',
						filebrowserBrowseUrl: '{{ url('en/elfinder/ckeditor') }}'
					});
				},
				afterClose: function () {
					$('.errors').parent().remove();
					CKEDITOR.instances.textarea1.destroy(true);
				},
			});
			$('a[href="#editNews"]').fancybox({
				scrolling:  'no',
				titleShow:  false,
				beforeShow: function () {
					try {
						CKEDITOR.instances['textarea2'].destroy();
					} catch (e) {
					}
					CKEDITOR.replace('textarea2', {
						language: '{{ Lang::getLocale() }}',
						filebrowserBrowseUrl: '{{ url('en/elfinder/ckeditor') }}'
					});
				},
				afterClose: function () {
					$('.errors').parent().remove();
					CKEDITOR.instances.textarea2.destroy(true);
				},
			});

			$('#addNews').find('[name="title"]').on('keyup change', function (e) {
				$('#addNews').find('[name="url_name"]').val(cyr2lat($('#addNews').find('[name="title"]').val()));
			});

			var addUser = $('#addNews');

			$('a[href="#addNews"]').bind('click', function (e) {
				e.preventDefault();
				addUser.find('h1').html(($(this).attr('s') === '0') ? '@lang('admin.add_news_form_title_m')' : '@lang('admin.add_story_form_title_m')');
				addUser.find('input[name="newsOrStories"]').val($(this).attr('s'));
				if ($(this).attr('s') === '0')
					addUser.find('.add-image').hide();
				else
					addUser.find('.add-image').show();
			});

			$('.spoiler-body').hide()
			$('.spoiler-head').click(function () {
				$(this).toggleClass("folded").toggleClass("unfolded").next().toggle()
			});

			@if (count($errors->addArticlesErrors) > 0 && old('newsOrStories') != NULL)
				$('a[href="#addNews"][s="{{ old('newsOrStories') }}"]').trigger('click');
			@endif;

			$('a[id^="delete_usr_"]').bind('click', function (e) {
				e.preventDefault();
				var thisLink = $(this);
				if (confirm('@lang('admin.msg_confirm_delete_news')'))
					$.ajax({
						async:      true,
						url:        '{{ url(Lang::getLocale() . '/admin/delete-news') }}',
						type:       'POST',
						data:       '_token={{ csrf_token() }}&id=' + thisLink.attr('uid'),
						beforeSend: function () {
						},
						success:    function (data) {
							if (data === 'true')
								thisLink.closest('.spoiler-wrap').remove();
							else
								alert('Error!!!');
						},
						error:      function (request, status, error) {
							if (request.status === 401)
								window.location = '{{ url(Lang::getLocale()) }}';
						}
					});
			});

			$('a[id^="edit_usr_"]').bind('click', function (e) {
				e.preventDefault();

				var editUser = $('#editNews');
				editUser.find('input[name="uid"]').val($(this).attr('uid')); // set user id
				editUser.find('input[name="title"]').val($(this).attr('title2')); // set user id
				editUser.find('textarea[name="preview"]').val($(this).attr('preview')); // set user id
				editUser.find('textarea[name="text"]').val($(this).attr('text2')); // set user id
				editUser.find('input[name="url_name"]').val($(this).attr('url_name')); // set user id
				editUser.find('h1').html($(this).closest('.spoiler-wrap').find('.name').text()); // set user name in title
				editUser.find('input[name="name"]').val($(this).closest('.spoiler-wrap').find('.name').text()); // set user name in input
				editUser.find('input[name="email"]').val($(this).closest('.spoiler-body').find('.email div:last-child').text()); // set user email in input
			});
		});
	</script>
@stop


@section('header')
	@include('admin.inc.header')
@stop


@section('content')

	<div class="man">
		<h1>@lang('admin.add_News_form_NAME')</h1>
		<a href="#addNews" class="add" s="0">
			@lang('admin.users_add_button')
		</a>
		<div class="table">
			<div class="main_line">
				<div class="id">@lang('admin.users_table_id')</div>
				<div class="name">@lang('admin.users_table_name')</div>
			</div>
			@foreach ($News as $key => $new)
				<div class="spoiler-wrap">
					<div class="spoiler-head folded">
						<div class="id">{{ $key+1 }}</div>
						<div class="name">{{ mb_substr($new->title, 0, 29) }}</div>
					</div>
					<div class="spoiler-body">
						<br />
						<div class="clear"></div>
						<br />
						<a href="#" id="delete_usr_{{ $new->id }}" class="usr-button" uid="{{ $new->id }}">
							@lang('admin.users_table_delete_button')
						</a>
						<a href="#editNews" id="edit_usr_{{ $new->id }}" class="usr-button" url_name="{{ $new->url_name }}" preview="{{ $new->preview }}" text2="{{ $new->text }}" title2="{{ $new->title }}" uid="{{ $new->id }}">
							@lang('admin.users_table_edit_button')
						</a>
						<a href="{{ url(Lang::getLocale() . '/news/'.$new->url_name) }}" target="_blank" id="login_usr_{{ $new->id }}" class="usr-button" uid="{{ $new->id }}">
							@lang('admin.show_me_your_eyes')
						</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
	<div class="lady">
		<h1>@lang('admin.add_story_form_NAME')</h1>
		<a href="#addNews" class="add" s="1">
			@lang('admin.users_add_button')
		</a>
		<div class="table">
			<div class="main_line">
				<div class="id">@lang('admin.users_table_id')</div>
				<div class="name">@lang('admin.users_table_name')</div>
			</div>
			@foreach ($Story as $key => $new)
				<div class="spoiler-wrap">
					<div class="spoiler-head folded">
						<div class="id">{{ $key+1 }}</div>
						<div class="name">{{ mb_substr($new->title, 0, 29) }}</div>
					</div>
					<div class="spoiler-body">
						<br />
						<div class="clear"></div>
						<br />
						<a href="#" id="delete_usr_{{ $new->id }}" class="usr-button" uid="{{ $new->id }}">
							@lang('admin.users_table_delete_button')
						</a>
						<a href="#editNews" id="edit_usr_{{ $new->id }}" class="usr-button" url_name="{{ $new->url_name }}" preview="{{ $new->preview }}" text2="{{ $new->text }}" title2="{{ $new->title }}" uid="{{ $new->id }}">
							@lang('admin.users_table_edit_button')
						</a>
						<a href="{{ url(Lang::getLocale() . '/love-story/'.$new->url_name) }}" target="_blank" id="login_usr_{{ $new->id }}" class="usr-button" uid="{{ $new->id }}">
							@lang('admin.show_me_your_eyes')
						</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@stop


@section('footer')
	@include('admin.inc.footer')
@stop


@section('additional_data')
	@include('admin.inc.news-popups')
@stop

