@extends('app')


@section('css')
    <link href="{{ asset('css/admin/users.css') }}" rel="stylesheet"/>
@stop


@section('js')
    <script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a[href="#Withdraw"], a[href="#AddMoneyUser"], a[href="#addUser"], a[href="#editUser"]').fancybox({
                scrolling: 'no',
                titleShow: false,
            });

            var addUser = $('#addUser');

            $('.spoiler-body').hide()
            $('.spoiler-head').click(function () {
                $(this).toggleClass("folded").toggleClass("unfolded").next().toggle()
            });
            @if (count($errors->withdraw) > 0)
                $('a[href="#Withdraw"]').trigger('click');
            @endif;
            @if (count($errors->AddMoneyUser) > 0)
                $('a[href="#AddMoneyUser"]').trigger('click');
            @endif;
            @if (count($errors->register) > 0)
                $('a[href="#addUser"]').trigger('click');
            @endif;

            @if (count($errors->edit) > 0)
                $('a[href="#editUser"]').trigger('click');
            @endif;

            $('a[href="#addUser"]').bind('click', function (e) {
                e.preventDefault();
                addUser.find('h1').html(($(this).attr('s') === 'm') ? '@lang('admin.add_user_form_title_m')' : '@lang('admin.add_user_form_title_w')');
                addUser.find('input[name="sex"]').val($(this).attr('s'));
            });

            $('a[id^="delete_usr_"]').bind('click', function (e) {
                e.preventDefault();
                var thisLink = $(this);
                if (confirm('@lang('admin.msg_confirm_delete')'))
                    $.ajax({
                        async: true,
                        url: '{{ url(Lang::getLocale() . '/admin/delete-user') }}',
                        type: 'POST',
                        data: '_token={{ csrf_token() }}&id=' + thisLink.attr('uid'),
                        beforeSend: function () {
                        },
                        success: function (data) {
                            if (data === 'true')
                                thisLink.closest('.spoiler-wrap').remove();
                            else
                                alert('Error!!!');
                        },
                        error: function (request, status, error) {
                            if (request.status === 401)
                                window.location = '{{ url(Lang::getLocale()) }}';
                        }
                    });
            });
            /*            $('a[id^="add_money_usr_"]').bind('click', function (e) {
             e.preventDefault();
             var thisLink = $(this);
             if (confirm('@lang('admin.msg_confirm_delete')'))
             $.ajax({
             async:      true,
             url:        '{{ url(Lang::getLocale() . '/admin/add-money-user') }}',
             type:       'POST',
             data:       '_token={{ csrf_token() }}&id=' + thisLink.attr('uid'),
             beforeSend: function () {
             },
             success:    function (data) {
             if (data === 'true')
             alert('Money is add');
             else
             alert('Error!!!');
             },
             error:      function (request, status, error) {
             if (request.status === 401)
             window.location = '{{ url(Lang::getLocale()) }}';
             }
             });
             });*/
            $('a[id^="add_money_usr_"]').bind('click', function (e) {
                e.preventDefault();

                var addmoneyusr = $('#AddMoneyUser');
                addmoneyusr.find('input[name="uid"]').val($(this).attr('uid')); // set user id
                addmoneyusr.find('h1').html($(this).closest('.spoiler-wrap').find('.name').text()); // set user name in title

            });
            $('a[id^="withdraw_usr_"]').bind('click', function (e) {
                e.preventDefault();
                var Withdraw = $('#Withdraw');
                $.ajax({
                    async: true,
                    url: '{{ url(Lang::getLocale() . '/admin/money-user') }}',
                    type: 'POST',
                    data: '_token={{ csrf_token() }}&id=' + $(this).attr('uid'),
                    beforeSend: function () {
                    },
                    success: function (data) {
                        if(data == null){
                            data = 0.000 ;
                            Withdraw.find('h2').html(data+' USD');
                        }else{
                            Withdraw.find('h2').html(data+' USD');
                        }
                    },
                    error: function (request, status, error) {

                    }
                });
                Withdraw.find('input[name="uid"]').val($(this).attr('uid')); // set user id
              //  Withdraw.find('input[name="amount"]').attr('max' , ''+parseInt(data)+'' ); // set user id
                Withdraw.find('h1').html($(this).closest('.spoiler-wrap').find('.name').text()); // set user name in title
            });

            $('a[id^="edit_usr_"]').bind('click', function (e) {
                e.preventDefault();

                var editUser = $('#editUser');
                editUser.find('input[name="uid"]').val($(this).attr('uid')); // set user id
                editUser.find('h1').html($(this).closest('.spoiler-wrap').find('.name').text()); // set user name in title
                editUser.find('input[name="name"]').val($(this).closest('.spoiler-wrap').find('.name').text()); // set user name in input
                editUser.find('input[name="email"]').val($(this).closest('.spoiler-body').find('.email div:last-child').text()); // set user email in input
            });

            $('a[id^="login_usr_"]').bind('click', function (e) {
                e.preventDefault();

                $.ajax({
                    async: true,
                    url: '{{ url(Lang::getLocale() . '/admin/login-user') }}',
                    type: 'POST',
                    data: '_token={{ csrf_token() }}&id=' + $(this).attr('uid'),
                    beforeSend: function () {
                    },
                    success: function (data) {
                        if (data === 'true')
                            window.location = '{{ url(Lang::getLocale() . Config::get('app.default_cabinet_page')) }}';
                    },
                    error: function (request, status, error) {
                        if (request.status === 401)
                            window.location = '{{ url(Lang::getLocale()) }}';
                    }
                });
            });

        });
    </script>
@stop


@section('header')
    @include('admin.inc.header')
@stop


@section('content')
    <div class="man">
        <h1>@lang('admin.users_man_title')</h1>
        <a href="#addUser" class="add" s="m">
            @lang('admin.users_add_button')
        </a>
        <div class="table">
            <div class="main_line">
                <div class="id">@lang('admin.users_table_id')</div>
                <div class="name">@lang('admin.users_table_name')</div>
            </div>
            @foreach ($m_list AS $user)
                <div class="spoiler-wrap">
                    <div class="spoiler-head folded">
                        <div class="id">{{ str_pad($user->id, 5, "0", STR_PAD_LEFT) }}</div>
                        <div class="name">{{ $user->name }}</div>
                    </div>
                    <div class="spoiler-body">
                        <br/>
                        <div class="email">
                            <div>@lang('admin.users_table_email')</div>
                            <div>{{ $user->email }}</div>
                        </div>
                        <div class="clear"></div>
                        <div class="password">
                            <div>@lang('admin.users_table_password')</div>
                            <div>******</div>
                        </div>
                        <div class="clear"></div>
                        <br/>
                        <a href="#" id="delete_usr_{{ $user->id }}" class="usr-button" uid="{{ $user->id }}">
                            @lang('admin.users_table_delete_button')
                        </a>
                        <a href="#editUser" id="edit_usr_{{ $user->id }}" class="usr-button" uid="{{ $user->id }}">
                            @lang('admin.users_table_edit_button')
                        </a>
                        <a href="#" id="login_usr_{{ $user->id }}" class="usr-button" uid="{{ $user->id }}">
                            @lang('admin.users_table_login_button')
                        </a>
                        <div class="clear"></div>
                        <br/>
                        <a href="#Withdraw" id="withdraw_usr_{{ $user->id }}" class="usr-button" uid="{{
						$user->id }}">
                            Withdraw
                        </a>
                        <a href="#AddMoneyUser" id="add_money_usr_{{ $user->id }}" class="usr-button" uid="{{
						$user->id }}">
                            Add Money
                        </a>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="lady">
        <h1>@lang('admin.users_lady_title')</h1>
        <a href="#addUser" class="add" s="w">
            @lang('admin.users_add_button')
        </a>
        <div class="table">
            <div class="main_line">
                <div class="id">@lang('admin.users_table_id')</div>
                <div class="name">@lang('admin.users_table_name')</div>
            </div>
            @foreach ($w_list AS $user)
                <div class="spoiler-wrap">
                    <div class="spoiler-head folded">
                        <div class="id">{{ str_pad($user->id, 5, "0", STR_PAD_LEFT) }}</div>
                        <div class="name">{{ $user->name }}</div>
                    </div>
                    <div class="spoiler-body">
                        <br/>
                        <div class="email">
                            <div>@lang('admin.users_table_email')</div>
                            <div>{{ $user->email }}</div>
                        </div>
                        <div class="clear"></div>
                        <div class="password">
                            <div>@lang('admin.users_table_password')</div>
                            <div>******</div>
                        </div>
                        <div class="clear"></div>
                        <br/>
                        <a href="#" id="delete_usr_{{ $user->id }}" class="usr-button" uid="{{ $user->id }}">
                            @lang('admin.users_table_delete_button')
                        </a>
                        <a href="#editUser" id="edit_usr_{{ $user->id }}" class="usr-button" uid="{{ $user->id }}">
                            @lang('admin.users_table_edit_button')
                        </a>
                        <a href="#" id="login_usr_{{ $user->id }}" class="usr-button" uid="{{ $user->id }}">
                            @lang('admin.users_table_login_button')
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop


@section('footer')
    @include('admin.inc.footer')
@stop


@section('additional_data')
    @include('admin.inc.user-popups')
@stop
