@extends('app')


@section('css')
	<link href="{{ asset('css/admin/statistics.css') }}" rel="stylesheet" />
@stop


@section('js')
	<script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>

@stop


@section('header')
	@include('admin.inc.header')
@stop


@section('content')
	<h1 style="width: 100%;" >men name - {{$name}}</h1>
		<div class="clear"></div>
		<table style="margin-top: 28px;">
			<tr>
				<th style="width: 120px;">@lang('cabinet.payments_list_date')</th>
				<th style="width: 320px;">@lang('cabinet.payments_list_event')</th>
				<th style="width: 170px;">@lang('cabinet.payments_partner')</th>
				<th style="width: 170px;">Gifts</th>
				<th style="">@lang('cabinet.payments_paid')</th>
			</tr>
		</table>
		<div class="clear"></div>
		<div class="scroller">
			<table>
				@foreach($historyData AS $action)
					<tr class="blue">
						<td style="text-align: center; width: 120px;">{{ $action->date }}</td>
						<td style="width: 320px;">@lang('cabinet.event_' . $action->event)</td>
						<td style="text-align: center; width: 170px;">
							@if (empty($action->partner_id))
								--
							@else
								<a style = "color:navy;" href="{{ url(Lang::getLocale() . '/cabinet/profile/' . $action->partner_id) }}">{{ $action->partner_name }}</a>
							@endif
						</td>
						<td style="text-align: center; width: 170px;">
							{{ $action->gifts }}
						</td>
						<td style="text-align: right;">{{ ($action->event == 'recharge' ? '+' : '-') . $action->paid }} $</td>
					</tr>
				@endforeach
			</table>
		</div>
		<div id="payForm" style="display: none;">
			<div class="pay-form-h1">The amount of replenishment:</div>
			{{-- <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                 <input type="hidden" name="cmd" value="_s-xclick">
                 <input type="hidden" name="hosted_button_id" value="PEH2ZXDXATSS2">
                 <table>
                     <tr><td><input type="hidden" name="on0" value="Pricing - UKR Bride 4 U">Pricing - UKR Bride 4 U</td></tr><tr><td><select name="os0">
                                 <option value="Live Chat - per minute">Live Chat - per minute $0,01 USD</option>
                                 <option value="Letter">Letter $3,00 USD</option>
                                 <option value="Meet in Person">Meet in Person $20,00 USD</option>
                             </select> </td></tr>
                 </table>
                 <input type="hidden" name="currency_code" value="USD">
                 <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                 <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
             </form>--}}


			{{--<form action="{{ url(Lang::getLocale() . '/cabinet/paymentspaypal') }}" method="get">
                <input type='number' name='total' required min='1' step='1' value='10' autofocus >
                <button type="submit">
                    Pay Now
                </button>
            </form>--}}
		</div>

		<div class="clear"></div>


@stop


@section('footer')
	@include('admin.inc.footer')
@stop


@section('additional_data')
@stop
