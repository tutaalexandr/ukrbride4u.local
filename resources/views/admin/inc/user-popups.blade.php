<div id="addUser" style="display: none;">
	<h1></h1>
	@if (count($errors->register) > 0)
		<div>
			<ul class="errors">
				@foreach ($errors->register->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/admin/register-user') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="sex" value="{{ old('sex') }}">

		<div>
			<label for="registerName">@lang('admin.add_user_form_name')</label>
			<br />
			<input id="registerName" type="text" name="name" value="{{ old('name') }}">
		</div>

		<div>
			<label for="registerEmail">@lang('admin.add_user_form_email')</label>
			<br />
			<input id="registerEmail" type="text" name="email" value="{{ old('email') }}">
			<br />
			<p>@lang('admin.add_user_form_will_be_used_as_login')</p>
		</div>

		<div>
			<label for="registerPassword">@lang('admin.add_user_form_password')</label>
			<br />
			<input id="registerPassword" type="password" name="password" value="{{ old('password') }}">
		</div>

		<div>
			<label for="registerPasswordConfirm">@lang('admin.add_user_form_password_confirm')</label>
			<br />
			<input id="registerPasswordConfirm" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
		</div>

		<div>
			<input id="needLogin" type="checkbox" name="need_login"{{ old('needLogin') ? ' checked=""' : '' }}>
			<label for="needLogin"><span></span>@lang('admin.add_user_form_need_login')</label>
		</div>

		<button type="submit">@lang('admin.button_register_new_user')</button>
	</form>
</div>

<div id="editUser" style="display: none;">
	<h1></h1>
	@if (count($errors->edit) > 0)
		<div>
			<ul class="errors">
				@foreach ($errors->edit->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/admin/edit-user') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="uid" value="{{ old('uid') }}">

		<div>
			<label>@lang('admin.edit_user_name')</label>
			<br />
			<input type="text" name="name" value="{{ old('name') }}">
		</div>

		<div>
			<label>@lang('admin.edit_user_email')</label>
			<br />
			<input type="text" name="email" value="{{ old('email') }}">
		</div>

		<br />

		<div>
			<label>@lang('admin.edit_user_password')</label>
			<br />
			<input type="password" name="password" value="{{ old('password') }}" placeholder="@lang('admin.edit_user_placeholder_edit_password')">
		</div>

		<div>
			<label>@lang('admin.edit_user_password_confirm')</label>
			<br />
			<input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="@lang('admin.edit_user_placeholder_edit_password_confirm')">
		</div>

		<br />

		<button type="submit">@lang('admin.button_edit_user')</button>
	</form>
</div>
<div id="AddMoneyUser" style="display: none;">
	<h1></h1>
	<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/admin/add-money-user') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="uid" value="{{ old('uid') }}">

		<div>
			<label>@lang('admin.edit_user_name')</label>
			<br />
			<input type="number" name="amount" value="1">
			<label>USD</label>
		</div>


		<br />

		<button type="submit">Add Money</button>
	</form>
</div>
<div id="Withdraw" style="display: none;">
	<h1></h1>
	<h2></h2>
	<form role="form" method="POST" action="{{ url(Lang::getLocale() . '/admin/withdraw-money-user') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="uid" value="{{ old('uid') }}">

		<div>
			<label>change amount</label>
			<br />
			<input type="number" name="amount" value="1" max = ''>
			<label>USD</label>
		</div>
		<br />
		<button type="submit">Withdraw Money</button>
	</form>
</div>