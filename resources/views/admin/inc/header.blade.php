<div>
	<img src="{{ asset('img/melodyOfLove.svg') }}" width="412px" height="70px" />
</div>
<div class="brutto">
	<div class="social">
		<div class="">
			<a href="#">
				<img src="{{ asset('img/facebook.png') }}" alt="facebook" />
			</a>
		</div>
		<div class="">
			<a href="#">
				<img src="{{ asset('img/twitter.png') }}" alt="twitter" />
			</a>
		</div>
		<div class="">
			<a href="#">
				<img src="{{ asset('img/facebook.png') }}" alt="facebook" />
			</a>
		</div>
		<div class="">
			<a href="#">
				<img src="{{ asset('img/twitter.png') }}" alt="twitter" />
			</a>
		</div>
	</div>
	<div class="lang">
		<a class="ru" href="{{ getCurrentURLWithLang('ru') }}">rus</a>
		<a class="eng" href="{{ getCurrentURLWithLang('en') }}">eng</a>
	</div>
</div>
<div class="button_header">
	<ul>
		<li>
			<a class="button" href="{{ url(Lang::getLocale() . '/admin/articles') }}">@lang('admin.header_articles')</a>
		</li>
		<li>
			<a class="button" href="{{ url(Lang::getLocale() . '/admin/statistics') }}">@lang('admin.header_statistics')</a>
		</li>
		<li>
			<a class="button" href="{{ url(Lang::getLocale() . '/admin/users') }}">@lang('admin.header_users')</a>
		</li>
		<li>
			<a class="button" href="{{ url(Lang::getLocale() . '/auth/logout') }}">@lang('admin.header_logout')</a>
		</li>
	</ul>
</div>
@if (Auth::getUser()->rights == 'superadmin')
	<div class="sa-menu">
		<h3>SuperAdmin Menu</h3>
		<hr />
		<a class="sa-button{{ (getCurrentControllerName() == 'HomeController') ? ' sa-button-selected' : '' }}" href="{{ url(\Lang::getLocale()) }}">Go To Home</a>
		<a class="sa-button{{ (getCurrentControllerName() == 'CabinetController') ? ' sa-button-selected' : '' }}" href="{{ url(\Lang::getLocale() . \Config::get('app.default_cabinet_page')) }}">Go To Cabinet</a>
		<a class="sa-button{{ (getCurrentControllerName() == 'AdminController') ? ' sa-button-selected' : '' }}" href="{{ url(\Lang::getLocale() . \Config::get('app.default_admin_page')) }}">Go To Admin</a>
	</div>
@endif
