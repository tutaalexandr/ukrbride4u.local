<div id="addNews" style="display: none;">
    <h1></h1>
    @if (count($errors->addArticlesErrors) > 0)
        <div>
            <ul class="errors">
                @foreach ($errors->addArticlesErrors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form role="form" method="POST" action="{{ url(Lang::getLocale() . '/admin/register-news') }}" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="newsOrStories" value="{{ old('newsOrStories') }}">

        <div>
            <label for="title">title</label>
            <br />
            <input size="150" id="title" type="text" name="title" value="{{ old('title') }}">
            <br />
            <label for="url_name">url_name</label>
            <br />
            <input size="150" id="url_name" type="text" name="url_name" value="{{ old('url_name') }}">
            <br />
            <div class="add-image">
                <label for="url_name">image</label>
                <br />
                <input name="image" type="file">
                <br />
            </div>
        </div>
        <div>
            <label for="title">text</label>
            <br />
            <div><textarea id="textarea1" name="text">{{ old('text') }}</textarea></div>
            <br />
        </div>

        <button type="submit">@lang('admin.button_register_new_user')</button>
    </form>
</div>
<div id="editNews" style="display: none;">
    <form role="form" method="POST" action="{{ url(Lang::getLocale() . '/admin/edit-news') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="uid" value="{{ old('uid') }}">
        <div>
            <br />
            <label>title</label>
            <br />
            <input size="150" type="text" name="title" value="{{ old('title2') }}  onclick=" urls();"">
        </div>
        <div>
            <label>url_name</label>
            <br />
            <input size="150" type="text" name="url_name" value="{{ old('url_name') }}">
        </div>
        <div>
            <textarea id="textarea2" name="text">{{ old('text2') }}</textarea>
        </div>
        <br />
        <button type="submit">Corect</button>
    </form>
</div>
