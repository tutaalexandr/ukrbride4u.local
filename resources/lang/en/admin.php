<?php

return [
    /* HEADER */
    "header_articles"                             => "Articles",
    "header_statistics"                           => "Statistics",
    "header_users"                                => "Users",
    "header_logout"                               => "Log out",
    /* USERS */
    "users_man_title"                             => "work with men",
    "users_lady_title"                            => "work with ladies",
    "users_add_button"                            => "Add",
    "users_table_id"                              => "ID",
    "users_table_name"                            => "Name",
    "users_table_email"                           => "EMail",
    "users_table_password"                        => "Password",
    "users_table_delete_button"                   => "Delete",
    "users_table_edit_button"                     => "Edit",
    "users_table_login_button"                    => "LogIn",
    /* Add user form */
    "add_user_form_title_m"                       => "Add man",
    "add_user_form_title_w"                       => "Add lady",
    "add_user_form_name"                          => "Name",
    "add_user_form_email"                         => "E-Mail",
    "add_user_form_will_be_used_as_login"         => "Will be used as login",
    "add_user_form_password"                      => "Password",
    "add_user_form_password_confirm"              => "Confirm",
    "add_user_form_need_login"                    => "auto login",
    "button_register_new_user"                    => "Add",
    /* Messages */
    "msg_confirm_delete"                          => "Are you sure you want to delete a user?",
    /* Edit user form */
    "edit_user_name"                              => "Edit name",
    "edit_user_email"                             => "Edit e-mail",
    "edit_user_password"                          => "Set new password",
    "edit_user_placeholder_edit_password"         => "set new password if need",
    "edit_user_password_confirm"                  => "Confirm new password",
    "edit_user_placeholder_edit_password_confirm" => "confirm new password",
    "button_edit_user"                            => "Edit user",
    /*  news form */
    "msg_confirm_delete_news"                     => "Are you sure you want to delete?",
    "add_news_form_title_m"                       => "Add news",
    "add_story_form_title_m"                      => "Add Love story",
    "add_News_form_NAME"                          => "News",
    "add_story_form_NAME"                         => "Love story",
    "show_me_your_eyes"                           => "Show me",


];
