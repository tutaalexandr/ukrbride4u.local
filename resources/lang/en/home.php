<?php

/*********************** ENGLISH ***********************/

return [
	/* Registration form */
	"reg_form_title"                 => "Registration for men",
	"reg_form_name"                  => "Name",
	"reg_form_email"                 => "E-Mail",
	"reg_form_will_be_used_as_login" => "Will be used as login",
	"reg_form_password"              => "Password",
	"reg_form_password_confirm"      => "Confirm",
	"reg_form_sign_in_with"          => "Sign in with",
	"reg_form_agreement"             => "By clicking «Join now» you agree with all of the terms and condition. If you can not register, please contact us.",
	/* Buttons */
	"button_for_ladies"              => "For ladies",
	"button_about_us"                => "About Us",
	"button_login"                   => "Log In",
	"button_join_now"                => "Join now",
	"button_more_stories"            => "More stories",
	/* Online ladies */
	"online_ladies_title"            => "Online ladies",
	/* Succesful stories */
	"suc_stories_title"              => "SUCCESSFUL STORIES AND FEEDBACK",
	/* News */
	"news_title"                     => "News",
	/* Video */
	"video_title"                    => "Video",
	/* Login form */
	"login_form_title"               => "Log In",
	"login_form_email"               => "User mail",
	"login_form_password"            => "User password",
	"login_form_remember_me"         => "Remember me",

];
