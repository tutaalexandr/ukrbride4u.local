<?php

/*********************** RUSSIAN ***********************/

return [
	/* Registration form */
	"reg_form_title"                 => "Регистрация для мужчин",
	"reg_form_name"                  => "Имя",
	"reg_form_email"                 => "E-Mail",
	"reg_form_will_be_used_as_login" => "Будет использовано как логин",
	"reg_form_password"              => "Пароль",
	"reg_form_password_confirm"      => "Повторно",
	"reg_form_sign_in_with"          => "Войти через",
	"reg_form_agreement"             => "Нажав «Присоединиться» вы согласны со всеми условиями и положениями. Если вы не можете зарегистрироваться, пожалуйста, свяжитесь с нами.",
	/* Buttons */
	"button_for_ladies"              => "Для девушек",
	"button_about_us"                => "О нас",
	"button_login"                   => "Вход",
	"button_join_now"                => "Присоединиться",
	"button_more_stories"            => "Больше историй",
	/* Online ladies */
	"online_ladies_title"            => "Девушки онлайн",
	/* Succesful stories */
	"suc_stories_title"              => "ИСТОРИИ УСПЕХА И ОТЗЫВЫ",
	/* News */
	"news_title"                     => "Новости",
	/* Video */
	"video_title"                    => "Видео",
	/* Login form */
	"login_form_title"               => "Вход",
	"login_form_email"               => "EMail пользователя",
	"login_form_password"            => "Пароль пользователя",
	"login_form_remember_me"         => "Запомнить меня",
];
