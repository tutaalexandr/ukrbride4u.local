<?php

return [
	/* HEADER */
	"header_articles"                             => "Статьи",
	"header_statistics"                           => "Статистика",
	"header_users"                                => "Пользователи",
	"header_logout"                               => "Выход",
	/* USERS */
	"users_man_title"                             => "работа с мужчинами",
	"users_lady_title"                            => "работа с девушками",
	"users_add_button"                            => "Добавить",
	"users_table_id"                              => "ID",
	"users_table_name"                            => "Имя",
	"users_table_email"                           => "EMail",
	"users_table_password"                        => "Пароль",
	"users_table_delete_button"                   => "Удалить",
	"users_table_edit_button"                     => "Изменить",
	"users_table_login_button"                    => "Войти",
	/* Add user form */
	"add_user_form_title_m"                       => "Добавить мужчину",
	"add_user_form_title_w"                       => "Добавить девушку",
	"add_user_form_name"                          => "Имя",
	"add_user_form_email"                         => "E-Mail",
	"add_user_form_will_be_used_as_login"         => "Будет использоваться как логин",
	"add_user_form_password"                      => "Пароль",
	"add_user_form_password_confirm"              => "Подтверждение",
	"add_user_form_need_login"                    => "автоматически войти",
	"button_register_new_user"                    => "Добавить",
	/* Messages */
	"msg_confirm_delete"                          => "Вы уверены что желаете удалить пользователя?",
	/* Edit user form */
	"edit_user_name"                              => "Изменить имя",
	"edit_user_email"                             => "Изменить e-mail",
	"edit_user_password"                          => "Новый пароль",
	"edit_user_placeholder_edit_password"         => "новый пароль если нужно",
	"edit_user_password_confirm"                  => "Подтвердите пароль",
	"edit_user_placeholder_edit_password_confirm" => "подтвердите новый пароль",
	"button_edit_user"                            => "Редактировать",
	/*  news form */
	"msg_confirm_delete_news"                     => "Вы уверены что желаете удалить?",
	"add_news_form_title_m"                       => "Добавить новость",
	"add_story_form_title_m"                      => "Добавить историю",
	"add_News_form_NAME"                          => "Новости",
	"add_story_form_NAME"                         => "Истории",
	"show_me_your_eyes"                           => "Посмотреть",


];
